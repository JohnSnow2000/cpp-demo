/*
 * Copyright (c) 2012-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <usb.h>
#include <errno.h>
#include <asm/io.h>
#include <asm/arch/clk.h>
#include <asm/arch/rsc.h>
#include <asm/arch/rst.h>
#include <asm/arch/usb.h>
#include <asm/arch/gpio.h>
#include <asm/arch/bitops.h>

#include "ehci.h"

static struct usbcd_db usbcd_db;

#define USB_REG(ofs)		(usbcd_db.regbase + (ofs))

#ifdef CONFIG_ARCH_ATLASVI
static void sirf_gpio_output(int group, int index, int value)
{
	/* set as output pin */
	setbits_le32(GPIO_CTRL_REGBASE(group, index), GPIO_CTL_OUT_EN_MASK);
	/* set output value */
	if (value)
		setbits_le32(GPIO_CTRL_REGBASE(group, index),
			     GPIO_CTL_DATAOUT_MASK);
	else
		clrbits_le32(GPIO_CTRL_REGBASE(group, index),
			     GPIO_CTL_DATAOUT_MASK);
	/* disable gpio interrupt */
	clrbits_le32(GPIO_CTRL_REGBASE(group, index), GPIO_CTL_INTR_EN_MASK);
}
#endif

#ifndef CONFIG_ARCH_ATLAS7
static void usbcd_pad_share(int action)
{
#ifdef CONFIG_ARCH_MARCO
	if (!get_bit((void *)RSC_USB_UART_SHARE_SET, USBUART_SHARE_EN))
#else
	if (!get_bit((void *)RSC_USB_UART_SHARE, USBUART_SHARE_EN))
#endif
		action = 0;

	if (action) {
		/* switch to UART mode  */
		debug("WARNING: switching to UART mode.\n");

	} else {
		/* switch to USB mode */
		debug("%s: switching to USB mode.\n", __func__);
#ifdef CONFIG_ARCH_MARCO
		setbits_le32(RSC_USB_UART_SHARE_CLR, USBUART_SHARE_EN);
#else
		clrbits_le32(RSC_USB_UART_SHARE, USBUART_SHARE_EN);
#endif
		setbits_le32(USB_REG(USBPHY_PROGRAM), USBPHY_DRVVBUS);
		udelay(100);
#ifdef CONFIG_ARCH_MARCO
		setbits_le32(RSC_USB_UART_SHARE_SET, USB1_MODE_SEL);
#else
		setbits_le32(RSC_USB_UART_SHARE, USB1_MODE_SEL);
#endif
		udelay(100);
		clrbits_le32(USB_REG(USBPHY_PROGRAM), USBPHY_POR);
	}
}
#endif

static inline void usbcd_start_controller(void)
{
	setbits_le32(USB_REG(USBOTG_USBCMD), RS);
}

static void usbcd_set_mode(void)
{
	debug("USB OTG in Host Mode.\n");
	/* Enable PHY Clock */
	clrbits_le32(USB_REG(USBOTG_PORTSC1), PHCD);

#ifndef CONFIG_ARCH_ATLAS7
	usbcd_pad_share(1);
#endif
	setbits_le32(USB_REG(USBOTG_USBMODE), USBOTG_MODE_HOST);
}

static inline void usbcd_reset_controller(void)
{
	setbits_le32(USB_REG(USBOTG_USBCMD), RST);

	if (on_bit_clear(USB_REG(USBOTG_USBCMD), RST, 1000) == -ETIME)
		debug("USB Host Reset Controller Timeout!\n");
	else
		debug("USB Host Controller Reset Done.\n");
}

static void usbcd_hw_init(void)
{
	setbits_le32(USB_REG(USBPHY_PROGRAM), USBPHY_AXI_BURST_LENGTH);

	writel(0x1010, USB_REG(USBOTG_BURSTSIZE));

	clrbits_le32(USB_REG(USBOTG_USBMODE), USBOTG_MODE_SDIS);

	/* Reset Controller */
	usbcd_reset_controller();

	/* Set Host Mode */
	usbcd_set_mode();

	setbits_le32(USB_REG(USBOTG_USBINTR), PCE);

	/* Enable the required interrupts */
	setbits_le32(USB_REG(USBOTG_OTGSC),
		     IDIE | AVVIE | ASVIE | BSVIE | BSEIE);

	/* Start Controller */
	usbcd_start_controller();
}

static void sirf_usbcd_init(int index)
{
#ifdef CONFIG_ARCH_ATLAS7
	usbcd_db.regbase	= (void *)USB_REG_BASE(index);
	usbcd_db.phy		= USB_PHY_UTMI;
	usbcd_db.hccr		= (u32)usbcd_db.regbase + HCCR_OFFSET;
	usbcd_db.hcor		= (u32)usbcd_db.regbase + HCOR_OFFSET;
	clrbits_le32(USB_REG(USBPHY_PROGRAM), USBPHY_POR);

#else
	usbcd_db.regbase	= (void *)USB_REG_BASE(1);
	usbcd_db.phy		= USB_PHY_UTMI;
	usbcd_db.hccr		= (u32)usbcd_db.regbase + HCCR_OFFSET;
	usbcd_db.hcor		= (u32)usbcd_db.regbase + HCOR_OFFSET;

#ifdef CONFIG_ARCH_MARCO
	setbits_le32(RESET_SR0_SET, RESET_SR_USB1_RST);
#else
	setbits_le32(RESET_SR0, RESET_SR_USB1_RST);
#endif
	udelay(500);
#ifdef CONFIG_ARCH_MARCO
	setbits_le32(RESET_SR0_CLR, RESET_SR_USB1_RST);
#else
	clrbits_le32(RESET_SR0, RESET_SR_USB1_RST);
#endif

#ifdef CONFIG_ARCH_MARCO
	setbits_le32(RSC_USB_CTRL_CLR, USBPHY_PLL_POWERDOWN);
#else
	clrbits_le32(RSC_USBPHY_PLL_CTRL, USBPHY_PLL_POWERDOWN);
#endif
#ifdef CONFIG_ARCH_MARCO
	on_bit_set(RSC_USB_CTRL_CLR, USBPHY_PLL_LOCK, 500);
#else
	on_bit_clear((void *)RSC_USBPHY_PLL_CTRL, USBPHY_PLL_LOCK, 500);
#endif

	clrbits_le32(USB_REG(USBPHY_PROGRAM), USBPHY_POR);

	setbits_le32(CLKC_CLK_EN0, CLK_USB1_EN);

#ifdef CONFIG_ARCH_ATLASVI
	sirf_gpio_output(0, 14, 1);
#endif
#ifdef CONFIG_ARCH_PRIMAII
	clrbits_le32(GPIO_PAD_EN(1), (1 << 27));
	setbits_le32(RSC_PIN_MUX, PAD_UTMI_DRVVBUS1_EN);
#endif
#endif
	udelay(100);
	usbcd_hw_init();
}

/*
 * EHCI-initialization
 * Create the appropriate control structures to manage
 * a new EHCI host controller.
 */
int ehci_hcd_init(int index, enum usb_init_type init,
			struct ehci_hccr **hccr, struct ehci_hcor **hcor)
{
	sirf_usbcd_init(index);
	*hccr = (struct ehci_hccr *)(usbcd_db.hccr);
	*hcor = (struct ehci_hcor *)(usbcd_db.hcor);

	debug("SiRF-ehci: init hccr %x and hcor %x hc_length %d\n",
	      (uint32_t)*hccr, (uint32_t)*hcor,
	      (uint32_t)HC_LENGTH(ehci_readl(&(*hccr)->cr_capbase)));

	return 0;
}

/*
 * Destroy the appropriate control structures corresponding
 * the EHCI host controller.
 */
int ehci_hcd_stop(int index)
{
	return 0;
}
