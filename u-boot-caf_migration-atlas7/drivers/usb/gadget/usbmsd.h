/*
 * (C) Copyright 2008
 *
 * Based on Linux kernel's USB device MSD gadget driver
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	 USA
 *
 */

#ifndef __USB_MSD_H__
#define __USB_MSD_H__

/* #include <usb.h> */
#include <usbdevice.h>
#include "sirfsoc_udc.h"

/* defined in nanddisk.c */
extern int nanddisk_start;
/* defined in core.c */
extern struct usb_string_descriptor **usb_strings;

extern block_dev_desc_t *block_dev_init(void);

#define CONFIG_USBD_MANUFACTURER        "SiRF"
#define CONFIG_USBD_PRODUCT_NAME        "AtlasVI EVB"

#ifndef CONFIG_USBD_VENDORID
#define CONFIG_USBD_VENDORID		0x105B
#endif	/* CONFIG_USBD_VENDORID */

#ifndef CONFIG_USBD_PRODUCTID
#define CONFIG_USBD_PRODUCTID		0x9030
#endif

#define NUM_CONFIGS    1
#define NUM_INTERFACES 1
#define NUM_ENDPOINTS  2
#define NUM_CLASSES    0

#define EP0_MAX_PACKET_SIZE 64

#define CONFIG_USBD_CONFIGURATION_STR "Mass Storage"
#define CONFIG_USBD_INTERFACE_STR     "Mass Storage"

#define CONFIG_USBD_MSD_OUT_ENDPOINT	1
#define CONFIG_USBD_MSD_OUT_PKTSIZE	512
#define CONFIG_USBD_MSD_IN_ENDPOINT	2
#define CONFIG_USBD_MSD_IN_PKTSIZE	512


#define USBMSD_DEVICE_CLASS	PER_INTERFACE_CLASS
#define USBMSD_DEVICE_SUBCLASS	0
#define USBMSD_DEVICE_PROTOCOL	0

#define USBMSD_CLASS	   0x08	/* Mass Storage */

#define USBMSD_INTERFACE_CLASS	   USBMSD_CLASS	/* Mass Storage */
#define USBMSD_INTERFACE_SUBCLASS  0x06	/* SCSI */
#define USBMSD_INTERFACE_PROTOCOL  0x50	/* Bulk Only */

#define USBMSD_BCD_DEVICE 0x0200
#define USBMSD_MAXPOWER	  0x1

#define STR_LANG		 0
#define STR_MANUFACTURER 1
#define STR_PRODUCT	     2
#define STR_SERIAL	     3
#define STR_CONFIG	     4
#define STR_INTERFACE    5
#define STR_COUNT		 6


/* Bulk-only class specific requests */
#define USB_BULK_RESET_REQUEST		0xff
#define USB_BULK_GET_MAX_LUN_REQUEST	0xfe
#define MAX_COMMAND_SIZE	16/* Length of a SCSI Command Data Block */

enum data_direction {
	DATA_DIR_UNKNOWN = 0,
	DATA_DIR_FROM_HOST,
	DATA_DIR_TO_HOST,
	DATA_DIR_NONE
};

struct lun {
	u64 file_length;
	u32 num_sectors;

	unsigned int ro:1;
	unsigned int registered:1;
	unsigned int info_valid:1;

	u32 sense_data;
	u32 sense_data_info;

};

struct req {
	u32 length;
	u32 zero;
};

struct usb_msd_context {

	unsigned int bulk_out_maxpacket;
	unsigned int exception_req_tag;

	u8 config, new_config;

	unsigned int running:1;
	unsigned int bulk_in_enabled:1;
	unsigned int bulk_out_enabled:1;
	unsigned int intr_in_enabled:1;
	unsigned int phase_error:1;
	unsigned int short_packet_received:1;
	unsigned int bad_lun_okay:1;
	unsigned int cbw_error:1;
	unsigned int stopped:1;
	unsigned int exit_flag:1;


	unsigned long atomic_bitflags;
#define REGISTERED		0
#define CLEAR_BULK_HALTS	1
#define SUSPENDED		2

	int cmnd_size;
	u8 cmnd[MAX_COMMAND_SIZE];
	enum data_direction data_dir;
	u32 data_size;
	u32 data_size_from_cmnd;
	u32 tag;
	unsigned int lun;
	u32 residue;

	u32 rx_buffer_data_size;

	/* The CB protocol offers no way for a host to know when a command
	 * has completed.  As a result the next command may arrive early,
	 * and we will still have to handle it.  For that reason we need
	 * a buffer to store new commands when using CB (or CBI, which
	 * does not oblige a host to wait for command completion either). */
	int cbbuf_cmnd_size;
	u8 cbbuf_cmnd[MAX_COMMAND_SIZE];

	unsigned int nluns;
	struct lun luns[1];
	struct lun *curlun;

	struct req inreq;
	struct req outreq;
};



/* Bulk-only data structures */

/* Command Block Wrapper */
struct bulk_cb_wrap {
	u32 Signature;				/* Contains 'USBC' */
	u32 Tag;				/* Unique per command id */
	u32 DataTransferLength;			/* Size of the data */
	u8 Flags;				/* Direction in bit 7 */
	u8 Lun;					/* LUN (normally 0) */
	u8 Length;			/* Of the CDB, <= MAX_COMMAND_SIZE */
	u8 CDB[16];				/* Command Data Block */
};

#define USB_BULK_CB_WRAP_LEN	31
#define USB_BULK_CB_SIG		0x43425355	/* Spells out USBC */
#define USB_BULK_IN_FLAG	0x80

/* Command Status Wrapper */
struct bulk_cs_wrap {
	u32 Signature;				/* Should = 'USBS' */
	u32 Tag;				/* Same as original command */
	u32 Residue;				/* Amount not transferred */
	u8 Status;				/* See below */
};

#define USB_BULK_CS_WRAP_LEN	13
#define USB_BULK_CS_SIG		0x53425355	/* Spells out 'USBS' */
#define USB_STATUS_PASS		0
#define USB_STATUS_FAIL		1
#define USB_STATUS_PHASE_ERROR	2

#define MAX_COMMAND_SIZE	16/* Length of a SCSI Command Data Block */

/* SCSI commands that we recognize */
#define SC_FORMAT_UNIT			0x04
#define SC_INQUIRY			0x12
#define SC_MODE_SELECT_6		0x15
#define SC_MODE_SELECT_10		0x55
#define SC_MODE_SENSE_6			0x1a
#define SC_MODE_SENSE_10		0x5a
#define SC_PREVENT_ALLOW_MEDIUM_REMOVAL	0x1e
#define SC_READ_6			0x08
#define SC_READ_10			0x28
#define SC_READ_12			0xa8
#define SC_READ_CAPACITY		0x25
#define SC_READ_FORMAT_CAPACITIES	0x23
#define SC_RELEASE			0x17
#define SC_REQUEST_SENSE		0x03
#define SC_RESERVE			0x16
#define SC_SEND_DIAGNOSTIC		0x1d
#define SC_START_STOP_UNIT		0x1b
#define SC_SYNCHRONIZE_CACHE		0x35
#define SC_TEST_UNIT_READY		0x00
#define SC_VERIFY			0x2f
#define SC_WRITE_6			0x0a
#define SC_WRITE_10			0x2a
#define SC_WRITE_12			0xaa

/* SCSI Sense Key/Additional Sense Code/ASC Qualifier values */
#define SS_NO_SENSE				0
#define SS_COMMUNICATION_FAILURE		0x040800
#define SS_INVALID_COMMAND			0x052000
#define SS_INVALID_FIELD_IN_CDB			0x052400
#define SS_LOGICAL_BLOCK_ADDRESS_OUT_OF_RANGE	0x052100
#define SS_LOGICAL_UNIT_NOT_SUPPORTED		0x052500
#define SS_MEDIUM_NOT_PRESENT			0x023a00
#define SS_MEDIUM_REMOVAL_PREVENTED		0x055302
#define SS_NOT_READY_TO_READY_TRANSITION	0x062800
#define SS_RESET_OCCURRED			0x062900
#define SS_SAVING_PARAMETERS_NOT_SUPPORTED	0x053900
#define SS_UNRECOVERED_READ_ERROR		0x031100
#define SS_WRITE_ERROR				0x030c02
#define SS_WRITE_PROTECTED			0x072700

#define SK(x)		((u8) ((x) >> 16))	/* Sense Key byte, etc. */
#define ASC(x)		((u8) ((x) >> 8))
#define ASCQ(x)		((u8) (x))

#define BLOCK_DEV_SPI_INDEX		0x2
#endif
