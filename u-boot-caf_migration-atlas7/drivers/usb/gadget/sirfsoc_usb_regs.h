
/*
 * USB Device Controller driver for CSR SiRFatlasVI
 *
 * Copyright (c) 2012 Cambridge Silicon Radio Limited, a CSR plc group company.
 *
 * Licensed under GPLv2 or later.
 */

#ifndef __SIRFSOC_USB_REGS_H_
#define __SIRFSOC_USB_REGS_H_

/* #define CONFIG_FULL_SPEED_DEVICE */

/* usb registers' base addresses */
#ifdef CONFIG_ARCH_ATLAS7
#define USB0_REG_BASE		0x17060000
#define USB1_REG_BASE		0x17070000
#else
#define USB0_REG_BASE		0xb8000000
#define USB1_REG_BASE		0xb8010000
#endif

/** USB controller registers **/

/* Identification Registers */
#define ID                      0x000
#define HWGENERAL               0x004
#define HWHOST                  0x008
#define HWDEVICE                0x00c
#define HWTXBUF                 0x010
#define HWRXBUF                 0x014
#define SBUSCFG                 0x090

/* Device/Host Timer Registers (Non-EHCI) */
#define GPTIMER0LD              0x080
#define GPTIMER0CTRL            0x084
#define GPTIMER1LD              0x088
#define GPTIMER1CTRL            0x08c

/* Device/Host Capability Registers */
#define CAPLENGTH               0x100
#define HCIVERSION              0x102
#define HCSPARAMS               0x104
#define HCCPARAMS               0x108
#define DCIVERSION              0x120
#define DCCPARAMS               0x124

/* Device/Host Operational Registers */
#define USBCMD                  0x140
#define USBSTS                  0x144
#define USBINTR                 0x148
#define FRINDX                  0x14C
#define DEVICEADDR              0x154
#define PERIODICLISTBASE        0x154
#define ENDPOINTLISTADDR        0x158
#define ASYNCLISTADDR           0x158
#define TTCTRL                  0x15C
#define BURSTSIZE               0x160
#define TXFILLTUNNING           0x164
#define ULPIVIEWPORT            0x170
#define ENDPTNAK                0x178
#define ENDPTNAKEN              0x17C
#define PORTSC1                 0x184
#define OTGSC                   0x1A4
#define USBMODE                 0x1A8
#define ENDPTSETUPSTAT          0x1AC
#define ENDPTPRIME              0x1B0
#define ENDPTFLUSH              0x1B4
#define ENDPTSTATUS             0x1B8
#define ENDPTCOMPLETE           0x1BC
#define ENDPTCTRL0              0x1C0
#define ENDPTCTRLN              0x1C4
#define USBOTG_PHY              0x200

/* DCCPARAMS Register */
#define DCCPARAMS_RESERVE_MASK          0xFFFFFE60

#define DCCPARAMS_DEN_MASK              0x0000001F
#define DCCPARAMS_DEN_OFFSET            0

#define DCCPARAMS_DC_MASK               0x00000080
#define DCCPARAMS_DC_OFFSET             7

#define DCCPARAMS_HC_MASK               0x00000100
#define DCCPARAMS_HC_OFFSET             8

/* USBCMD Register */
#define USBCMD_RESERVE_MASK             0xFF004400

#define USBCMD_RS_MASK                  0x00000001
#define USBCMD_RS_OFFSET                0

#define USBCMD_RST_MASK                 0x00000002
#define USBCMD_RST_OFFSET               1

#define USBCMD_FS0_MASK                 0x00000004
#define USBCMD_FS0_OFFSET               2

#define USBCMD_FS1_MASK                 0x00000008
#define USBCMD_FS1_OFFSET               3

#define USBCMD_PSE_MASK                 0x00000010
#define USBCMD_PSE_OFFSET               4

#define USBCMD_ASE_MASK                 0x00000020
#define USBCMD_ASE_OFFSET               5

#define USBCMD_IAA_MASK                 0x00000040
#define USBCMD_IAA_OFFSET               6

#define USBCMD_LR_MASK                  0x00000080
#define USBCMD_LR_OFFSET                7

#define USBCMD_ASP0_MASK                0x00000100
#define USBCMD_ASP0_OFFSET              8

#define USBCMD_ASP1_MASK                0x00000200
#define USBCMD_ASP1_OFFSET              9

#define USBCMD_ASPE_MASK                0x00000800
#define USBCMD_ASPE_OFFSET              11

#define USBCMD_ATDTW_MASK               0x00001000
#define USBCMD_ATDTW_OFFSET             12

#define USBCMD_SUTW_MASK                0x00002000
#define USBCMD_SUTW_OFFSET              13

#define USBCMD_FS2_MASK                 0x00008000
#define USBCMD_FS2_OFFSET               15

#define USBCMD_ITC_MASK                 0x00FF0000
#define USBCMD_ITC_OFFSET               16

/* USBSTATUS Register */
#define USBSTS_RESERVE_MASK             0xFCF3Fa00

#define USBSTS_UI_MASK                  0x00000001
#define USBSTS_UI_OFFSET                0

#define USBSTS_UEI_MASK                 0x00000002
#define USBSTS_UEI_OFFSET               1

#define USBSTS_PCI_MASK                 0x00000004
#define USBSTS_PCI_OFFSET               2

#define USBSTS_FRI_MASK                 0x00000008
#define USBSTS_FRI_OFFSET               3

#define USBSTS_SEI_MASK                 0x00000010
#define USBSTS_SEI_OFFSET               4

#define USBSTS_AAI_MASK                 0x00000020
#define USBSTS_AAI_OFFSET               5

#define USBSTS_URI_MASK                 0x00000040
#define USBSTS_URI_OFFSET               6

#define USBSTS_SRI_MASK                 0x00000080
#define USBSTS_SRI_OFFSET               7

#define USBSTS_SLI_MASK                 0x00000100
#define USBSTS_SLI_OFFSET               8

#define USBSTS_ULPII_MASK               0x00000400
#define USBSTS_ULPII_OFFSET             10

#define USBSTS_HCH_MASK                 0x00001000
#define USBSTS_HCH_OFFSET               12

#define USBSTS_RCL_MASK                 0x00002000
#define USBSTS_RCL_OFFSET               13

#define USBSTS_PS_MASK                  0x00004000
#define USBSTS_PS_OFFSET                14

#define USBSTS_AS_MASK                  0x00008000
#define USBSTS_AS_OFFSET                15

#define USBSTS_NAKI_MASK                0x00010000
#define USBSTS_NAKI_OFFSET              16

#define USBSTS_UAI_MASK                 0x00040000
#define USBSTS_UAI_OFFSET               18

#define USBSTS_UPI_MASK                 0x000800000
#define USBSTS_UPI_OFFSET               19

#define USBSTS_TI0_MASK                 0x00100000
#define USBSTS_TI0_OFFSET               24

#define USBSTS_TI1_MASK                 0x00200000
#define USBSTS_TI1_OFFSET               22

/* USBINTR Register */
#define USBINTR_RESERVE_MASK            0xFCF2FA00

#define USBINTR_UE_MASK                 0x00000001
#define USBINTR_UE_OFFSET               0

#define USBINTR_UEE_MASK                0x00000002
#define USBINTR_UEE_OFFSET              1

#define USBINTR_PCE_MASK                0x00000004
#define USBINTR_PCE_OFFSET              2

#define USBINTR_FRE_MASK                0x00000008
#define USBINTR_FRE_OFFSET              3

#define USBINTR_SEE_MASK                0x00000010
#define USBINTR_SEE_OFFSET              4

#define USBINTR_AAE_MASK                0x00000020
#define USBINTR_AAE_OFFSET              5

#define USBINTR_URE_MASK                0x00000040
#define USBINTR_URE_OFFSET              6

#define USBINTR_SRE_MASK                0x00000080
#define USBINTR_SRE_OFFSET              7

#define USBINTR_SLE_MASK                0x00000100
#define USBINTR_SLE_OFFSET              8

#define USBINTR_ULPIE_MASK              0x00000400
#define USBINTR_ULPIE_OFFSET            10

#define USBINTR_NAKE_MASK               0x00010000
#define USBINTR_NAKE_OFFSET             16

#define USBINTR_UAIE_MASK               0x00040000
#define USBINTR_UAIE_OFFSET             18

#define USBINTR_TIE0_MASK               0x01000000
#define USBINTR_TIE0_OFFSET             24

#define USBINTR_TIE1_MASK               0x02000000
#define USBINTR_TIE1_OFFSET             25

/* FRINDX Register */
#define FRINDX_RESERVE_MASK             0xFFFFC000

#define FRINDX_FRINDEX_MASK             0x00003FFF
#define FRINDX_FRINDEX_OFFSET           0

/* DEVICEADDR Register */
#define DEVICEADDR_RESERVE_MASK         0x00FFFFFF

#define DEVICEADDR_USBADRA_MASK         0x01000000
#define DEVICEADDR_USBADRA_OFFSET       24

#define DEVICEADDR_DEVICE_ADDR_MASK     0xFE000000
#define DEVICEADDR_DEVICE_ADDR_OFFSET   25

/*  ENDPOINTLISTADDR Register */
#define ENDPOINTLISTADDR_RESERVE_MASK   0x000007FF

#define ENDPOINTLISTADDR_EPBASE_MASK    0xFFFFF800
#define ENDPOINTLISTADDR_EPBASE_OFFSET  0

/* ENDPTNAK Register */
#define ENDPTNAK_RESERVE_RESET          0

#define ENDPTNAK_EPRN_MASK              0x0000FFFF
#define ENDPTNAK_EPRN_OFFSET            0

#define ENDPTNAK_EPTN_MASK              0xFFFF0000
#define ENDPTNAK_EPTN_OFFSET            16


/* ENDPTNAKEN Register */
#define ENDPTNAKEN_RESERVE_MASK         0

#define ENDPTNAKEN_EPRNE_MASK           0x0000FFFF
#define ENDPTNAKEN_EPRNE_OFFSET         0

#define ENDPTNAKEN_EPTNE_MASK           0xFFFF0000
#define ENDPTNAKEN_EPTNE_OFFSET         0

/* PORTSC1 Register */
#define PORTSC1_RESERVE_MASK            0x02000000

#define PORTSC1_CCS_MASK                0x00000001
#define PORTSC1_CCS_OFFSET              0

#define PORTSC1_CSC_MASK                0x00000002
#define PORTSC1_CSC_OFFSET              1

#define PORTSC1_PE_MASK                 0x00000004
#define PORTSC1_PE_OFFSET               2

#define PORTSC1_PEC_MASK                0x00000008
#define PORTSC1_PEC_OFFSET              3

#define PORTSC1_OCA_MASK                0x00000010
#define PORTSC1_OCA_OFFSET              4

#define PORTSC1_OCC_MASK                0x00000020
#define PORTSC1_OCC_OFFSET              5

#define PORTSC1_FPR_MASK                0x00000040
#define PORTSC1_FPR_OFFSET              6

#define PORTSC1_SUSP_MASK               0x00000080
#define PORTSC1_SUSP_OFFSET             7

#define PORTSC1_PR_MASK                 0x00000100
#define PORTSC1_PR_OFFSET               8

#define PORTSC1_HSP_MASK                0x00000200
#define PORTSC1_HSP_OFFSET              9

#define PORTSC1_LS_MASK                 0x00000C00
#define PORTSC1_LS_OFFSET               10

#define PORTSC1_PP_MASK                 0x00001000
#define PORTSC1_PP_OFFSET               12

#define PORTSC1_PO_MASK                 0x00002000
#define PORTSC1_PO_OFFSET               13

#define PORTSC1_PIC_MASK                0x0000C000
#define PORTSC1_PIC_OFFSET              14

#define PORTSC1_PTC_MASK                0x000F0000
#define PORTSC1_PTC_OFFSET              16

#define PORTSC1_WKCN_MASK               0x00100000
#define PORTSC1_WKCN_OFFSET             20

#define PORTSC1_WKDC_MASK               0x00200000
#define PORTSC1_WKDC_OFFSET             21

#define PORTSC1_WKOC_MASK               0x00400000
#define PORTSC1_WKOC_OFFSET             22

#define PORTSC1_PHCD_MASK               0x00800000
#define PORTSC1_PHCD_OFFSET             23

#define PORTSC1_PFSC_MASK               0x01000000
#define PORTSC1_PFSC_OFFSET             24

#define PORTSC1_PSPD_MASK               0x0C000000
#define PORTSC1_PSPD_OFFSET             26

#define PORTSC1_PTW_MASK                0x10000000
#define PORTSC1_PTW_OFFSET              28

#define PORTSC1_STS_MASK                0x20000000
#define PORTSC1_STS_OFFSET              29

#define PORTSC1_PTS_MASK                0xC0000000
#define PORTSC1_PTS_OFFSET              30

/*  OTGSC Register */
#define OTGSC_RESERVE_MASK              0x80808000

#define OTGSC_VD_MASK                   0x00000001
#define OTGSC_VD_OFFSET                 0

#define OTGSC_VC_MASK                   0x00000002
#define OTGSC_VC_OFFSET                 1

#define OTGSC_HAAR_MASK                 0x00000004
#define OTGSC_HAAR_OFFSET               2

#define OTGSC_OT_MASK                   0x00000008
#define OTGSC_OT_OFFSET                 3

#define OTGSC_DP_MASK                   0x00000010
#define OTGSC_DP_OFFSET                 4

#define OTGSC_IDPU_MASK                 0x00000020
#define OTGSC_IDPU_OFFSET               5

#define OTGSC_HADP_MASK                 0x00000040
#define OTGSC_HADP_OFFSET               6

#define OTGSC_HABA_MASK                 0x00000080
#define OTGSC_HABA_OFFSET               7

#define OTGSC_ID_MASK                   0x00000100
#define OTGSC_ID_OFFSET                 8

#define OTGSC_AVV_MASK                  0x00000200
#define OTGSC_AVV_OFFSET                9

#define OTGSC_ASV_MASK                  0x00000400
#define OTGSC_ASV_OFFSET                10

#define OTGSC_BSV_MASK                  0x00000800
#define OTGSC_BSV_OFFSET                11

#define OTGSC_BSE_MASK                  0x00001000
#define OTGSC_BSE_OFFSET                12

#define OTGSC_1MST_MASK                 0x00002000
#define OTGSC_1MST_OFFSET               13

#define OTGSC_DPS_MASK                  0x00004000
#define OTGSC_DPS_OFFSET                14

#define OTGSC_IDIS_MASK                 0x00010000
#define OTGSC_IDIS_OFFSET               16

#define OTGSC_AVVIS_MASK                0x00020000
#define OTGSC_AVVIS_OFFSET              17

#define OTGSC_ASVIS_MASK                0x00040000
#define OTGSC_ASVIS_OFFSET              18

#define OTGSC_BSVIS_MASK                0x00080000
#define OTGSC_BSVIS_OFFSET              19

#define OTGSC_BSEIS_MASK                0x00100000
#define OTGSC_BSEIS_OFFSET              20

#define OTGSC_1MSS_MASK                 0x00200000
#define OTGSC_1MSS_OFFSET               21

#define OTGSC_DPIS_MASK                 0x00400000
#define OTGSC_DPIS_OFFSET               22

#define OTGSC_IDIE_MASK                 0x01000000
#define OTGSC_IDIE_OFFSET               24

#define OTGSC_AVVIE_MASK                0x02000000
#define OTGSC_AVVIE_OFFSET              25

#define OTGSC_ASVIE_MASK                0x04000000
#define OTGSC_ASVIE_OFFSET              26

#define OTGSC_BSVIE_MASK                0x08000000
#define OTGSC_BSVIE_OFFSET              27

#define OTGSC_BSEIE_MASK                0x10000000
#define OTGSC_BSEIE_OFFSET              28

#define OTGSC_1MSE_MASK                 0x20000000
#define OTGSC_1MSE_OFFSET               29

#define OTGSC_DPIE_MASK                 0x40000000
#define OTGSC_DPIE_OFFSET               30

/* USBMOD Register */
#define USBMODE_RESERVE_MASK            0xFFFFFFC0

#define USBMODE_CM_MASK                 0x00000003
#define USBMODE_CM_OFFSET               0

#define USBMODE_ES_MASK                 0x00000004
#define USBMODE_ES_OFFSET               2

#define USBMODE_SLOM_MASK               0x00000008
#define USBMODE_SLOM_OFFSET             3

#define USBMODE_SDIS_MASK               0x00000010
#define USBMODE_SDIS_OFFSET             4

#define USBMODE_VBPS_MASK               0x00000020
#define USBMODE_VBPS_OFFSET             5

/* ENDPTSETUPSTAT Register */
#define ENDPTSETUPSTAT_RESERVE_MASK		0xFFFF0000

#define ENDPTSETUPSTAT_ENDPSETUPSTAT_MASK       0x0000FFFF
#define ENDPTSETUPSTAT_ENDPSETUPSTAT_OFFSET     0

/*  ENDPTPRIME Register */
#define ENDPTPRIME_RESERVE_MASK         0x00000000

#define ENDPTPRIME_PERB_MASK            0x0000FFFF
#define ENDPTPRIME_PERB_OFFSET          0

#define ENDPTPRIME_PETB_MASK            0xFFFF0000
#define ENDPTPRIME_PETB_OFFSET          16

/* ENDPTFLUSH Register */
#define ENDPTFLUSH_RESERVE_MASK         0x00000000

#define ENDPTFLUSH_FERB_MASK            0x0000FFFF
#define ENDPTFLUSH_FERB_OFFSET          0

#define ENDPTFLUSH_FETB_MASK            0xFFFF0000
#define ENDPTFLUSH_FETB_OFFSET          16

/* ENDPTSTAT  Register */
#define ENDPTSTATUS_RESERVE_MASK        0x00000000

#define ENDPTSTATUS_ERBR_MASK           0x0000FFFF
#define ENDPTSTATUS_ERBR_OFFSET         0

#define ENDPTSTATUS_ETBR_MASK           0xFFFF0000
#define ENDPTSTATUS_ETBR_OFFSET         16

/* ENDPTCOMPLETE Register */
#define ENDPTCOMPLETE_RESERVE_MASK      0x00000000

#define ENDPTCOMPLETE_ERCE_MASK         0x0000FFFF
#define ENDPTCOMPLETE_ERCE_OFFSET       0

#define ENDPTCOMPLETE_ETCE_MASK         0xFFFF0000
#define ENDPTCOMPLETE_ETCE_OFFSET       16


/* ENDPTCTRL0 Register */
#define ENPTCTRLI0_RESERVE_MASK         0xFF72FF72

#define ENDPTCTRL0_RXS_MASK             0x00000001
#define ENDPTCTRL0_RXS_OFFSET           0

#define ENDPTCTRL0_RXT_MASK             0x0000000C
#define ENDPTCTRL0_RXT_OFFSET           2

#define ENDPTCTRL0_TXS_MASK             0x00010000
#define ENDPTCTRL0_TXS_OFFSET           16

#define ENDPTCTRL0_TXT_MASK             0x000C0000
#define ENDPTCTRL0_TXT_OFFSET           18


/* ENDPTCTRLN Register */
#define ENDPTCTRLN_RESERVE_MASK         0xFF10FF10

#define ENDPTCTRLN_RXS_MASK             0x00000001
#define ENDPTCTRLN_RXS_OFFSET           0


#define ENDPTCTRLN_RXD_MASK             0x00000002
#define ENDPTCTRLN_RXD_OFFSET           1


#define ENDPTCTRLN_RXT_MASK             0x0000000C
#define ENDPTCTRLN_RXT_OFFSET           2


#define ENDPTCTRLN_RXI_MASK             0x00000020
#define ENDPTCTRLN_RXI_OFFSET           5

#define ENDPTCTRLN_RXR_MASK             0x00000040
#define ENDPTCTRLN_RXR_OFFSET           6

#define ENDPTCTRLN_RXE_MASK             0x00000080
#define ENDPTCTRLN_RXE_OFFSET           7

#define ENDPTCTRLN_TXS_MASK             0x00010000
#define ENDPTCTRLN_TXS_OFFSET           16

#define ENDPTCTRLN_TXD_MASK             0x00020000
#define ENDPTCTRLN_TXD_OFFSET           17

#define ENDPTCTRLN_TXT_MASK             0x000C0000
#define ENDPTCTRLN_TXT_OFFSET           18

#define ENDPTCTRLN_TXI_MASK             0x00200000
#define ENDPTCTRLN_TXI_OFFSET           21

#define ENDPTCTRLN_TXR_MASK             0x00400000
#define ENDPTCTRLN_TXR_OFFSET           22

#define ENDPTCTRLN_TXE_MASK             0x00800000
#define ENDPTCTRLN_TXE_OFFSET           23

#define CTRL_PHYS_ADDR_QH	        USB_RUNTIME_MEM_BASE
#define CTRL_PHYS_ADDR_TD		(CTRL_PHYS_ADDR_QH + 0x1000)
#define CTRL_PHYS_ADDR_DATA_OUT		(CTRL_PHYS_ADDR_TD + 0x1000)
#define CTRL_PHYS_ADDR_DATA_IN		(CTRL_PHYS_ADDR_DATA_OUT + 0x1000)

#define USB_VIRT_TO_PHYS(addr)		(u32)(addr)

/* Other defines */
/* Max buffer supported by  TD */
#define TD_MAX_BUF_PTR			5
/* Each buffer size can be of 4K */
#define MAX_TD_PER_BUF_SIZE		0x1000
#define MAX_TD_BUF_SIZE		((MAX_TD_PER_BUF_SIZE) * (TD_MAX_BUF_PTR - 1))

#define MAX_LOOP_CNT			0xFFF
#define USB_MAX_CTRL_PAYLOAD		64

#ifdef CONFIG_FULL_SPEED_DEVICE
#define USB_MAX_NON_CTRL_PAYLOAD	64
#else
#define USB_MAX_NON_CTRL_PAYLOAD	512
#endif

#define MAX_ENDPOINTS			5
#define MAX_TD				MAX_ENDPOINTS

/* Endpoint parameters */
#define EP0_MAX_PACKET_SIZE		64
#define UDC_OUT_PACKET_SIZE		USB_MAX_NON_CTRL_PAYLOAD
#define UDC_IN_PACKET_SIZE		USB_MAX_NON_CTRL_PAYLOAD
#define UDC_BULK_PACKET_SIZE		USB_MAX_NON_CTRL_PAYLOAD

#define BULK_OUT_EP_NUM			1
#define BULK_IN_EP_NUM			2

#define DTD_ITEM(tdaddr, item) \
	((struct dTD *)(((u32)tdaddr) + (sizeof(struct dTD) * item)))

/* Register operations */

#define WRITE_REG(base, val, offset)	writel((u32)val, (u32)(base + offset))
#define READ_REG(base, offset)		readl((u32)(base + offset))

#define WRITE_EP_REG(base, val, ep_num)	\
		writel((u32)val, (u32)(base + ENDPTCTRL0 + (ep_num * 0x4)))
#define READ_EP_REG(base, ep_num)	\
		readl((u32)base + ENDPTCTRL0 + (ep_num * 0x4))

#define GET_CMP(r, rv, c)       (((rv) & r##_##c##_MASK) >> r##_##c##_OFFSET)
#define SET_CMP(r, rv, c, v)		\
		(rv = (((rv) & ~r##_##c##_MASK) | ((v) << r##_##c##_OFFSET)))
#define RESERVE_RESET(r, rv)		(rv = ((rv) & ~r##_RESERVE_MASK))

#define ENPT_FLUSH_ALL_EP		0xFFFFFFFF


/* Device Queue head descriptor */
struct dQH {
	u32 reserv1:15;
	u32 ios:1;
	u32 maxPktLen:11;
	u32 reserv2:2;
	u32 zlt:1;
	u32 mult:2;

	u32 reserv3:5;
	u32 currtd_ptr:27;

	u32 terminate:1;
	u32 reserv4:4;
	u32 nexttd_ptr:27;

	u32 status:8;
	u32 reserv5:2;
	u32 multo:2;
	u32 reserv6:3;
	u32 ioc:1;
	u32 total_bytes:15;
	u32 reserv7:1;

	u32 cur_offset:12;
	u32 buffer_ptr:20;

	u32 transfer_overlay[5];
	u32 setup_buffer[2];
	u32 reserved[4];
};

enum qTD_status {
	PCD_TD_ACTIVE = (1 << 7),
	PCD_TD_HALTED = (1 << 6),
	PCD_TD_DATA_BUF_ERR = (1 << 5),
	PCD_TD_TRANS_ERR = (1 << 3),
};

/* device Transfer Descriptor */
struct dTD {
	u32 terminate:1;
	u32 reserv1:4;
	u32 next_link:27;

	u32 status:8;
	u32 reserv2:2;
	u32 multo:2;
	u32 reserv3:3;
	u32 ioc:1;
	u32 total_bytes:15;
	u32 reserv4:1;
	/* make sure the buffer specified is physical */
	u32 buf_ptr[TD_MAX_BUF_PTR];
	u32 reserv5;		/* To make the structure 32 byte aligned */
};

/* Control Endpoint Phases */
#define EPC_IDLE		1
#define EPC_IN_DATA_PHASE	2
#define EPC_OUT_DATA_PHASE	3
#define EPC_IN_STATUS_PHASE	4
#define EPC_OUT_STATUS_PHASE	5

/* Interrupt bitmap */
#define EP_SETUP_INTR		1
#define EP_IN_INTR		2
#define	EP_OUT_INTR	        4
#define	EP_RESET_INTR		8
#define	EP_SUSPEND_INTR		16

struct udc_global_var  {
	struct usb_device_instance *udc_device;
	u8 *dtd_in_buffer;		/* Buffer for Receive data */
	u8 *dtd_data_buffer;		/* Buffer for transmit data */
	struct urb *ep0_urb;		/* Ctrl ep URB */
	u16 max_ep;		/* Maximum ep supported by the controller */
	struct dQH *dQH_ptr;		/* Queue head Base pointer */
	struct dTD *dTD_ptr;		/* TD Base pointer */
	struct dTD *td[MAX_TD];		/* Trasfer descriptor pointer */
	u32 addr;			/* Controller register base address */
	u32 clkbit;
	rx_callback pfrxdone;
	u32 counter;
	u8 usb_wait_for_setup;
	u32 queued_rx_bytes;
};

#endif
