/*
 * Copyright (C) 2014 Cambridge Silicon Radio Limited
 * Xiaomeng Hou <Xiaomeng.Hou@csr.com>
 *
 * SPDX-License-Identifier:    GPL-2.0+
 */


#ifndef __SIRF_LCD_H
#define __SIRF_LCD_H


/* Screen Control Registers Offset address */
#define LCD_S0_HSYNC_PERIOD	0x0000
#define LCD_S0_HSYNC_WIDTH	0x0004
#define LCD_S0_VSYNC_PERIOD	0x0008
#define LCD_S0_VSYNC_WIDTH	0x000C
#define LCD_S0_ACT_HSTART	0x0010
#define LCD_S0_ACT_VSTART	0x0014
#define LCD_S0_ACT_HEND		0x0018
#define LCD_S0_ACT_VEND		0x001C
#define LCD_S0_OSC_RATIO	0x0020
#define LCD_S0_TIM_CTRL		0x0024
#define LCD_S0_TIM_STATUS	0x0028
#define LCD_S0_HCOUNT		0x002C
#define LCD_S0_VCOUNT		0x0030
#define LCD_S0_BLANK		0x0034
#define LCD_S0_BACK_COLOR	0x0038
#define LCD_S0_DISP_MODE	0x003C
#define LCD_S0_LAYER_SEL	0x0040
#define LCD_S0_RGB_SEQ		0x0044
#define LCD_S0_RGB_YUV_COEF1	0x0048
#define LCD_S0_RGB_YUV_COEF2	0x004C
#define LCD_S0_RGB_YUV_COEF3	0x0050
#define LCD_S0_YUV_CTRL		0x0054
#define LCD_S0_TV_FIELD		0x0058
#define LCD_S0_INT_LINE		0x005C
#define LCD_S0_LAYER_STATUS	0x0060
#define LCD_S0_RGB_YUV_OFFSET	0x0070

/* DMA Status Registers Offset address */
#define LCD_DMA_STATUS		0x00F0
#define LCD_INT_MASK		0x00F4
#define LCD_INT_STATUS		0x00F8
#define LCD_SCR_CTRL		0x00FC

/* Layer Control Registers Offset address */
#define LCD_LAYER_CTRL		0x0000
#define LCD_LAYER_HSTART	0x0004
#define LCD_LAYER_VSTART	0x0008
#define LCD_LAYER_HEND		0x000C
#define LCD_LAYER_VEND		0x0010
#define LCD_LAYER_BASE0		0x0014
#define LCD_LAYER_BASE1		0x0018
#define LCD_LAYER_XSIZE		0x001C
#define LCD_LAYER_YSIZE		0x0020
#define LCD_LAYER_SKIP		0x0024
#define LCD_LAYER_DMA_CTRL	0x0028
#define LCD_LAYER_ALPHA		0x002C
#define LCD_LAYER_CKEYB_SRC     0x0030
#define LCD_LAYER_CKEYS_SRC     0x0034
#define LCD_LAYER_FIFO_CHK      0x0038
#define LCD_LAYER_FIFO_STATUS   0x003C
#define LCD_LAYER_CKEYB_DST     0x0050
#define LCD_LAYER_CKEYS_DST     0x0054


#define LCD_LAYER_BASE(x)	(((x) + 1) * 0x100)
#define LCD_CTRL(x)		(LCD_LAYER_BASE(x) + LCD_LAYER_CTRL)
#define LCD_HSTART(x)		(LCD_LAYER_BASE(x) + LCD_LAYER_HSTART)
#define LCD_VSTART(x)		(LCD_LAYER_BASE(x) + LCD_LAYER_VSTART)
#define LCD_HEND(x)		(LCD_LAYER_BASE(x) + LCD_LAYER_HEND)
#define LCD_VEND(x)		(LCD_LAYER_BASE(x) + LCD_LAYER_VEND)
#define LCD_BASE0(x)		(LCD_LAYER_BASE(x) + LCD_LAYER_BASE0)
#define LCD_BASE1(x)		(LCD_LAYER_BASE(x) + LCD_LAYER_BASE1)
#define LCD_XSIZE(x)		(LCD_LAYER_BASE(x) + LCD_LAYER_XSIZE)
#define LCD_YSIZE(x)		(LCD_LAYER_BASE(x) + LCD_LAYER_YSIZE)
#define LCD_SKIP(x)		(LCD_LAYER_BASE(x) + LCD_LAYER_SKIP)
#define LCD_DMA_CTRL(x)		(LCD_LAYER_BASE(x) + LCD_LAYER_DMA_CTRL)
#define LCD_ALPHA(x)		(LCD_LAYER_BASE(x) + LCD_LAYER_ALPHA)
#define LCD_CKEYB_SRC(x)	(LCD_LAYER_BASE(x) + LCD_LAYER_CKEYB_SRC)
#define LCD_CKEYS_SRC(x)	(LCD_LAYER_BASE(x) + LCD_LAYER_CKEYS_SRC)
#define LCD_FIFO_CHK(x)		(LCD_LAYER_BASE(x) + LCD_LAYER_FIFO_CHK)
#define LCD_FIFO_STATUS(x)	(LCD_LAYER_BASE(x) + LCD_LAYER_FIFO_STATUS)
#define LCD_CKEYB_DST(x)	(LCD_LAYER_BASE(x) + LCD_LAYER_CKEYB_DST)
#define LCD_CKEYS_DST(x)	(LCD_LAYER_BASE(x) + LCD_LAYER_CKEYS_DST)


#define PANEL_PCLK_POLAR	(1 << 2)
#define PANEL_PCLK_EDGE		(1 << 3)
#define PANEL_HSYNC_POLAR	(1 << 5)
#define PANEL_VSYNC_POLAR	(1 << 7)

/* OSC ratio bits */
#define HALF_DUTY		(1 << 12)
/* LCD_S0_TIM_CTRL bits */
#define PCLK_IO			(1 << 1) /* Pixel clock master mode */
#define PCLK_POLAR		(1 << 2)
#define PCLK_EDGE		(1 << 3)
#define HSYNC_IO		(1 << 4)
#define HSYNC_POLAR		(1 << 5)
#define VSYNC_IO		(1 << 6)
#define VSYNC_POLAR		(1 << 7)
/* RGB sequence */
#define RGB_SEQ_RGB		0x186
#define RGB_SEQ_BGR		0x924
/* Blank reg */
#define BLANK_VALID		(1 << 24)
/* Vsync Width in lines */
#define WIDTH_UNIT_LINES	(1 << 12)
/* LCD_S0_DISP_MODE bits */
#define FRAME_VALID		0x01
#define OUT_FORMAT_RGBRGB	0x00
#define OUT_FORMAT_8YUV422	0x02
#define OUT_FORMAT_YUV422	0x04
#define OUT_FORMAT_RGB666	0x06
#define OUT_FORMAT_RGB888	0x08
#define TOP_LAYER_MASK		(7 << 4)
#define TOP_LAYER(x)		((x) << 4)
/* Screen ctrl */
#define LCD_SCRN_EN		(1 << 0)
/* LCD_S0_INT_LINE bits */
#define INT_LINE_VALID		(1 << 12)
/* LCD_S0_YUV_CTRL bits */
#define YUV_SEQ_YUYV		(0 << 6)
#define YUV_SEQ_YVYU		(1 << 6)
#define YUV_SEQ_UYVY		(2 << 6)
#define YUV_SEQ_VYUY		(3 << 6)
#define EVEN_UV			(1 << 9)

/* DMA CTRL bits */
#define DMA_START		(1 << 0)
#define DMA_CONTINUOUS_MODE	(1 << 1)
/* LCD_DMA_STATUS bits */
#define LAYER_DMA_EN(x)		(1 << (x))

/* LCD_Lx_CTRL bits */
#define BPP_RGB666		0x000
#define BPP_RGB565		0x001
#define BPP_RGB556		0x002
#define BPP_RGB655		0x003
#define BPP_RGB888		0x004
#define BPP_TRGB888		0x005
#define BPP_ARGB8888		0x006
#define CTRL_FIFO_RESET		0x020
#define CTRL_CKEY_EN		0x040
#define CTRL_FIFO_FKRDY		(1 << 7)
#define CTRL_CONFIRM		(1 << 8)
#define CTRL_REPLICATE		(1 << 10)
#define CRTL_PREMULTI_ALPHA	(1 << 12)
/* LCD_S0_LAYER_SEL bits */
#define LAYER_ENABLE(x)		(1 << (x))


struct video_mode {
	unsigned long pixclock; /* pixel clock in HZ */

	/* Timing: all values in pixclocks */
	u32 hactive;
	u32 hfront_porch;
	u32 hback_porch;
	u32 hsync_len;

	u32 vactive;
	u32 vfront_porch;
	u32 vback_porch;
	u32 vsync_len;
};

/*
 * LCD controller structure for SiRF SoC
 */
struct vidinfo {
	struct video_mode	mode;
	u32			timing;
	unsigned int		bpp;
	unsigned long		smem_start; /* start of frame buffer mem */
	void __iomem		*base;      /* start of controller address */
	unsigned long		controller_clock;

	unsigned int		vcc;
	unsigned int		vdd;
	unsigned int		vee;
};

#endif
