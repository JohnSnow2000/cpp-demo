/*
 * Copyright (c) 2012-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <asm/io.h>
#include <asm/arch/uart.h>
#include <asm/arch/clkc.h>
#include <serial.h>
#include <watchdog.h>

DECLARE_GLOBAL_DATA_PTR;

#define CONFIG_CONS_INDEX	1

int uart_config(int baudrate)
{
	return 0;
}

static void uart_putc(const char c)
{
	while (readl(UART1_TXFIFO_STATUS) & UART1_TXFIFO_FULL)
		WATCHDOG_RESET();
	writel((unsigned long)c, UART1_TXFIFO_DATA);
}

static char uart_getc(void)
{
	while (readl(UART1_RXFIFO_STATUS) & UART1_RXFIFO_EMPTY)
		WATCHDOG_RESET();
	return (char)readl(UART1_RXFIFO_DATA);
}

void _serial_putc(const char c, const int port)
{
	if (c == '\n')
		uart_putc('\r');
	uart_putc(c);
}

void _serial_putc_raw(const char c, const int port)
{
	uart_putc(c);
}

void _serial_puts(const char *s, const int port)
{
	while (*s)
		_serial_putc(*s++, port);
}

int _serial_getc(const int port)
{
	return uart_getc();
}

int _serial_tstc(const int port)
{
	return !(readl(UART1_RXFIFO_STATUS) & UART1_RXFIFO_EMPTY);
}

void _serial_setbrg(const int port)
{
	uart_config(gd->baudrate);
}

void sirfsoc_serial_putc(const char c)
{
	_serial_putc(c, CONFIG_CONS_INDEX);
}

void serial_putc_raw(const char c)
{
	_serial_putc_raw(c, CONFIG_CONS_INDEX);
}

int sirfsoc_serial_getc(void)
{
	return _serial_getc(CONFIG_CONS_INDEX);
}

int sirfsoc_serial_tstc(void)
{
	return _serial_tstc(CONFIG_CONS_INDEX);
}

void sirfsoc_serial_setbrg(void)
{
}


#ifdef CONFIG_ARCH_ATLAS7
void uart0_ini(int baudrate, int stop_bit_num,
		int data_bit_num, int parity_bit, int sample_div, int io_clk)
{
	u32 uart_baud_rate;
	u32 val;

	writel(UART1_TXFIFO_RESET, UART1_TXFIFO_OP);
	writel(UART1_RXFIFO_RESET, UART1_RXFIFO_OP);
	__udelay(5000);
	writel(UART1_TXFIFO_START, UART1_TXFIFO_OP);
	writel(UART1_RXFIFO_START, UART1_RXFIFO_OP);
	writel(CLKC_ROOT_CLK_EN0_SET_GNSSM_IO, CLKC_ROOT_CLK_EN0_SET);
	writel(CLKC_DMAC0_LEAF_UART0_IO_CLKEN, CLKC_DMAC0_LEAF_CLK_EN_SET);
	uart_baud_rate = (io_clk * 2 + baudrate * sample_div)
			/ (baudrate * sample_div * 2) - 1;

	val = uart_baud_rate | ((sample_div - 1) << 16);
	writel(val, UART0_DIVISOR);
	writel(val, UART1_DIVISOR);

	val = data_bit_num | (stop_bit_num << 2) | (parity_bit << 3);
	writel(val, UART0_LINE_CTRL);
	writel(UART0_TX_IO_MODE, UART0_TX_DMA_IO_CTRL);
	writel(UART0_RX_IO_MODE, UART0_RX_DMA_IO_CTRL);
	writel(0, UART0_TX_DMA_IO_LEN);
	writel(0, UART0_RX_DMA_IO_LEN);
	writel(0x08, UART0_TXFIFO_CTRL);
	writel(0x18, UART0_RXFIFO_CTRL);

	writel(0x06 | (0x04 << 10) | (0x02 << 20), UART0_TXFIFO_LEVEL_CHK);
	writel(0x02 | (0x04 << 10) | (0x06 << 20), UART0_RXFIFO_LEVEL_CHK);

	val = readl(UART0_TXRX_ENA_REG);
	val |= UART0_RX_EN | UART0_TX_EN;
	writel(val, UART0_TXRX_ENA_REG);
	writel(val, UART1_TXRX_ENA_REG);
	/*
	writel(UART0_TXFIFO_RESET, UART0_TXFIFO_OP);
	writel(UART0_RXFIFO_RESET, UART0_RXFIFO_OP);
	writel(UART0_TXFIFO_START, UART0_TXFIFO_OP);
	writel(UART0_RXFIFO_START, UART0_RXFIFO_OP);
	*/
	/*clear all pending uart interrupts*/
	writel(UART_INT_MASK_ALL, UART0_INT_STATUS);
	/*set set funcsel rxd_0 txd_0 of uart0*
	val = readl(SW_TOP_FUNC_SEL_19_REG_SET);
	val |= UART0_RXD_0 | UART0_TXD_0;*/
	writel(0x00110011, SW_TOP_FUNC_SEL_19_REG_CLR);
	writel(0x00110011, SW_TOP_FUNC_SEL_19_REG_SET);
	return;
}
#endif
int sirfsoc_serial_init(void)
{
#ifdef ATLAS7_PXP
	uart0_ini(115200*4,
		  UART_1_STOP_BIT,
		  UART_8_DATA_BIT,
		  UART_NO_PARITY,
		  15,
		  120000000);
#elif defined(CONFIG_ARCH_ATLAS7)
	uart0_ini(115200,
		  UART_1_STOP_BIT,
		  UART_8_DATA_BIT,
		  UART_NO_PARITY,
		  15,
		  150000000);
#endif
	return 0;
}

static struct serial_device sirfsoc_serial_drv = {
	.name	= "sirfsoc_serial",
	.start	= sirfsoc_serial_init,
	.stop	= NULL,
	.setbrg	= sirfsoc_serial_setbrg,
	.putc	= sirfsoc_serial_putc,
	.puts	= default_serial_puts,
	.getc	= sirfsoc_serial_getc,
	.tstc	= sirfsoc_serial_tstc,
};

void sirfsoc_serial_initialize(void)
{
	serial_register(&sirfsoc_serial_drv);
}

__weak struct serial_device *default_serial_console(void)
{
	return &sirfsoc_serial_drv;
}
