/*
 * Copyright (C) 2014  Dialog Semiconductor Ltd.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 */


#ifndef DA9061_H
#define DA9061_H

#include <common.h>
#include <asm/io.h>
/*#include <i2c.h>*/
#include <asm/arch/i2c.h>

#define DLG_DBG(fmt, args...)

/* I2C chip addresses */
#define DA9061_I2C_SLAVE	0x58


/******************************************************/
#define DA9061_PMIC_CHIP_ID		0x62
/* #define DA9061_PMIC_VARIANT_ID_AA	0x10 */
#define	DA9061_CHIP_VARIANT_SHIFT	4

#define DA9061_I2C_PAGE_SEL_SHIFT	1

#define	DA9061_REG_PAGE_CON		0x00

/* System Control and Event Registers */
#define	DA9061_REG_STATUS_A		0x01
#define	DA9061_REG_STATUS_D		0x04
#define	DA9061_REG_FAULT_LOG		0x05
#define	DA9061_REG_EVENT_A		0x06
#define	DA9061_REG_EVENT_B		0x07
#define	DA9061_REG_IRQ_MASK_A		0x0A
#define DA9061_REG_CONTROL_A		0x0E
#define DA9061_REG_CONTROL_B		0x0F
#define	DA9061_REG_CONTROL_D		0x11
#define	DA9061_REG_CONTROL_F		0x13

/* Regulator Control Registers */
#define	DA9061_REG_BUCK1_CONT		0x21
#define	DA9061_REG_BUCK3_CONT		0x22
#define	DA9061_REG_BUCK2_CONT		0x24
#define	DA9061_REG_LDO1_CONT		0x26
#define	DA9061_REG_LDO2_CONT		0x27
#define	DA9061_REG_LDO3_CONT		0x28
#define	DA9061_REG_LDO4_CONT		0x29
#define	DA9061_REG_DVC_1		0x32

/* Regulator Setting Registers */
#define	DA9061_REG_BUCK_ILIM_A		0x9A
#define	DA9061_REG_BUCK_ILIM_B		0x9B
#define	DA9061_REG_BUCK_ILIM_C		0x9C
#define	DA9061_REG_BUCK1_CFG		0x9E
#define	DA9061_REG_BUCK3_CFG		0x9F
#define	DA9061_REG_BUCK2_CFG		0xA0
#define	DA9061_REG_VBUCK1_A		0xA4
#define	DA9061_REG_VBUCK3_A		0xA5
#define	DA9061_REG_VBUCK2_A		0xA7
#define	DA9061_REG_VLDO1_A		0xA9
#define	DA9061_REG_VLDO2_A		0xAA
#define	DA9061_REG_VLDO3_A		0xAB
#define	DA9061_REG_VLDO4_A		0xAC
#define	DA9061_REG_VBUCK1_B		0xB5
#define	DA9061_REG_VBUCK3_B		0xB6
#define	DA9061_REG_VBUCK2_B		0xB8
#define	DA9061_REG_VLDO1_B		0xBA
#define	DA9061_REG_VLDO2_B		0xBB
#define	DA9061_REG_VLDO3_B		0xBC
#define	DA9061_REG_VLDO4_B		0xBD

/* Chip ID and variant */
#define	DA9061_REG_CHIP_ID		0x181
#define	DA9061_REG_CHIP_VARIANT	0x182

/*
 * PMIC registers bits
 */

/* DA9061_REG_STATUS_D */
#define DA9061_LDO1_ILIM		0x01
#define DA9061_LDO2_ILIM		0x02
#define DA9061_LDO3_ILIM		0x04
#define DA9061_LDO4_ILIM		0x08

/* DA9061_REG_FAULT_LOG (addr=0x05) */
#define	DA9061_TWD_ERROR		0x01
#define	DA9061_POR			0x02
#define	DA9061_VDD_FAULT		0x04
#define	DA9061_VDD_START		0x08
#define	DA9061_TEMP_CRIT		0x10
#define	DA9061_KEY_RESET		0x20
#define	DA9061_NRESETREQ		0x40
#define	DA9061_WAIT_SHUT		0x80

/* DA9061_REG_EVENT_A */
#define DA9061_E_NONKEY		0x01
#define DA9061_E_WDG_WARN		0x08
#define DA9061_E_SEQ_RDY		0x10

/* DA9061_REG_EVENT_B */
#define DA9061_E_TEMP			0x02
#define DA9061_E_LDO_LIM		0x08
#define DA9061_E_DVC_RDY		0x20
#define DA9061_E_VDD_WARN		0x80

/* DA9061_REG_EVENT_C */
#define DA9061_E_GPI0			0x01
#define DA9061_E_GPI1			0x02
#define DA9061_E_GPI2			0x04
#define DA9061_E_GPI3			0x08
#define DA9061_E_GPI4			0x10

/* DA9061_REG_IRQ_MASK_A */
#define DA9061_M_NONKEY		0x01
#define DA9061_M_WDG_WARN		0x08
#define DA9061_M_SEQ_RDY		0x10

/* DA9061_REG_IRQ_MASK_B */
#define DA9061_M_TEMP			0x02
#define DA9061_M_LDO_LIM		0x08
#define DA9061_M_DVC_RDY		0x20
#define DA9061_M_VDD_WARN		0x80

/* DA9061_REG_IRQ_MASK_C */
#define DA9061_M_GPI0			0x01
#define DA9061_M_GPI1			0x02
#define DA9061_M_GPI2			0x04
#define DA9061_M_GPI3			0x08
#define DA9061_M_GPI4			0x10

/* DA9061_REG_CONTROL_A	*/
#define DA9061_SYSTEM_EN		0x01

/* DA9061_REG_CONTROL_B	*/
#define DA9061_FREEZE_EN		0x04
#define DA9061_FREEZE_EN_SHIFT	2
#define DA9061_NFREEZE			0x60
#define DA9061_NFREEZE_SHIFT	5

/* DA9061_REG_CONTROL_D */
#define DA9061_TWDSCALE		0x07

/* DA9061_REG_CONTROL_F */
#define DA9061_WATCHDOG		0x01

/* DA9061_REG_BUCK1_CONT */
#define DA9061_BUCK1_EN		0x01

/* DA9061_REG_BUCK2_CONT */
#define DA9061_BUCK2_EN		0x01

/* DA9061_REG_BUCK3_CONT */
#define DA9061_BUCK3_EN		0x01

/* DA9061_REG_LDO1_CONT */
#define DA9061_LDO1_EN			0x01

/* DA9061_REG_LDO2_CONT */
#define DA9061_LDO2_EN			0x01

/* DA9061_REG_LDO3_CONT */
#define DA9061_LDO3_EN			0x01

/* DA9061_REG_LDO4_CONT */
#define DA9061_LDO4_EN			0x01

/* DA9061_REG_DVC_1 */
#define DA9061_VBUCK1_SEL		0x01
#define DA9061_VBUCK3_SEL		0x04
#define DA9061_VBUCK2_SEL		0x08
#define DA9061_VLDO1_SEL		0x10
#define DA9061_VLDO2_SEL		0x20
#define DA9061_VLDO3_SEL		0x40
#define DA9061_VLDO4_SEL		0x80

/* DA9061_REG_BUCK_ILIM_A */
#define DA9061_BUCK2_ILIM		0x0f

/* DA9061_REG_BUCK_ILIM_B */
#define DA9061_BUCK3_ILIM		0x0f

/* DA9061_REG_BUCK_ILIM_C */
#define	DA9061_BUCK1_ILIM		0x0f

/* DA9061_REG_BUCK2_CFG */
#define DA9061_BUCK2_MODE		0xc0

/* DA9061_REG_BUCK1_CFG */
#define DA9061_BUCK1_MODE		0xc0

/* DA9061_REG_BUCK3_CFG */
#define DA9061_BUCK3_MODE		0xc0

/* DA9061_REG_VBUCK2_A */
#define DA9061_VBUCK2_A		0x7f
#define DA9061_VBUCK2_A_BIAS	0
#define DA9061_BUCK2_SL_A		0x80

/* DA9061_REG_VBUCK1_A */
#define DA9061_VBUCK1_A		0x7f
#define DA9061_VBUCK1_A_BIAS	0
#define DA9061_BUCK1_SL_A		0x80

/* DA9061_REG_VBUCK3_A */
#define DA9061_VBUCK3_A		0x7f
#define DA9061_VBUCK3_A_BIAS	0
#define DA9061_BUCK3_SL_A		0x80

/* DA9061_REG_VLDO1_A */
#define DA9061_VLDO1_A			0x3f
#define DA9061_VLD01_A_BIAS	0
#define DA9061_LDO1_SL_A		0x80

/* DA9061_REG_VLDO2_A */
#define DA9061_VLDO2_A			0x3f
#define DA9061_VLD02_A_BIAS	0
#define DA9061_LDO2_SL_A		0x80

/* DA9061_REG_VLDO3_A */
#define DA9061_VLDO3_A			0x3f
#define DA9061_VLD03_A_BIAS	0
#define DA9061_LDO3_SL_A		0x80

/* DA9061_REG_VLDO4_A */
#define DA9061_VLDO4_A			0x3f
#define DA9061_VLD04_A_BIAS	0
#define DA9061_LDO4_SL_A		0x80

/* DA9061_REG_VBUCK2_B */
#define DA9061_BUCK2_SL_B		0x80

/* DA9061_REG_VBUCK1_B */
#define DA9061_BUCK1_SL_B		0x80

/* DA9061_REG_VBUCK3_B */
#define DA9061_BUCK3_SL_B		0x80

/* DA9061_REG_VLDO1_B */
#define DA9061_LDO1_SL_B		0x80

/* DA9061_REG_VLDO2_B */
#define DA9061_LDO2_SL_B		0x80

/* DA9061_REG_VLDO3_B */
#define DA9061_LDO3_SL_B		0x80

/* DA9061_REG_VLDO4_B */
#define DA9061_LDO4_SL_B		0x80
/******************************************************/

/* < Bosch requirement >
 * 4.1.1 Device Detect in u-boot
 * u-boot should implement the driver for getting the chip ID of PMIC
 *	for quick verify the HW connection.
 *
 * u-boot should implement the function for kicking the WDT
 * because the WDT inside the PMIC is enable default
 * when the PMIC enter activate mode.
 * The WDT should be kicked by accessing the register only.
 * u-boot should implement the function for changing the WDT timeout value.
*/

/*	Watchdog functions */
/*
int da9061_wdt_start(unsigned int time);
int da9061_wdt_reset(void);
int da9061_wdt_disable(void);
int da9061_wdt_init(void);
*/

/*	Device ID check function */
/*
int da9061_check_device_ID(void);
*/



#endif /* DA9061_H */
