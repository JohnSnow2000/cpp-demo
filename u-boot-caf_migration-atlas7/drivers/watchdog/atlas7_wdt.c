/*
 * Watchdog driver for the CSR ATLAS7 watchdog
 *
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <watchdog.h>
#include <asm/io.h>
#include <asm/processor.h>
#include <asm/arch/cpu.h>
#include <asm/arch/timer.h>
#include <asm/arch/clkc.h>

/* Hardware timeout in seconds */
#define WDT_HW_TIMEOUT 25

#define A7DA_CPU_CLK_26M	0
#define A7DA_CLK_STABLE BIT(0)

static void atlas7_wdt_init(void)
{
	u32 timer_div;

	timer_div = 0;
	writel(0, ATLAS7_WATCHDOG_CTRL);
	writel(0, TIMER_32COUNTER_WD_CTRL);
	writel(0, ATLAS7_WATCHDOG_COUNTER);
	writel(0, IMER_COUNTER_WD);
	writel(timer_div << 16, ATLAS7_WATCHDOG_CTRL);
	writel(timer_div << 16, TIMER_32COUNTER_WD_CTRL);
}

void hw_watchdog_disable(void)
{
	u32 ctrl;

	ctrl = readl(ATLAS7_WATCHDOG_CTRL);
	ctrl &= ~0x3;
	writel(ctrl, ATLAS7_WATCHDOG_CTRL);
	/* disable watchdog */
	writel(0, TIMER_WATCHDOG_EN);
}

static void atlas7_wdt_set_timeout(unsigned int timeout)
{
	timeout *= CONFIG_IO_CLK_FREQ;
	hw_watchdog_disable();
	/* set watchdog counter initial value */
	writel(0, ATLAS7_WATCHDOG_COUNTER);
	writel(0, IMER_COUNTER_WD);
	/* set watchdog match reg */
	writel(timeout, ATLAS7_WATCHDOG_MATCH);
	writel(0xffffffff, TIMER_MATCH_WD);

	/* kick off watchdog timer */
	writel(readl(ATLAS7_WATCHDOG_CTRL) | 0x3, ATLAS7_WATCHDOG_CTRL);
	/* enable watchdog reset function */
	writel(A7DA_WDOG_EN_BIT, TIMER_WATCHDOG_EN);
}

void  hw_watchdog_init(void)
{
	atlas7_wdt_init();
	atlas7_wdt_set_timeout(WDT_HW_TIMEOUT);
}

void hw_watchdog_reset(void)
{
	atlas7_wdt_set_timeout(WDT_HW_TIMEOUT);
}

void  reset_cpu(unsigned long ignored)
{
	atlas7_wdt_init();
	atlas7_wdt_set_timeout(1);
	while (1)
		cpu_relax();
}
