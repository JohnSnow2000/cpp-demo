/*
 * Drivers for SiRF On-Chip SPI devices
 *
 * Copyright (c) 2013-2014, 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <asm/io.h>
#include <spi.h>
#include <malloc.h>
#include <asm/arch/spi.h>
#include <asm/arch/rst.h>
#include <asm/arch/clk.h>
#include <asm/arch/rsc.h>
#include <asm/arch/gpio.h>
#include <asm/arch/timer.h>
#define SIRF_SPI_DEBUG		1
#define spi_dbg(fmt, arg...)\
	debug_cond(SIRF_SPI_DEBUG, fmt, ##arg)
#define SIRF_MAX_SPI_CS		1
#define SIRF_SPI_FIFO_SIZE	256
#define SIRF_SPI_MAX_TRAN_LEN	(64 * 1024)
#define rd_regl(slave, offset)\
	readl((slave)->regs + offset)
#define wr_regl(slave, offset, value)\
	writel(value, (slave)->regs + offset)
#define MHZ		(1000*1000)
#define SIRF_IO_CLK	(150*MHZ)
#define SIRF_SPI_CLK	(1*MHZ)
#define CLKSPERBYTE     (8*200)
#define SPI_TIMEOUT(bytes)   ((((u64)(bytes+5))*CLKSPERBYTE)/(MHZ/1000000))

enum sirf_cs_mode {
	HARD_CTL_MODE,
	RISC_IO_MODE,
};
struct sirf_spi_slave {
	struct spi_slave slave;
	enum sirf_cs_mode cs_mode;
	void *regs;
	unsigned int max_hz;
	unsigned int mode;
	unsigned int baud_rate;
};
static u64 timeout_value;
void set_timer(u64 bytes)
{
	u64 spi_timeout = SPI_TIMEOUT(bytes);
	timeout_value = spi_timeout + get_timer_masked() + 1;
}

bool is_timeout(void)
{
	return (get_timer_masked() > timeout_value);
}

void spi_init(void)
{
	writel(readl(RESET_SR0) | (1 << 27), RESET_SR0);
	writel(readl(CLKC_CLK_EN1) | (1 << 12), CLKC_CLK_EN1);
	writel(readl(RESET_SR0) & ~(1 << 27), RESET_SR0);
	writel(readl(RSC_PIN_MUX) & ~(1 << 16), RSC_PIN_MUX);
	writel(readl(GPIO_PAD_EN(1)) & ~(0xf << 11), GPIO_PAD_EN(1));
}

struct sirf_spi_slave *to_sirf_spi_slave(struct spi_slave *slave)
{
	return container_of(slave, struct sirf_spi_slave, slave);
}

struct spi_slave *spi_setup_slave(unsigned int bus, unsigned int cs,
		unsigned int max_hz, unsigned int mode)
{
	struct sirf_spi_slave *sirf_slave;

	spi_init();
	if (!spi_cs_is_valid(bus, cs))
		return NULL;
	sirf_slave = spi_alloc_slave(struct sirf_spi_slave, bus, cs);
	if (!sirf_slave)
		return NULL;
	sirf_slave->regs = (void *)SIRFSOC_SPI1_BASE;
	if (max_hz > SIRF_IO_CLK / 2)
		max_hz = SIRF_IO_CLK / 2;
	sirf_slave->max_hz = max_hz;
	sirf_slave->mode = mode;
	sirf_slave->cs_mode = RISC_IO_MODE;

	return &sirf_slave->slave;
}

void spi_free_slave(struct spi_slave *slave)
{
	struct sirf_spi_slave *sirf_slave;
	sirf_slave = to_sirf_spi_slave(slave);
	free(sirf_slave);
}

int spi_claim_bus(struct spi_slave *slave)
{
	struct sirf_spi_slave *sirf_slave;
	unsigned int regval;

	sirf_slave = to_sirf_spi_slave(slave);
	spi_set_speed(slave, sirf_slave->max_hz);
	regval = (SIRF_IO_CLK / (2 * sirf_slave->baud_rate)) - 1;
	regval &= ~SIRFSOC_SPI_SLV_MODE;
	regval &= ~SIRFSOC_SPI_CMD_MODE;
	regval &= ~SIRFSOC_SPI_CS_IO_MODE;
	if (sirf_slave->mode & SPI_CPOL)
		regval |= SIRFSOC_SPI_CLK_IDLE_STAT;
	if (sirf_slave->mode == SPI_MODE_0 || sirf_slave->mode == SPI_MODE_3)
		regval &= ~SIRFSOC_SPI_DRV_POS_EDGE;
	else
		regval |= SIRFSOC_SPI_DRV_POS_EDGE;
	regval |= SIRFSOC_SPI_TRAN_MSB;
	regval |= SIRFSOC_SPI_CS_HOLD_TIME;
	regval |= SIRFSOC_SPI_TRAN_DAT_FORMAT_8;
	regval &= ~SIRFSOC_SPI_ENA_AUTO_CLR;
	regval &= ~SIRFSOC_SPI_MUL_DAT_MODE;
	regval |= SIRFSOC_SPI_CS_IDLE_STAT;
	regval &= ~SIRFSOC_SPI_CMD_MODE;
	if (sirf_slave->cs_mode == RISC_IO_MODE)
		regval |= SIRFSOC_SPI_CS_IO_MODE;
	else
		regval &= ~SIRFSOC_SPI_CS_IO_MODE;
	wr_regl(sirf_slave, SIRFSOC_SPI_CTRL, regval);

	wr_regl(sirf_slave, SIRFSOC_SPI_TXFIFO_LEVEL_CHK,
		(0x30) | (0x20 << 10) | (0x10 << 20));
	wr_regl(sirf_slave, SIRFSOC_SPI_RXFIFO_LEVEL_CHK,
		(10) | (20 << 10) | (30 << 20));
	wr_regl(sirf_slave, SIRFSOC_SPI_TXFIFO_CTRL,
		SIRFSOC_SPI_FIFO_WIDTH_BYTE | 128 << 2);
	wr_regl(sirf_slave, SIRFSOC_SPI_RXFIFO_CTRL,
		SIRFSOC_SPI_FIFO_WIDTH_BYTE | 128 << 2);

	wr_regl(sirf_slave, SIRFSOC_SPI_TX_DMA_IO_CTRL, 1);
	wr_regl(sirf_slave, SIRFSOC_SPI_RX_DMA_IO_CTRL, 1);

	wr_regl(sirf_slave, SIRFSOC_SPI_TX_DMA_IO_LEN, 0);
	wr_regl(sirf_slave, SIRFSOC_SPI_RX_DMA_IO_LEN, 0);

	wr_regl(sirf_slave, SIRFSOC_SPI_DUMMY_DELAY_CTL, 0);
	wr_regl(sirf_slave, SIRFSOC_SPI_INT_EN, 0);
	wr_regl(sirf_slave, SIRFSOC_SPI_INT_STATUS, 0X7FF);

	return 0;
}

void spi_release_bus(struct spi_slave *slave)
{
	struct sirf_spi_slave *sirf_slave = to_sirf_spi_slave(slave);
	wr_regl(sirf_slave, SIRFSOC_SPI_RXFIFO_OP, 0);
	wr_regl(sirf_slave, SIRFSOC_SPI_TXFIFO_OP, 0);
	wr_regl(sirf_slave, SIRFSOC_SPI_TX_RX_EN, 0);
	wr_regl(sirf_slave, SIRFSOC_SPI_INT_EN, 0);
}

int spi_xfer(struct spi_slave *slave, unsigned int bitlen, const void *dout,
		void *din, unsigned long flags)
{
	struct sirf_spi_slave *sirf_slave = to_sirf_spi_slave(slave);
	unsigned int data_len;
	u32 data = 0;
	const u8 *ptx = dout;
	u8 *prx = din;
	int min_val;
	if (!bitlen)
		goto out;
	if (bitlen % 8) {
		flags |= SPI_XFER_END;
		goto out;
	}
	data_len = bitlen / 8;
	if (flags & SPI_XFER_BEGIN)
		spi_cs_activate(slave);
	while (data_len) {
		/* step 1 initial setting include fifo reset, intr_stat clear */
		wr_regl(sirf_slave, SIRFSOC_SPI_TX_RX_EN, 0);
		wr_regl(sirf_slave, SIRFSOC_SPI_TXFIFO_OP,
			SIRFSOC_SPI_FIFO_RESET);
		wr_regl(sirf_slave, SIRFSOC_SPI_TXFIFO_OP,
			SIRFSOC_SPI_FIFO_START);
		wr_regl(sirf_slave, SIRFSOC_SPI_RXFIFO_OP,
			SIRFSOC_SPI_FIFO_RESET);
		wr_regl(sirf_slave, SIRFSOC_SPI_RXFIFO_OP,
			SIRFSOC_SPI_FIFO_START);
		wr_regl(sirf_slave, SIRFSOC_SPI_INT_STATUS,
			SIRFSOC_SPI_INT_MASK_ALL);
		wr_regl(sirf_slave, SIRFSOC_SPI_CTRL,
			rd_regl(sirf_slave, SIRFSOC_SPI_CTRL) |
			SIRFSOC_SPI_ENA_AUTO_CLR);

		/*
		 * step 2 frame len and mode setting:1 byte sigle frame;
		 * >1 byte multi frame.
		 */
		if (data_len == 1)
			wr_regl(sirf_slave, SIRFSOC_SPI_CTRL,
				rd_regl(sirf_slave, SIRFSOC_SPI_CTRL) &
				~SIRFSOC_SPI_MUL_DAT_MODE);
		else
			wr_regl(sirf_slave, SIRFSOC_SPI_CTRL,
				rd_regl(sirf_slave, SIRFSOC_SPI_CTRL) |
				SIRFSOC_SPI_MUL_DAT_MODE);
		min_val = min(256, data_len);
		wr_regl(sirf_slave, SIRFSOC_SPI_TX_DMA_IO_LEN, min_val - 1);
		wr_regl(sirf_slave, SIRFSOC_SPI_RX_DMA_IO_LEN, min_val - 1);

		/* step 3 fill data into TX fifo */
		while (min_val--) {
			if (ptx)
				data = *ptx++;
			else
				data = 0xff;
			wr_regl(sirf_slave, SIRFSOC_SPI_TXFIFO_DATA, data);
		}
		set_timer(data_len);

		/* step 4 enable transfer and check if RX completed */
		wr_regl(sirf_slave, SIRFSOC_SPI_TX_RX_EN, SIRFSOC_SPI_RX_EN |
			SIRFSOC_SPI_TX_EN);
		min_val = min(256, data_len);
		if (min_val == 1)
			while ((rd_regl(sirf_slave, SIRFSOC_SPI_RXFIFO_STATUS) &
				SIRFSOC_SPI_FIFO_EMPTY) && !is_timeout())
				;
		else
			while (!(rd_regl(sirf_slave, SIRFSOC_SPI_INT_STATUS) &
				SIRFSOC_SPI_RX_IO_DMA) && !is_timeout())
				;
		if (is_timeout()) {
			spi_dbg("timeout because no frame end\n");
			break;
		}

		/* step 5 fetch data from RX fifo */
		while (min_val--) {
			data = rd_regl(sirf_slave, SIRFSOC_SPI_RXFIFO_DATA);
			if (prx)
				*prx++ = data;
			data_len--;
		}
	}
out:
	set_timer(3);
	while (!(rd_regl(sirf_slave, SIRFSOC_SPI_INT_STATUS) &
		SIRFSOC_SPI_FRM_END_INT_EN)) {
		if (is_timeout()) {
			spi_dbg("timeout because no frame end\n");
			return -1;
		}
	}
	if (flags & SPI_XFER_END)
		spi_cs_deactivate(slave);
	wr_regl(sirf_slave, SIRFSOC_SPI_TX_RX_EN, 0);

	return 0;
}

int spi_cs_is_valid(unsigned int bus, unsigned int cs)
{
	if (cs)
		return 0;
	return (bus == 1) ? 1 : 0;
}

void spi_cs_activate(struct spi_slave *slave)
{
	struct sirf_spi_slave *sirf_slave;
	unsigned long sirf_spi_cs_idle_stat;

	sirf_slave = to_sirf_spi_slave(slave);
	sirf_spi_cs_idle_stat = rd_regl(sirf_slave, SIRFSOC_SPI_CTRL) &
					SIRFSOC_SPI_CS_IDLE_STAT;
	/* HARD_CTL_MODE mode hardware does */
	if (sirf_slave->cs_mode == RISC_IO_MODE) {
		if (sirf_spi_cs_idle_stat)
			wr_regl(sirf_slave, SIRFSOC_SPI_CTRL,
				rd_regl(sirf_slave, SIRFSOC_SPI_CTRL) &
					~SIRFSOC_SPI_CS_IO_OUT);
		else
			wr_regl(sirf_slave, SIRFSOC_SPI_CTRL,
				rd_regl(sirf_slave, SIRFSOC_SPI_CTRL) |
					SIRFSOC_SPI_CS_IO_OUT);
	}
}

void spi_cs_deactivate(struct spi_slave *slave)
{
	struct sirf_spi_slave *sirf_slave;
	unsigned long sirf_spi_cs_idle_stat;

	sirf_slave = to_sirf_spi_slave(slave);
	sirf_spi_cs_idle_stat = rd_regl(sirf_slave, SIRFSOC_SPI_CTRL) &
					SIRFSOC_SPI_CS_IDLE_STAT;
	if (sirf_slave->cs_mode == RISC_IO_MODE) {
		if (sirf_spi_cs_idle_stat)
			wr_regl(sirf_slave, SIRFSOC_SPI_CTRL,
				rd_regl(sirf_slave, SIRFSOC_SPI_CTRL) |
					SIRFSOC_SPI_CS_IO_OUT);
		else
			wr_regl(sirf_slave, SIRFSOC_SPI_CTRL,
				rd_regl(sirf_slave, SIRFSOC_SPI_CTRL) &
					~SIRFSOC_SPI_CS_IO_OUT);
	}
}

void spi_set_speed(struct spi_slave *slave, uint hz)
{
	struct sirf_spi_slave *sirf_slave;

	sirf_slave = to_sirf_spi_slave(slave);
	if (hz > sirf_slave->max_hz)
		hz = sirf_slave->max_hz;
	sirf_slave->baud_rate = hz;
}
