/*
 *
 *
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <asm/io.h>
#include <asm/arch/cpu.h>

#define A7DA_PWRC_TESTANDSET_0	0x188D0078
#define request_hwlock()	while (!readl(A7DA_PWRC_TESTANDSET_0))
#define release_hwlock()	writel(1, A7DA_PWRC_TESTANDSET_0)

u32 sirfsoc_rtc_iobrg_unsafe_readl(u32 addr)
{
	u32 val;

	writel(addr, CPU_IO_BRIDGE + CPUIOBRG_ADDR);
	writel(0x00, CPU_IO_BRIDGE + CPUIOBRG_WRBE);
	writel(0x01, CPU_IO_BRIDGE + CPUIOBRG_CTRL);
	while (readl(CPU_IO_BRIDGE + CPUIOBRG_CTRL))
		;
	val = readl(CPU_IO_BRIDGE + CPUIOBRG_DATA);

	return val;
}

void sirfsoc_rtc_iobrg_unsafe_writel(u32 val, u32 addr)
{
	writel(0xf1, CPU_IO_BRIDGE + CPUIOBRG_WRBE);
	writel(addr, CPU_IO_BRIDGE + CPUIOBRG_ADDR);
	writel(val, CPU_IO_BRIDGE + CPUIOBRG_DATA);
	writel(0x1, CPU_IO_BRIDGE + CPUIOBRG_CTRL);
	while (readl(CPU_IO_BRIDGE + CPUIOBRG_CTRL))
		;
}

u32 sirfsoc_rtc_iobrg_readl(u32 addr)
{
	u32 val;

	request_hwlock();
	val = sirfsoc_rtc_iobrg_unsafe_readl(addr);
	release_hwlock();
	return val;
}

void sirfsoc_rtc_iobrg_writel(u32 val, u32 addr)
{
	request_hwlock();
	sirfsoc_rtc_iobrg_unsafe_writel(val, addr);
	release_hwlock();
}
