/*
 * Copyright (c) 2013-2014, 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <asm/io.h>
#include <asm/arch/timer.h>
#include <asm/system.h>

DECLARE_GLOBAL_DATA_PTR;

#ifndef CONFIG_SYS_DCACHE_OFF
void enable_caches(void)
{
#ifdef CONFIG_CSRVISOR_SUPPORT
	/*
	 * if CSRVISOR is enabled, we put security mode page table to SRAM
	 * and avoid monitor codes hold DDR memory
	 * this makes the whole DDR available to Linux in NS mode
	 */
	gd->arch.tlb_addr = CONFIG_SECURITY_TLB_ADDR;
#endif
	/* Enable D-cache. I-cache is already enabled in start.S */
	dcache_enable();
}

static void sirfsoc_fw_mem_remap(u32 vaddr_base, u32 paddr_base,
		u32 size, enum dcache_option opt)
{
	u32 i, start_idx, end_idx;
	u32 *page_table = (u32 *)gd->arch.tlb_addr;

	start_idx = vaddr_base >> 20;
	end_idx = (vaddr_base + size) >> 20;

	for (i = start_idx; i < end_idx; i++)
		page_table[i] = (page_table[i] & 0xFFFF0UL) |
			(paddr_base + ((i - start_idx) << 20)) | opt;
}

void dram_bank_mmu_setup(int bank)
{
#define ARM_SECTION_BIT_NS              (1 << 19)
	unsigned long dram_map_size = PHYS_DRAM_SIZE;
	unsigned long dram_map_attr = DCACHE_WRITEBACK;

#ifdef CONFIG_SYS_MEM_TOP_HIDE
	dram_map_size -= CONFIG_SYS_MEM_TOP_HIDE;
#endif
#ifdef CONFIG_CSRVISOR_SUPPORT
	dram_map_attr |= ARM_SECTION_BIT_NS;
#endif
	sirfsoc_fw_mem_remap(PHYS_DRAM_BASE, PHYS_DRAM_BASE, dram_map_size,
			     dram_map_attr);

#ifdef CONFIG_SIRF_NANDDISK
	sirfsoc_fw_mem_remap(CONFIG_NANDDISK_ENTRY_VADDR,
			     CONFIG_NANDDISK_ENTRY_PADDR,
			     CONFIG_NANDDISK_CODE_SIZE,
			     DCACHE_WRITEBACK);

	sirfsoc_fw_mem_remap(CONFIG_NANDDISK_DMA_BUF_VADDR,
			     CONFIG_NANDDISK_DMA_BUF_PADDR,
			     CONFIG_NANDDISK_DMA_BUF_SIZE,
			     DCACHE_OFF);
#endif
#ifdef CONFIG_SIRF_UMS
#ifdef CONFIG_ARCH_ATLAS7
	sirfsoc_fw_mem_remap(USB_RUNTIME_MEM_BASE,
			     USB_RUNTIME_MEM_BASE,
			     USB_RUNTIME_MEM_SIZE,
			     DCACHE_OFF);
#else
	sirfsoc_fw_mem_remap(USB_RUNTIME_MEM_BASE,
			     USB_RUNTIME_MEM_BASE - KERNEL_MEM_BASE,
			     USB_RUNTIME_MEM_SIZE,
			     DCACHE_OFF);
#endif
#endif
#ifdef CONFIG_CSRVISOR_SUPPORT
	sirfsoc_fw_mem_remap(CONFIG_CSRVISOR_VADDR,
			     CONFIG_CSRVISOR_PADDR,
			     CONFIG_CSRVISOR_MEM_SIZE,
			     DCACHE_WRITEBACK);
#endif
}
#endif

void sirfsoc_set_watchdog(u32 timeout, int enable)
{
	u32 timeout_ticks;

#if defined(CONFIG_ARCH_MARCO) || defined(CONFIG_ARCH_ATLAS7)
	if (timeout > 0xFFFFFFFFUL / CONFIG_IO_CLK_FREQ)
		timeout_ticks = 0xFFFFFFFFUL;
	else
		timeout_ticks = timeout * CONFIG_IO_CLK_FREQ;
#else
	timeout_ticks = timeout * 100000000;
#endif

	writel(timeout_ticks, TIMER_MATCH_5);
#if defined(CONFIG_ARCH_MARCO) || defined(CONFIG_ARCH_ATLAS7)
	writel(enable | 0x2, TIMER_32COUNTER_5_CTRL);
#else
	writel((enable << SIRFSOC_TIMER_WDT_INDEX), TIMER_INT_EN);
#endif
	writel(enable, TIMER_WATCHDOG_EN);
}

#ifdef CONFIG_KERNEL_BACKUP
void arch_preboot_os(void)
{
	sirfsoc_set_watchdog(CONFIG_BOOT_TIMEOUT, 1);
}
#endif
