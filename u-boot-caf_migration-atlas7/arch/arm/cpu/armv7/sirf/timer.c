/*
 * Copyright (c) 2012-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <asm/io.h>
#include <asm/arch/timer.h>

DECLARE_GLOBAL_DATA_PTR;

#define SIRF_REGV_TO_MS(clk_regv)	((clk_regv) / 1000)
#define SIRF_USEC_TO_REGV(usec)		(usec)

int timer_init(void)
{
#if defined(CONFIG_ARCH_MARCO) || defined(CONFIG_ARCH_ATLAS7)
	u32 timer_div;
#if defined(CONFIG_ARCH_MARCO)
	timer_div = CONFIG_IO_CLK_FREQ / CONFIG_CLOCK_TICK_RATE / 2 - 1;
#endif
#if defined(CONFIG_ARCH_ATLAS7)
	timer_div = CONFIG_IO_CLK_FREQ / CONFIG_CLOCK_TICK_RATE - 1;
#endif
	/* restore to default state */
	writel(0, TIMER_64COUNTER_CTRL);
	writel((timer_div << 16), TIMER_64COUNTER_CTRL);
	writel(0, TIMER_64COUNTER_LOAD_LO);
	writel(0, TIMER_64COUNTER_LOAD_HI);
	writel(readl(TIMER_64COUNTER_CTRL) | 0x2, TIMER_64COUNTER_CTRL);
	writel(readl(TIMER_64COUNTER_CTRL) & ~0x2, TIMER_64COUNTER_CTRL);
#endif
	gd->arch.lastinc = 0;
	gd->arch.tbl = 0;

	return 0;
}

/*
 * timer without interrupts
 */
unsigned long get_timer(unsigned long base)
{
	return get_timer_masked() - base;
}

/* delay x useconds */
void __udelay(unsigned long usec)
{
	unsigned long tmo = SIRF_USEC_TO_REGV(usec);
	unsigned long now, last;

#if defined(CONFIG_ARCH_MARCO) || defined(CONFIG_ARCH_ATLAS7)
	writel(readl(TIMER_64COUNTER_CTRL) | 0x1, TIMER_64COUNTER_CTRL);
	last = readl(TIMER_64COUNTER_RLATCHED_LO);
#else
	writel(0x1, TIMER_LATCH);
	last = readl(TIMER_LATCHED_LO);
#endif

	while ((long)tmo > 0) {
#if defined(CONFIG_ARCH_MARCO) || defined(CONFIG_ARCH_ATLAS7)
		writel(readl(TIMER_64COUNTER_CTRL) | 0x1, TIMER_64COUNTER_CTRL);
		now = readl(TIMER_64COUNTER_RLATCHED_LO);
#else
		writel(0x1, TIMER_LATCH);
		now = readl(TIMER_LATCHED_LO);
#endif
		tmo -= now - last;
		last = now;
	}
}

unsigned long get_timer_masked(void)
{
	unsigned long now;

#if defined(CONFIG_ARCH_MARCO) || defined(CONFIG_ARCH_ATLAS7)
	writel(readl(TIMER_64COUNTER_CTRL) | 0x1, TIMER_64COUNTER_CTRL);
	now = readl(TIMER_64COUNTER_RLATCHED_LO);
#else
	writel(0x1, TIMER_LATCH);
	now = readl(TIMER_LATCHED_LO);
#endif

	now = SIRF_REGV_TO_MS(now);
	gd->arch.tbl += now - gd->arch.lastinc;
	gd->arch.lastinc = now;

	return gd->arch.tbl;
}

/*
 * This function is derived from PowerPC code (read timebase as long long).
 * On ARM it just returns the timer value.
 */
unsigned long long get_ticks(void)
{
	return get_timer(0);
}

/*
 * This function is derived from PowerPC code (timebase clock frequency).
 * On ARM it returns the number of timer ticks per second.
 */
unsigned long get_tbclk(void)
{
#if defined(CONFIG_ARCH_ATLAS7)
	return CONFIG_CLOCK_TICK_RATE;
#else
	return CONFIG_SYS_HZ;
#endif
}
