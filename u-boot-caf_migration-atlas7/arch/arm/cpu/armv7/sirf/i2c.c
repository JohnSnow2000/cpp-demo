/*
 * Support SiRF NandDisk Environment Variable Save.
 *
 * Copyright (c) 2012-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <asm/io.h>
#include <asm/arch/i2c.h>
#include <asm/arch/rst.h>
#include <asm/arch/clk.h>
#include <asm/arch/clkc.h>
#include <asm/arch/gpio.h>
#include <asm/arch/ioctop.h>

struct sirf_i2c_reg {
	u32 clk_ctrl;
	u32 res1[2];

	u32 status;
	u32 ctrl;
	u32 io_ctrl;
	u32 sda_delay;
	u32 cmd_start;
	u32 res2[4];

	u32 cmd[16];
	u32 res3[4];

	u32 data[4];
};

static const u32 i2c_port_base_addr[] = {I2C0_MODULE_BASE, I2C1_MODULE_BASE};
static bool is_i2c_init[] = {false, false};

static bool err_print(char *called, char *calling)
{
	printf("Error! Failed to call %s() in %s(). \r\n", called, calling);

	return false;
}
#define failed_call(x)	   err_print(x, (char *)(__func__))

bool i2c_init(int port_index, u32 clock_rate)
{
	struct sirf_i2c_reg *i2c_reg =
		(struct sirf_i2c_reg *)i2c_port_base_addr[port_index];
	u32 temp;

	/*save up sometime for pxp*/
#ifdef ATLAS7_PXP
	return false;
#else

	if (0 == port_index) {
#ifdef CONFIG_ARCH_ATLAS7
		writel(IOC_TOP_FUNC_I2C0, SW_TOP_FUNC_SEL_REG_SET(17));
		writel(readl(CLKC_MISC2_LEAF_CLK_EN_SET) |
			CLKC_LEAF_CLK_I2C0_EN, CLKC_MISC2_LEAF_CLK_EN_SET);
#else
		writel(readl(GPIO_PAD_EN(2)) & ~0x0c000000, GPIO_PAD_EN(2));
		writel(readl(RESET_SR0) | RESET_SR_I2C0_RST, RESET_SR0);
		writel(readl(CLKC_CLK_EN1) | CLK_I2C0_EN, CLKC_CLK_EN1);
		writel(readl(RESET_SR0) & ~RESET_SR_I2C0_RST, RESET_SR0);
#endif
		} else {
#ifdef CONFIG_ARCH_ATLAS7
		writel(readl(SW_TOP_FUNC_SEL_REG_SET(16)) |
			IOC_TOP_FUNC_I2C1, SW_TOP_FUNC_SEL_REG_SET(16));
		writel(readl(CLKC_MISC2_LEAF_CLK_EN_SET) |
			CLKC_LEAF_CLK_I2C1_EN, CLKC_MISC2_LEAF_CLK_EN_SET);
#else
		writel(readl(GPIO_PAD_EN(0)) & ~0x0000a000, GPIO_PAD_EN(0));
		writel(readl(RESET_SR0) | RESET_SR_I2C1_RST, RESET_SR0);
		writel(readl(CLKC_CLK_EN1) | CLK_I2C1_EN, CLKC_CLK_EN1);
		writel(readl(RESET_SR0) & ~RESET_SR_I2C1_RST, RESET_SR0);
#endif
	}

	if (clock_rate > 100*1000)
		/*ratio 5.5*/
		i2c_reg->clk_ctrl = (CONFIG_SYS_CLK_FREQ / clock_rate) * 2 / 11;
	else
		/*ratio 5*/
		i2c_reg->clk_ctrl = CONFIG_SYS_CLK_FREQ / clock_rate / 5;

	if (i2c_reg->clk_ctrl > 0x1e)
		temp = I2C_10_IOCLK_FILTER;
	else if (i2c_reg->clk_ctrl > 0x12)
		temp = I2C_6_IOCLK_FILTER;
	else if (i2c_reg->clk_ctrl > 0x09)
		temp = I2C_3_IOCLK_FILTER;
	else
		temp = I2C_NO_FILTER;

	i2c_reg->clk_ctrl = temp |
		(i2c_reg->clk_ctrl > 0xff ? 0xff : i2c_reg->clk_ctrl);
	i2c_reg->ctrl	= I2C_CTRL_CORE_EN | I2C_CTRL_MODE_M | I2C_CTRL_RESET;

	return true;
#endif
}

static bool i2c_wait_status(struct sirf_i2c_reg *i2c_reg, u32 u_status)
{
	u32 start_tick = get_timer(0);

	/* Wait at most 10ms for response */
	while (get_timer(start_tick) < 10) {
		if (u_status >= 0x10) {
			if ((i2c_reg->status & u_status) != 0) {
				i2c_reg->status |= u_status;
				return true;
			}
		} else {
			if ((i2c_reg->status & u_status) == 0)
				return true;
		}
	}

	return false;
}

bool i2c_read(int port_index, u32 dev_addr, u32 reg_addr, u8 *data)
{
	struct sirf_i2c_reg *i2c_reg =
		(struct sirf_i2c_reg *)i2c_port_base_addr[port_index];

	if (false == i2c_wait_status(i2c_reg, I2C_STATUS_BUSY))
		return failed_call("i2c_wait_status(I2C_STATUS_BUSY)");

	i2c_reg->cmd[0] = I2C_CMD_SEND_START | I2C_CMD_WRITE;
	i2c_reg->cmd[1] = dev_addr;
	i2c_reg->cmd[2] = I2C_CMD_WRITE | I2C_CMD_SEND_STOP;
	i2c_reg->cmd[3] = reg_addr;
	i2c_reg->cmd[4] = I2C_CMD_SEND_START | I2C_CMD_WRITE;
	i2c_reg->cmd[5] = dev_addr | 0x01;
	i2c_reg->cmd[6] = I2C_CMD_READ | I2C_CMD_NACK | I2C_CMD_SEND_STOP;
	i2c_reg->cmd_start = 1;			/*start command*/

	if (false == i2c_wait_status(i2c_reg, I2C_STATUS_CMD_FINISH_INT))
		return failed_call("i2c_wait_status(CMD_FINISH_INT)");
	if ((i2c_reg->status & I2C_STATUS_ERROR_INT) != 0) {
		printf("Error! I2C error in i2c_write().  \r\n");
		return false;
	}

	i2c_reg->status |= I2C_STATUS_CMD_FINISH_INT | I2C_STATUS_ERROR_INT;

	*data = (u8)i2c_reg->data[0];

	return true;
}


bool i2c_write(int port_index, u32 dev_addr, u32 reg_addr, u8 u_data)
{
	struct sirf_i2c_reg *i2c_reg =
		(struct sirf_i2c_reg *)i2c_port_base_addr[port_index];

	if (false == i2c_wait_status(i2c_reg, I2C_STATUS_BUSY))
		return false;

	i2c_reg->cmd[0] = I2C_CMD_SEND_START | I2C_CMD_WRITE;
	i2c_reg->cmd[1] = dev_addr;
	i2c_reg->cmd[2] = I2C_CMD_WRITE | I2C_CMD_NACK;
	i2c_reg->cmd[3] = reg_addr;
	i2c_reg->cmd[4] = I2C_CMD_WRITE | I2C_CMD_NACK | I2C_CMD_SEND_STOP;
	i2c_reg->cmd[5] = u_data;
	i2c_reg->cmd_start = 1;			/*start command*/

	if (false == i2c_wait_status(i2c_reg, I2C_STATUS_CMD_FINISH_INT))
		return failed_call("i2c_wait_status(CMD_FINISH_INT)");
	if ((i2c_reg->status & I2C_STATUS_ERROR_INT) != 0) {
		printf("Error! I2C error in i2c_write().  \r\n");
		return false;
	}

	i2c_reg->status |= I2C_STATUS_CMD_FINISH_INT | I2C_STATUS_ERROR_INT;

	return true;
}

bool dev_reg_bit_modify(int port_index, u32 dev_addr, u32 reg_addr,
	u8 bit_map, bool is_set)
{
	u8 value;

	if (is_i2c_init[port_index] == false)
		if (i2c_init(port_index, 20*1000) == false)
			return failed_call("i2c_init");

	if (false == i2c_read(port_index, dev_addr, reg_addr, &value))
		return failed_call("i2c_read");

	if (true == is_set)
		value |= bit_map;
	else
		value &= ~bit_map;

	if (false == i2c_write(port_index, dev_addr, reg_addr, value))
		return false; /* failed_call("i2c_write"); */

	return true;
}

bool i2c_dev_reg_read(int port_index, u32 dev_addr, u32 reg_addr,
	u8 *value)
{
	if (!is_i2c_init[port_index])
		if (!i2c_init(port_index, 20*1000))
			return failed_call("i2c_init");

	if (!i2c_read(port_index, dev_addr, reg_addr, value))
		return failed_call("i2c_read");
	else
		return 1;
}

bool i2c_dev_reg_bit_set(int port_index, u32 dev_addr, u32 reg_addr,
	u8 bit_map)
{
	return dev_reg_bit_modify(port_index, dev_addr, reg_addr,
		bit_map, true);
}

bool i2c_dev_reg_bit_clear(int port_index, u32 dev_addr, u32 reg_addr,
	u8 bit_map)
{
	return dev_reg_bit_modify(port_index, dev_addr, reg_addr,
		bit_map, false);
}
