/*
 * Copyright (c) 2013-2014, 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SIRF_NAND_H__
#define __SIRF_NAND_H__

#define CLOCK_BASE			0x88000000
#define CLOCK_EN1			0x4
#define CLOCK_DMAC0_BIT			0x0
#define CLOCK_NAND_BIT			0x2

#define RSC_BASE			0x88020000
#define RSC_PINMUX			0x4

#define GPIO_BASE			0xb0120000
#define GPIO3_PAD_EN			(0x84 + 0x300)
#define GPIO2_PAD_EN			(0x84 + 0x200)

#define RESET_BASE			0x88010000

#define NAND_TIMEOUT			0x000FFFFF


#if defined(CONFIG_ARCH_PRIMAII) || defined(CONFIG_ARCH_ATLASVI)
#define NAND_BASE			0xb0030000
#elif defined(CONFIG_ARCH_ATLAS7)
#define NAND_BASE			0x17050000
#endif

#define NAND_BOOTMODE			0x0000
#define NAND_WAIT0			0x0004
#define NAND_WAIT1			0x0008
#define NAND_CMDWAIT			0x000c
#define NAND_BANKSEL			0X0010
#define NAND_ADDNUM			0x0014
#define NAND_COMMAND			0x0018
#define NAND_LOWADD			0x001c
#define NAND_HIGHADD			0x0020
#define NAND_SECTLOCATION		0x0024
#define NAND_INTEN			0x0028
#define NAND_INTSTATUS			0x002c
#define NAND_CTRL			0x0030
#define NAND_DMAIOCTRL			0x0034
#define NAND_TRANSLEN			0x0038
#define NAND_IORRY			0x003c
#define NAND_IOWRY			0x0040
#define NAND_ECCSET			0x0044
#define NAND_WSECTINFOLOW		0x0048
#define NAND_WSECTINFOHIGH		0x004c
#define NAND_SECTINFOPARITY		0x0050
#define NAND_ECCERRSTATUS(n)		(0x006c + (n) * 4)
#define NAND_ECCERRNUMS(n)		(0x0074 + (n) * 4)
#define NAND_BZEROCNT(n)		(0x0084 + (n) * 4)
#define NAND_BITZEROCNT(n)		(0x0094 + (n) * 4)
#define NAND_RSECTINFOLOW		0x00a4
#define NAND_RSECTINFOHIGH		0x00a8
#define NAND_SECTINFOAREA		0x00ac
#define NAND_SECTECCERRFLAG		0x00b0
#define NAND_SECTECCERRINDEX		0x00b4
#define NAND_RANDCTRL			0x00b8
#define NAND_RANDLEVEN			0x00bc
#define NAND_RANDSEED(n)		(0x00c0 + (n) * 4)

#define NAND_SAMPLEDLL			0x00e4
#define NAND_RDATAPADDLY		0x0110

#define NAND_DMASTARTADDR		0x0400
#define NAND_DMAXLEN			0X0404
#define NAND_DMAYLEN			0x0408
#define NAND_DMACTRL			0x040C
#define NAND_DMAWIDTH			0x0410
#define NAND_DMAINT			0x0418
#define NAND_DMAINTEN			0x041C

#define NAND_SRAM(n)			(0x4000 + (n) * 4)

/* interrupt controller */
#if defined(CONFIG_ARCH_PRIMAII)
#define INT_BASE			0x80020000
#define  CHIP_ID_PA			(INT_BASE + 0x44)
#elif defined(CONFIG_ARCH_ATLASVI)
#define INT_BASE			0x80020000
#define  CHIP_ID_PA			(INT_BASE + 0x34)
#elif defined(CONFIG_ARCH_ATLAS7)
#define INT_BASE			0x10220000
#define  CHIP_ID_PA			(INT_BASE + 0x48)
#endif
#define INT_DMACMASK			0x0054

/*
 * for NAND_INT
 */
#define NAND_WT_ADD_DONE		(1 << 0)
#define NAND_IO_DMA_DONE		(1 << 1)
#define NAND_ECC_ENC_DONE		(1 << 2)
#define NAND_ECC_FAIL			(1 << 3)
#define NAND_ECC_WARNING		(1 << 4)
#define NAND_ECC_DEC_DONE		(1 << 5)
#define NAND_SI_ECC_ENC_DONE		(1 << 6)
#define NAND_SI_ECC_DEC_DONE		(1 << 7)
#define NAND_IOR_RY_EN			(1 << 8)
#define NAND_ISSUE_CMD_FAIL_EN		(1 << 9)
#define NAND_ALL_INT_MASK		0x1FF

/*
 * for NAND_CTRL
 */
#define NAND_SDR_MODE		1
#define NAND_DDR_MODE		0
#define NAND_DIRECT_READ	(1 << 4)
#define NAND_READ_SPARE_DREAD	(1 << 5)

/*
 * for NAND_DMA_IO_CTRL
 */
#define NAND_IO_ACCESS		(1 << 0)
#define NAND_DMA_ACCESS		(0 << 0)
#define NAND_READ		(1 << 1)
#define NAND_WRITE		(0 << 1)
#define NAND_NO_RDWT		(1 << 2)
#define NAND_SI_READ_ONLY	(1 << 3)
#define NAND_GEN_CMD_RYBY	(1 << 4)
#define NAND_GEN_CMDINT_RYBY	(1 << 5)

/*
 * for NAND_ECC_SET
 */
#define	NAND_DATA_BCH_ECC_EN	(1 << 3)
#define	NAND_DATA_BCH_ECC_RESET	(1 << 4)
#define	NAND_SI_BCH_ECC_EN	(1 << 10)
#define	NAND_SI_BCH_ECC_RESET	(1 << 11)
#define	NAND_BCH_12BITS_ECC	(0 << 12)
#define	NAND_BCH_24BITS_ECC	(1 << 12)
#define	NAND_BCH_40BITS_ECC	(2 << 12)
#define	NAND_BCH_60BITS_ECC	(3 << 12)

/* ecc parity per 1 k data */
#define NAND_ECC_BANK_UNIT_LEN	1024
#define NAND_ECC_SI_UNIT_LEN	8
#define NAND_ECC_BANK_PARITY_LEN(ecc_bits)	\
	((ecc_bits == 0) ? 0 : (ecc_bits == 12) ? 21 :	\
	(ecc_bits == 24) ? 42 : (ecc_bits == 40) ? 70 :	105)
#define NAND_ECC_BANK_LEN(ecc_bits)	\
	(NAND_ECC_BANK_UNIT_LEN + (NAND_ECC_BANK_PARITY_LEN(ecc_bits)))
#define NAND_ECC_SI_PARITY_LEN	4
#define NAND_ECC_SI_LEN	(NAND_ECC_SI_UNIT_LEN + NAND_ECC_SI_PARITY_LEN)

/*
 * for NAND_ECC_ERR_STATUS0
 */
#define NAND_ECC_FAIL_FLAG	(1 << 31)

/*
 * for NAND_ECC_ERR_STATUS1
 */
#define NAND_ECC_ERR_FLAG	(1 << 31)

/*
 * for NAND_SECT_ECC_FLAG
 */
#define NAND_SECT_ERR_FLAG	(1 << 0)
#define NAND_SECT_ERR_FAULT	(1 << 1)
#define NAND_SECT_ERR_BIT_MASK	(7 << 2)
#define NAND_SECT_ECC_FAIL	(NAND_SECT_ERR_FLAG | NAND_SECT_ERR_FAULT)

/*
 * for NAND_SECT_ECC_FLAG
 */
#define NAND_SECT_ERR_BYTE_INDEX_MASK	(0x78)
#define NAND_SECT_ERR_BIT_INDEX_MASK	(0x7)
#define NAND_SECT_ERR_BYTE(n)	(((n) & NAND_SECT_ERR_BYTE_INDEX_MASK) >> 3)
#define NAND_SECT_ERR_BIT(n)	((n) & NAND_SECT_ERR_BIT_INDEX_MASK)

/*
 * for NAND_RAN_CTRL
 */
#define NAND_RAND_ENABLE	(1 << 0)
#define NAND_RAND_RESET		(1 << 1)
#define NAND_RAND_SI_SELECT	(1 << 2)
#define NAND_RAND_LEVEL		0x6000

#define NAND_INFO_NEED_RANDOMIZER 1
#define DMAC0_BASE			0xb00b0000
#define WRITE_FLASH      0x10
#define READ_FLASH       0x0
#define DMA_BURST_LEN    0x0
#define DMA_INT_FINISH   0x1

/*
 * Access SRAM
 */
#define NAND_SRAM_BANK_SIZE	(1024)
#define NAND_SRAM_MAX_BANK	(8)

/*
 * Clock related
 */
#define NAND_HIGH_SAMPLE        0x0
#define NAND_DLL_SAMPLE         0x1

#define NAND_CALC_TIMING(time, nand_clk)	\
	((time) ? (((time) + (nand_clk) - 1) / (nand_clk) - 1) : 0)

/* nanddisk related */
extern unsigned nanddisk_bin_start, nanddisk_bin_end;

int nanddisk_init(void);
int nanddisk_erase(void);
int nanddisk_reset(void);
void nanddisk_print_info(void);

enum nanddisk_map_state {
	NANDISK_MAP_NORMAL,
	NANDISK_MAP_BOOTPARTITION,
};
int nanddisk_map_change(enum nanddisk_map_state state);

unsigned long nanddisk_read(int dev, unsigned long start, lbaint_t blkcnt,
		void *buffer);
unsigned long nanddisk_write(int dev, unsigned long start, lbaint_t blkcnt,
		const void *buffer);

#endif /* __SIRF_NAND_H__ */
