/*
 * Copyright (c) 2012, 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SIRF_RSC_H_
#define __SIRF_RSC_H_

#ifdef CONFIG_ARCH_MARCO
#define RSC_BASE				0xC3010000
#define RSC_USB_UART_SHARE_SET			(RSC_BASE + 0x0000)
#define RSC_USB_UART_SHARE_CLR			(RSC_BASE + 0x0004)
#define RSC_PIN_MUX_SET				(RSC_BASE + 0x0008)
#define RSC_PIN_MUX_CLR				(RSC_BASE + 0x000C)
#define RSC_USB_CTRL_SET			(RSC_BASE + 0x0010)
#define RSC_USB_CTRL_CLR			(RSC_BASE + 0x0014)
#define PADCTRL_BASE				0xC3014000
#define PADCTRL_PAD_CD_0_SET			(PADCTRL_BASE + 0x0078)
#define PADCTRL_PAD_CD_0_CLR			(PADCTRL_BASE + 0x007C)
#else
#define RSC_BASE				0x88020000
#define RSC_USB_UART_SHARE			(RSC_BASE + 0x0000)
#define RSC_PIN_MUX				(RSC_BASE + 0x0004)
#define RSC_USBPHY_PLL_CTRL			(RSC_BASE + 0x0008)
#define PADCTRL_BASE				0x88024000
#define PADCTRL_PAD_CD_0			(PADCTRL_BASE + 0x0070)
#endif

#define USBONLY					(1 << 0)
#define USBUART_SHARE_EN			(1 << 1)
#define USB1_MODE_SEL				(1 << 2)

#define PAD_UTMI_DRVVBUS1_EN			(1 << 11)

#define USBPHY_PLL_POWERDOWN			(1 << 1)
#define USBPHY_PLL_BYPASS			(1 << 2)
#define USBPHY_PLL_LOCK				(1 << 3)

#endif
