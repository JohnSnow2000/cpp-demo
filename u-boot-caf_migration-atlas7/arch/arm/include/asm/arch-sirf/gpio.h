/*
 * Copyright (c) 2012-2014, 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SIRF_GPIO_H_
#define __SIRF_GPIO_H_

#ifdef CONFIG_ARCH_MARCO
#define GPIO_BASE				0xcc120000
#define GPIO_DSP_EN0_SET			(GPIO_BASE + 0x80)
#define GPIO_DSP_EN0_CLR			(GPIO_BASE + 0x94)
#define GPIO_PAD_EN_SET(g)			(GPIO_BASE + (g) * 0x100 + 0x84)
#define GPIO_PAD_EN_CLR(g)			(GPIO_BASE + (g) * 0x100 + 0x90)
#else
#define GPIO_BASE				0xb0120000
#define GPIO_DSP_EN0				(GPIO_BASE + 0x80)
#define GPIO_PAD_EN(g)				(GPIO_BASE + (g) * 0x100 + 0x84)
#endif
#define GPIO_CTRL(g, i)			(GPIO_BASE + (g) * 0x100 + (i) * 4)
#define GPIO_CTRL_REGBASE(g, i)			(GPIO_CTRL(g, i))
#define GPIO_INT_STATUS(g)			(GPIO_BASE + (g) * 0x100 + 0x8C)

#define GPIO_CTL_INTR_LOW_MASK			0x1
#define GPIO_CTL_INTR_HIGH_MASK			0x2
#define GPIO_CTL_INTR_TYPE_MASK			0x4
#define GPIO_CTL_INTR_EN_MASK			0x8
#define GPIO_CTL_INTR_STS_MASK			0x10
#define GPIO_CTL_OUT_EN_MASK			0x20
#define GPIO_CTL_DATAOUT_MASK			0x40
#define GPIO_CTL_DATAIN_MASK			0x80
#define GPIO_CTL_PULL_MASK			0x100
#define GPIO_CTL_PULL_HIGH			0x200
#define GPIO_CTL_DSP_INT			0x400

#endif
