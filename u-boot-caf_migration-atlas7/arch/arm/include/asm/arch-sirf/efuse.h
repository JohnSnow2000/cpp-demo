/*
 * Copyright (c) 2012, 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SIRF_EFUSE_H
#define __SIRF_EFUSE_H

#ifdef CONFIG_ARCH_MARCO
#define EFUSE_BASE		0xCC140000
#else
#define EFUSE_BASE		0xb0140000
#endif
#define EFUSE0_OUT0		(EFUSE_BASE + 0x0000)
#define EFUSE0_OUT1		(EFUSE_BASE + 0x0004)

#endif /* __SIRF_EFUSE_H */
