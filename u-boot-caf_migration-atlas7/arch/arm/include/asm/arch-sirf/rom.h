/*
 * Copyright (c) 2012, 2014, 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SIRF_ROM_H_
#define __SIRF_ROM_H_

#ifdef CONFIG_ARCH_MARCO
#define ROM_MODULE_BASE			0xCC000000
#else
#define ROM_MODULE_BASE			0xB0000000
#endif

#define ROM_CFG_CS1			(ROM_MODULE_BASE + 0x0004)

/*
 * Register definition
 */
#define ROM_WRITE_GAP			0x0000007f
#define ROM_FIFO_MDOE			0x00000080
#define ROM_TACC			0x00003f00
#define ROM_TCES			0x0000c000
#define ROM_TNACC			0x003F0000
#define ROM_TDF				0x00C00000
#define ROM_DWORD_ACCESS		0x01000000
#define ROM_DWORD_ACCESS_DIS		0x00000000
#define ROM_BURST_READ			0x02000000
#define ROM_BURST_READ_DIS		0x00000000
#define ROM_BURST_WRITE			0x04000000
#define ROM_BURST_WRITE_DIS		0x00000000
#define ROM_VARI_ACCESS			0x08000000
#define ROM_VARI_ACCESS_DIS		0x00000000
#define ROM_BE_ACTIVE1			0x10000000
#define ROM_BE_ACTIVE_DIS		0x00000000
#define ROM_WRITE_EN			0x20000000
#define ROM_WRITE_DIS			0x00000000
#define ROM_BUS_8			0x00000000
#define ROM_BUS_16			0x40000000
#define ROM_BUS_32			0x80000000

#define ROM_WRITE_GAP_BM		0x0000007f
#define ROM_WRITE_GAP_SF		0
#define ROM_FIFO_MODE_BM		0x00000080
#define ROM_FIFO_MODE_SF		7
#define ROM_TACC_BM			0x00003f00
#define ROM_TACC_SF			8
#define ROM_TCES_BM			0x0000c000
#define ROM_TCES_SF			14
#define ROM_TNACC_BM			0x003F0000
#define ROM_TNACC_SF			16
#define ROM_TDF_BM			0x00C00000
#define ROM_TDF_SF			22

#define ROM_SET_WGAP(x) \
			(ROM_WRITE_GAP_BM & ((x) << ROM_WRITE_GAP_SF))
#define ROM_SET_FIFOMODE(x) \
			(ROM_FIFO_MODE_BM & ((x) << ROM_FIFO_MODE_SF))
#define ROM_SET_TACC(x)			(ROM_TACC_BM & ((x) << ROM_TACC_SF))
#define ROM_SET_TCES(x)			(ROM_TCES_BM & ((x) << ROM_TCES_SF))
#define ROM_SET_TNACC(x)		(ROM_TNACC_BM & ((x) << ROM_TNACC_SF))
#define ROM_SET_TDF(x)			(ROM_TDF_BM & ((x) << ROM_TDF_SF))

#endif
