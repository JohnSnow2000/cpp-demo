/*
 * Copyright (c) 2012, 2014, 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SIRF_UART_H_
#define __SIRF_UART_H_

#ifdef CONFIG_ARCH_MARCO
#define UART1_MODULE_BASE			0xCC060000
#else
#ifdef CONFIG_ARCH_ATLAS7
#define UART1_MODULE_BASE			0x18020000
#else
#define UART1_MODULE_BASE			0xB0060000
#endif
#endif
#define UART_REG(offset)			(UART1_MODULE_BASE + (offset))

#define UART1_LINE_CTRL				UART_REG(0x0040)
#define UART1_MODEM_CTRL			UART_REG(0x0044)
#define UART1_MODEM_STATUS			UART_REG(0x0048)
#define UART1_TXRX_ENA_REG                      UART_REG(0x004c)
#define UART1_DIVISOR                           UART_REG(0x0050)
#define UART1_INT_ENABLE                        UART_REG(0x0054)
#define UART1_INT_STATUS                        UART_REG(0x0058)
#define UART1_RISC_DSP_MODE                     UART_REG(0x005C)

#ifdef CONFIG_ARCH_ATLAS7
#define UART1_INT_EN_CLR			UART_REG(0x0060)
#endif

#define UART1_TX_DMA_IO_CTRL                    UART_REG(0x0100)
#define UART1_TX_DMA_IO_LEN                     UART_REG(0x0104)
#define UART1_TXFIFO_CTRL                       UART_REG(0x0108)
#define UART1_TXFIFO_LEVEL_CHK                  UART_REG(0x010c)
#define UART1_TXFIFO_OP                         UART_REG(0x0110)
#define UART1_TXFIFO_STATUS                     UART_REG(0x0114)
#define UART1_TXFIFO_DATA			UART_REG(0x0118)

#define UART1_RX_DMA_IO_CTRL                    UART_REG(0x0120)
#define UART1_RX_DMA_IO_LEN			UART_REG(0x0124)
#define UART1_RXFIFO_CTRL			UART_REG(0x0128)
#define UART1_RXFIFO_LEVEL_CHK                  UART_REG(0x012c)
#define UART1_RXFIFO_OP                         UART_REG(0x0130)
#define UART1_RXFIFO_STATUS                     UART_REG(0x0134)
#define UART1_RXFIFO_DATA			UART_REG(0x0138)

#ifdef CONFIG_ARCH_ATLAS7
#define UART1_AFC_CTRL				UART_REG(0x0140)
#define UART1_RX_DMA_CNTR			UART_REG(0x0144)
#define UART1_SWH_DMA_IO			UART_REG(0x0148)
#endif

#define UART1_TX_IO_MODE                        0x1
#define UART1_RX_IO_MODE                        0x1
#define UART1_RX_EN                             0x01
#define UART1_TX_EN                             0x02
#define UART1_TXFIFO_RESET                      0x1
#define UART1_TXFIFO_START                      0x2
#define UART1_RXFIFO_RESET                      0x1
#define UART1_RXFIFO_START                      0x2
#define UART1_RXFIFO_EMPTY                      0x40
#define UART1_TXFIFO_EMPTY			0x40
#define UART1_TXFIFO_FULL                       0x20
#define UART1_RXFIFO_FULL                       0x20
#define UART_INT_MASK_ALL			0xffff
#define UART_FIFO_FULL				0x20

#ifdef CONFIG_ARCH_ATLAS7
#define UART0_MODULE_BASE			0x18010000
#define UART0_REG(offset)			(UART0_MODULE_BASE + (offset))

#define UART0_LINE_CTRL				UART0_REG(0x0040)
#define UART0_MODEM_CTRL			UART0_REG(0x0044)
#define UART0_MODEM_STATUS			UART0_REG(0x0048)
#define UART0_TXRX_ENA_REG                      UART0_REG(0x004c)
#define UART0_DIVISOR                           UART0_REG(0x0050)
#define UART0_INT_ENABLE                        UART0_REG(0x0054)
#define UART0_INT_STATUS                        UART0_REG(0x0058)
#define UART0_RISC_DSP_MODE                     UART0_REG(0x005C)

#define UART0_INT_EN_CLR			UART0_REG(0x0060)

#define UART0_TX_DMA_IO_CTRL                    UART0_REG(0x0100)
#define UART0_TX_DMA_IO_LEN                     UART0_REG(0x0104)
#define UART0_TXFIFO_CTRL                       UART0_REG(0x0108)
#define UART0_TXFIFO_LEVEL_CHK                  UART0_REG(0x010c)
#define UART0_TXFIFO_OP                         UART0_REG(0x0110)
#define UART0_TXFIFO_STATUS                     UART0_REG(0x0114)
#define UART0_TXFIFO_DATA			UART0_REG(0x0118)

#define UART0_RX_DMA_IO_CTRL                    UART0_REG(0x0120)
#define UART0_RX_DMA_IO_LEN			UART0_REG(0x0124)
#define UART0_RXFIFO_CTRL			UART0_REG(0x0128)
#define UART0_RXFIFO_LEVEL_CHK                  UART0_REG(0x012c)
#define UART0_RXFIFO_OP                         UART0_REG(0x0130)
#define UART0_RXFIFO_STATUS                     UART0_REG(0x0134)
#define UART0_RXFIFO_DATA			UART0_REG(0x0138)


#define UART0_AFC_CTRL				UART0_REG(0x0140)
#define UART0_RX_DMA_CNTR			UART0_REG(0x0144)
#define UART0_SWH_DMA_IO			UART0_REG(0x0148)

#define UART0_TX_IO_MODE                        0x1
#define UART0_RX_IO_MODE                        0x1
#define UART0_RX_EN                             0x01
#define UART0_TX_EN                             0x02
#define UART0_TXFIFO_RESET                      0x1
#define UART0_TXFIFO_START                      0x2
#define UART0_RXFIFO_RESET                      0x1
#define UART0_RXFIFO_START                      0x2
#define UART0_RXFIFO_EMPTY                      0x100
#define UART0_TXFIFO_EMPTY			0x40
#define UART0_TXFIFO_FULL                       0x20
#define UART0_RXFIFO_FULL                       0x20

#define UART_5_DATA_BIT 0
#define UART_6_DATA_BIT 1
#define UART_7_DATA_BIT 2
#define UART_8_DATA_BIT 3

#define UART_1_STOP_BIT 0
#define UART_2_STOP_BIT 1

#define UART_NO_PARITY 0
#define UART_EVEN_PARITY 1
#define UART_ODD_PARITY 3
#define UART_MARK_PARITY 5
#define UART_SPACE_PARITY 7

#define SW_TOP_FUNC_SEL_19_REG_CLR  0x10E4011C
#define SW_TOP_FUNC_SEL_19_REG_SET  0x10E40118
#define UART0_RXD_0 (1<<4)
#define UART0_TXD_0 (1<<0)

/*
* #define UART1_RXD_0 (1<<20)
* #define UART1_TXD_0 (1<<16)
*/
#endif

#endif
