/*
 * Copyright (c) 2012-2015, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SIRF_CLK_H
#define __SIRF_CLK_H

#if defined(CONFIG_ARCH_MARCO)
#define CLK_CTRL_BASE				0xC3000000
#elif defined(CONFIG_ARCH_ATLAS7)
#define CLK_CTRL_BASE				0x18620000
#else
#define CLK_CTRL_BASE				0x88000000
#endif

#define CLKC_CLK_EN0				(CLK_CTRL_BASE + 0x0000)
#define CLKC_CLK_EN1				(CLK_CTRL_BASE + 0x0004)

/*
 * bit define for rCLKC_CLK_EN0
 */
#define CLK_DSP_EN				0x00000001
#define CLK_USB0_EN				0x00010000
#define CLK_USB1_EN				0x00020000
#define CLK_SECURITY_EN				0x00080000

/*
 * bit define for rCLKC_CLK_EN1
 */
/* default is enabled */
#define CLK_NAND_EN				0x00000004
#define CLK_UART1_EN				0x00000020
#define CLK_I2C0_EN				0x00004000
#define CLK_I2C1_EN				0x00008000
/* default is enabled */
#define CLK_EFUSESYS_EN				0x00020000
/* default is enabled */
#define CLK_ROM_EN				0x02000000
#define CLK_SDIO01_EN				0x08000000
#define CLK_SDIO23_EN				0x10000000
#define CLK_SDIO45_EN				0x20000000
#define CLK_PWM_EN				0x00010000
#define CLK_LCD_EN				0x00000400

#if defined(CONFIG_ARCH_ATLAS7)
#define CLKC_LEAF_CLK_I2C0_EN			BIT(0)
#define CLKC_LEAF_CLK_I2C1_EN			BIT(1)
#endif
#endif
