/*
 * Copyright (c) 2014-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifdef CONFIG_ARCH_ATLAS7

#define IPC_BASE		    0x13240000
#define IPC_REG(offset)     (IPC_BASE + (offset))

#define IPC_TRGT1_INIT0_1					IPC_REG(0x0000)
#define IPC_TRGT1_INIT0_2					IPC_REG(0x0004)
#define IPC_TRGT2_INIT0_1					IPC_REG(0x0008)
#define IPC_TRGT2_INIT0_2					IPC_REG(0x000C)
#define IPC_TRGT3_INIT0_1					IPC_REG(0x0010)
#define IPC_TRGT3_INIT0_2					IPC_REG(0x0014)
#define IPC_TRGT0_INIT1_1					IPC_REG(0x0100)
#define IPC_TRGT0_INIT1_2					IPC_REG(0x0104)
#define IPC_TRGT2_INIT1_1					IPC_REG(0x0108)
#define IPC_TRGT2_INIT1_2					IPC_REG(0x010C)
#define IPC_TRGT3_INIT1_1					IPC_REG(0x0110)
#define IPC_TRGT3_INIT1_2					IPC_REG(0x0114)
#define IPC_TRGT0_INIT2_1					IPC_REG(0x0200)
#define IPC_TRGT0_INIT2_2					IPC_REG(0x0204)
#define IPC_TRGT1_INIT2_1					IPC_REG(0x0208)
#define IPC_TRGT1_INIT2_2					IPC_REG(0x020C)
#define IPC_TRGT3_INIT2_1					IPC_REG(0x0210)
#define IPC_TRGT3_INIT2_2					IPC_REG(0x0214)
#define IPC_TRGT0_INIT3_1					IPC_REG(0x0300)
#define IPC_TRGT0_INIT3_2					IPC_REG(0x0304)
#define IPC_TRGT1_INIT3_1					IPC_REG(0x0308)
#define IPC_TRGT1_INIT3_2					IPC_REG(0x030C)
#define IPC_TRGT2_INIT3_1					IPC_REG(0x0310)
#define IPC_TRGT2_INIT3_2					IPC_REG(0x0314)
#define IPC_SPINLOCK_RD_DEBUG			    IPC_REG(0x0400)
#define IPC_TESTANDSET_0					IPC_REG(0x0404)
#define IPC_TESTANDSET_1					IPC_REG(0x0408)
#define IPC_TESTANDSET_2					IPC_REG(0x040C)
#define IPC_TESTANDSET_3					IPC_REG(0x0410)
#define IPC_TESTANDSET_4					IPC_REG(0x0414)
#define IPC_TESTANDSET_5					IPC_REG(0x0418)
#define IPC_TESTANDSET_6					IPC_REG(0x041C)
#define IPC_TESTANDSET_7					IPC_REG(0x0420)
#define IPC_TESTANDSET_8					IPC_REG(0x0424)
#define IPC_TESTANDSET_9					IPC_REG(0x0428)
#define IPC_TESTANDSET_10					IPC_REG(0x042C)
#define IPC_TESTANDSET_11					IPC_REG(0x0430)
#define IPC_TESTANDSET_12					IPC_REG(0x0434)
#define IPC_TESTANDSET_13					IPC_REG(0x0438)
#define IPC_TESTANDSET_14					IPC_REG(0x043C)
#define IPC_TESTANDSET_15					IPC_REG(0x0440)
#define IPC_TESTANDSET_16					IPC_REG(0x0444)
#define IPC_TESTANDSET_17					IPC_REG(0x0448)
#define IPC_TESTANDSET_18					IPC_REG(0x044C)
#define IPC_TESTANDSET_19					IPC_REG(0x0450)
#define IPC_TESTANDSET_20					IPC_REG(0x0454)
#define IPC_TESTANDSET_21					IPC_REG(0x0458)
#define IPC_TESTANDSET_22					IPC_REG(0x045C)
#define IPC_TESTANDSET_23					IPC_REG(0x0460)
#define IPC_TESTANDSET_24					IPC_REG(0x0464)
#define IPC_TESTANDSET_25					IPC_REG(0x0468)
#define IPC_TESTANDSET_26					IPC_REG(0x046C)
#define IPC_TESTANDSET_27					IPC_REG(0x0470)
#define IPC_TESTANDSET_28					IPC_REG(0x0474)
#define IPC_TESTANDSET_29					IPC_REG(0x0478)

#define A7DA_POWER_SLP        1
#define A7DA_POWER_RST        2
#define A7DA_POWER_OFF        3
#define A7DA_M3_HOLD		9

#define A7DA_IPC_CONTENT	PWRC_SCRATCH_PAD8_F
#define A7DA_IPC_A72M3_REG	IPC_TRGT2_INIT0_2
#define A7DA_IPC_SEND		0x1

#endif
