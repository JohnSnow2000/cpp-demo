#ifndef _SIRF_HYPER_H
#define _SIRF_HYPER_H

#ifdef CONFIG_CSRVISOR_SUPPORT
extern int save_context(void);
#else
static inline int save_context(void) { return 0; }
#endif

#endif
