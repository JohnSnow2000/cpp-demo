/*
 * Copyright (c) 2014, 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <command.h>
#include <asm/utils.h>
#include <asm/io.h>

#if defined(CONFIG_CMD_VALIDATION)
void cpu_wfe(void)
{
	__asm__ ("wfe\n" : : : );
}

int do_cpuwfe(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	switch (argc) {
	case 1:
		cpu_wfe();
		break;
	default:
		return cmd_usage(cmdtp);
	}
	return 0;
}

U_BOOT_CMD(
	cpuwfe, 2, 0, do_cpuwfe,
	"bt_cpuclk_bypass\n",
	"bt_cpuclk_bypass: using 26Mhz, cpu is waiting\n"
);
#endif
