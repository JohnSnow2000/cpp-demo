/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <errno.h>
#include <asm/io.h>
#include "sha1.h"

#define DX_BASE_ADDRESS		0x18240000UL

#define DX_HOST_IRR_REG_OFFSET					0xA00UL
#define DX_HOST_IRR_DSCRPTR_COMPLETION_LOW_INT_BIT_SHIFT	0x2UL
#define DX_HOST_ICR_REG_OFFSET					0xA08UL
#define DX_HOST_ICR_DSCRPTR_COMPLETION_BIT_SHIFT		0x2UL

#define DX_AXIM_MON_INFLIGHT8_REG_OFFSET			0xB20UL
#define DX_AXIM_MON_COMP8_REG_OFFSET				0xBA0UL

#define DX_DSCRPTR_COMPLETION_COUNTER0_REG_OFFSET		0xE00UL

#define DX_DSCRPTR_QUEUE0_WORD0_REG_OFFSET			0xE80UL
#define DX_DSCRPTR_QUEUE0_WORD1_REG_OFFSET			0xE84UL
#define DX_DSCRPTR_QUEUE0_WORD2_REG_OFFSET			0xE88UL
#define DX_DSCRPTR_QUEUE0_WORD3_REG_OFFSET			0xE8CUL
#define DX_DSCRPTR_QUEUE0_WORD4_REG_OFFSET			0xE90UL
#define DX_DSCRPTR_QUEUE0_CONTENT_REG_OFFSET			0xE98UL

#define HASH_RESULT_SIZE_IN_WORDS		5
#define HASH_RESULT_SIZE_IN_BYTES		20
#define HASH_LENGTH_SIZE_IN_BYTES		16

#define DX_DMA_ALIGN_SIZE	64
#define MAX_TRANS_SIZE		0x30000		/* 18bits max */

#define WAIT_REG_ON_COND(var, reg, cond)	\
do {						\
	var = readl(reg);			\
} while (cond)

static void dx_queue_sequence(uint32_t hwbase, uint32_t seqs[5])
{
	uint32_t slots;

	WAIT_REG_ON_COND(
		slots,
		hwbase + DX_DSCRPTR_QUEUE0_CONTENT_REG_OFFSET,
		(slots & 0xF) == 0);

	writel(seqs[0], hwbase + DX_DSCRPTR_QUEUE0_WORD0_REG_OFFSET);
	writel(seqs[1], hwbase + DX_DSCRPTR_QUEUE0_WORD1_REG_OFFSET);
	writel(seqs[2], hwbase + DX_DSCRPTR_QUEUE0_WORD2_REG_OFFSET);
	writel(seqs[3], hwbase + DX_DSCRPTR_QUEUE0_WORD3_REG_OFFSET);
	writel(seqs[4], hwbase + DX_DSCRPTR_QUEUE0_WORD4_REG_OFFSET);
}

void dx_clear_interrupt(uint32_t hwbase)
{
	writel(1 << DX_HOST_ICR_DSCRPTR_COMPLETION_BIT_SHIFT,
	       hwbase + DX_HOST_ICR_REG_OFFSET);
}

void dx_Wait_desc_completion(uint32_t hwbase)
{
	uint32_t irr, irr_mask;

	irr_mask = 1 << DX_HOST_IRR_DSCRPTR_COMPLETION_LOW_INT_BIT_SHIFT;

	WAIT_REG_ON_COND(irr,
			 hwbase + DX_HOST_IRR_REG_OFFSET,
			 (irr & irr_mask) == 0);

	WAIT_REG_ON_COND(irr, hwbase + DX_AXIM_MON_INFLIGHT8_REG_OFFSET,
			 (irr & 0xFF) != 0);

	irr = readl(hwbase + DX_DSCRPTR_COMPLETION_COUNTER0_REG_OFFSET);
	irr = readl(hwbase + DX_AXIM_MON_COMP8_REG_OFFSET);

	dx_clear_interrupt(hwbase);
}

static void dx_sha1_init(uint32_t hwbase)
{
	uint32_t seqs[5];
	ALLOC_CACHE_ALIGN_BUFFER(uint32_t,
				 sha1_larval, HASH_RESULT_SIZE_IN_WORDS);

	sha1_larval[0] = 0xC3D2E1F0;
	sha1_larval[1] = 0x10325476;
	sha1_larval[2] = 0x98BADCFE;
	sha1_larval[3] = 0xEFCDAB89;
	sha1_larval[4] = 0x67452301;

	/* flush input caches */
	flush_cache((uint32_t)sha1_larval, HASH_RESULT_SIZE_IN_WORDS);

	/* clear int status */
	writel(1 << DX_HOST_ICR_DSCRPTR_COMPLETION_BIT_SHIFT,
	       hwbase + DX_HOST_ICR_REG_OFFSET);

	/* init descriptor */
	seqs[0] = (uint32_t)sha1_larval;
	seqs[1] = 0x00000002 | (HASH_RESULT_SIZE_IN_BYTES << 2);
	seqs[2] = 0x00000000;
	seqs[3] = 0x00000000;
	seqs[4] = 0x01000425;
	dx_queue_sequence(hwbase, seqs);

	seqs[0] = 0x00000000;
	seqs[1] = 0x08000041;
	seqs[2] = 0x00000000;
	seqs[3] = 0x00000000;
	seqs[4] = 0x04000425;
	dx_queue_sequence(hwbase, seqs);
}

static void dx_sha1_finish(uint32_t hwbase, uint8_t output[20])
{
	uint32_t seqs[5];
	ALLOC_CACHE_ALIGN_BUFFER(uint8_t, dma_out, 20);

	/* invalidate caches */
	flush_cache((uint32_t)dma_out, 20);

	/* get results */
	seqs[0] = 0x00000000;
	seqs[1] = 0x00000000;
	seqs[2] = (uint32_t)dma_out;
	seqs[3] = 0x08000002 | (HASH_RESULT_SIZE_IN_BYTES << 2);
	seqs[4] = 0x080c052b;
	dx_queue_sequence(hwbase, seqs);
	dx_Wait_desc_completion(hwbase);

	memcpy(output, dma_out, 20);
}

static void dx_sha1_update(uint32_t hwbase, const uint8_t *input,
		unsigned int ilen, int last_blk)
{
	uint32_t seqs[5];
	ALLOC_CACHE_ALIGN_BUFFER(uint8_t, dummy, HASH_LENGTH_SIZE_IN_BYTES);

	if (!input || !ilen)
		return;

	if (last_blk) {
		seqs[0] = 0x00000000;
		seqs[1] = 0x00000000;
		seqs[2] = (uint32_t)dummy;
		seqs[3] = 0x08000002 | (HASH_LENGTH_SIZE_IN_BYTES << 2);
		seqs[4] = 0x0908042b;
		dx_queue_sequence(hwbase, seqs);
	}

	flush_cache((unsigned long)input, ilen);
	seqs[0] = (uint32_t)input;
	seqs[1] = 0x00000002 | (ilen << 2);
	seqs[2] = 0x00000000;
	seqs[3] = 0x00000000;
	seqs[4] = 0x00000007;
	dx_queue_sequence(hwbase, seqs);
}

void sha1_starts(sha1_context *ctx)
{
	ctx->total[0] = 0;
	ctx->total[1] = 0;

	dx_sha1_init(DX_BASE_ADDRESS);
}

void sha1_update(sha1_context *ctx, const uint8_t *input,
		 unsigned int ilen)
{
	unsigned int copied_pos, inbuff_pos, left_bytes;
	ALLOC_CACHE_ALIGN_BUFFER(uint8_t,
				 aligned_buf, DX_DMA_ALIGN_SIZE);

	if (ilen <= 0 || input == NULL)
		return;

	/* record the length consumed */
	ctx->total[0] += ilen;
	ctx->total[0] &= 0xFFFFFFFF;

	if (ctx->total[0] < (uint32_t)ilen)
		ctx->total[1]++;

	/* dxsha1 engine needs dma at DX_DMA_ALIGN_SIZE boundary,
	   copy unalignment data into aligned buffers */
	inbuff_pos = 0;
	copied_pos = 0;
	while ((((uint32_t)&input[inbuff_pos]) & 0x3f) && ilen) {
		aligned_buf[copied_pos++] = input[inbuff_pos++];
		ilen--;
	}

	/* fetch header first */
	dx_sha1_update(DX_BASE_ADDRESS, aligned_buf, copied_pos, !ilen);

	/* then main body */
	while (ilen > MAX_TRANS_SIZE) {
		/* dxsha1 needs last bock indicator -- :( */
		left_bytes = ilen - MAX_TRANS_SIZE;
		dx_sha1_update(DX_BASE_ADDRESS,
			       &input[inbuff_pos], MAX_TRANS_SIZE, !left_bytes);
		ilen = left_bytes;
		inbuff_pos += MAX_TRANS_SIZE;
	}

	dx_sha1_update(DX_BASE_ADDRESS,  &input[inbuff_pos], ilen, 1);
}

void sha1_finish(sha1_context *ctx, uint8_t output[20])
{
	dx_sha1_finish(DX_BASE_ADDRESS, output);
}
