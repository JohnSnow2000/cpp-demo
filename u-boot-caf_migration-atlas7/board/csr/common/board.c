/*
 * Copyright (c) 2012-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <video_fb.h>
#include <asm/io.h>
#include <asm/processor.h>
#include <asm/arch/mmc.h>
#include <asm/arch/nand.h>
#include <asm/arch/efuse.h>
#include <asm/arch/rst.h>
#include <asm/arch/rsc.h>
#include <asm/arch/rom.h>
#include <asm/arch/gpio.h>
#include <asm/arch/clk.h>
#include <asm/arch/cpu.h>
#include <asm/arch/i2c.h>
#include <asm/arch/ipc.h>
#include <watchdog.h>
#include <exports.h>

#ifdef CONFIG_ARCH_ATLAS7
#include <asm/arch/ioctop.h>
#include <asm/arch/clkc.h>
#endif

#include <sdhci.h>
#include <libfdt.h>

#include "secondary_core.h"
#include <asm/arch/board.h>
#include "dxotprw.h"

DECLARE_GLOBAL_DATA_PTR;
#define PRIMA2_BOOTMODE_NAND_MASK	0x3f
#define PRIMA2_BOOTMODE_MASK		0x0f
#define PRIMA2_BOOTMODE_SD0_1		0x0d
#define PRIMA2_BOOTMODE_SD0_2		0x0e
#define PRIMA2_BOOTMODE_SD1_SHIFT	4
#define PRIMA2_BOOTMODE_SD1		0x01
#define ATLAS6_BOOTMODE_NAND_MASK	0x7
#define ATLAS6_BOOTMODE_SD0		3
#define ATLAS6_BOOTMODE_SD0BOOT		7
#define ATLAS6_BOOTMODE_SD2		2
#define ATLAS6_REALBOOTMODE		0xa8008034
#define ATLAS6_BM_SERIAL		8
#define ATLAS7_BM_SERIAL		8
#define ATLAS7_BM_DRAM			9
#define ATLAS7_BOOTMODE_REG		0x1023001c

enum mmcbootmode {
	BM_SD0,
	BM_SD0BOOT,
	BM_SD1,
	BM_SPI,
	BM_OTHER
};
static int nanddisk_init_result;
#ifdef CONFIG_ARCH_ATLAS7
struct cpu_rwm_init_data {
	u32 cpu0_config0;
	u32 cpu0_config1;
	u32 cpu1_config0;
	u32 cpu1_config1;
	u32 l2_sram_config;
};

#define sirf_read_boot_status	readl
#define sirf_write_boot_status	writel

#define INTC_BASE				0x10220000
#define INTC_DEVID_OFS			0x48
#define INTC_DEV_ID_A0			0xa0
#define INTC_DEV_ID_A1			0xa1
#define INTC_DEV_ID_B0			0xb0
#define INTC_DEV_ID_B1			0xb1
#define INTC_DEV_ID_B2			0xb2
#define INTC_DEV_ID_B3			0xb3
#else
#define sirf_read_boot_status	sirfsoc_cpu_iobrg_readl
#define sirf_write_boot_status	sirfsoc_cpu_iobrg_writel
#endif

#if defined(CONFIG_ARCH_PRIMAII)
static int getmmcbootmode(void)
{
	int bootmedia;
	bootmedia =
		readl(NAND_BASE + NAND_BOOTMODE) & PRIMA2_BOOTMODE_NAND_MASK;

	if ((bootmedia >> 4 & 0x3) == 0x1)
		return BM_SD1;
	else if ((bootmedia & 0xf) == 0xd)
		return BM_SD0;
	else if ((bootmedia & 0xf) == 0xe)
		return BM_SD0BOOT;
	else if ((bootmedia & 0x5) == 0x5)
		return BM_SPI;
	else
		return BM_OTHER;
}
#elif defined(CONFIG_ARCH_ATLASVI) || defined(CONFIG_ARCH_ATLAS7)
static int getmmcbootmode(void)
{
	int bootmedia;
#if defined(CONFIG_ARCH_ATLASVI)
	bootmedia = readl(ATLAS6_REALBOOTMODE) & 0xf;
	if (bootmedia == ATLAS6_BM_SERIAL)
		/* boot from launcher through uart, check board bootmode */
		bootmedia =
			readl(NAND_BASE + NAND_BOOTMODE) & ATLAS6_BOOTMODE_NAND_MASK;
#elif defined(CONFIG_ARCH_ATLAS7)
	bootmedia = readl(ROM_RETURN_ADRS + 0x14) & 0xf;
	if (bootmedia == ATLAS6_BM_SERIAL)
		bootmedia = readl(ATLAS7_BOOTMODE_REG) & 0x7;
#endif

	if ((bootmedia & 0x7) == 0x2)
		return BM_SD1;
	else if ((bootmedia & 0x7) == 0x3)
		return BM_SD0;
	else if ((bootmedia & 0x7) == 0x7)
		return BM_SD0BOOT;
	else if ((bootmedia & 0x5) == 0x5)
		return BM_SPI;
	else
		return BM_OTHER;
}
#endif

#if !defined(CONFIG_ARCH_ATLAS7)
static u32 sirfsoc_cpu_iobrg_readl(u32 addr)
{
	while (__raw_readl(CPU_IO_BRIDGE + CPUIOBRG_CTRL))
		cpu_relax();
	__raw_writel(0x0, CPU_IO_BRIDGE + CPUIOBRG_WRBE);
	__raw_writel(addr, CPU_IO_BRIDGE + CPUIOBRG_ADDR);
	__raw_writel(0x1, CPU_IO_BRIDGE + CPUIOBRG_CTRL);
	while (__raw_readl(CPU_IO_BRIDGE + CPUIOBRG_CTRL))
		cpu_relax();

	return __raw_readl(CPU_IO_BRIDGE + CPUIOBRG_DATA);
}

static void sirfsoc_cpu_iobrg_writel(u32 val, u32 addr)
{

	while (__raw_readl(CPU_IO_BRIDGE + CPUIOBRG_CTRL))
		cpu_relax();
	__raw_writel(0x1, CPU_IO_BRIDGE + CPUIOBRG_WRBE);
	__raw_writel(addr, CPU_IO_BRIDGE + CPUIOBRG_ADDR);
	__raw_writel(val, CPU_IO_BRIDGE + CPUIOBRG_DATA);
	__raw_writel(0x1, CPU_IO_BRIDGE + CPUIOBRG_CTRL);
	while (__raw_readl(CPU_IO_BRIDGE + CPUIOBRG_CTRL))
		cpu_relax();

	return;
}
#endif

#ifdef CONFIG_ARCH_ATLAS7
static void sirfsoc_board_ipc2m3(u32 data)
{
	writel(data, A7DA_IPC_CONTENT);
	writel(A7DA_IPC_SEND, A7DA_IPC_A72M3_REG);
}
#endif

static void sirfsoc_board_shutdown(void)
{
#if !defined(CONFIG_ARCH_ATLAS7)
	sirfsoc_cpu_iobrg_writel(sirfsoc_cpu_iobrg_readl(PWRC_PDN_CTRL) |
		PWRC_PDN_START_BIT | (PWRC_PDN_MODE_BIT | 0x11), PWRC_PDN_CTRL);
#else
	sirfsoc_board_ipc2m3(A7DA_POWER_OFF);
#endif
}
static void sirfsoc_board_reset(void)
{
	puts ("resetting ...\n");

	udelay (50000);				/* wait 50 ms */

	disable_interrupts();

#if !defined(CONFIG_ARCH_ATLAS7)
	reset_cpu(0);
#else
	sirfsoc_board_ipc2m3(A7DA_POWER_RST);
#endif
}

int dram_init (void)
{
	gd->ram_size = CONFIG_SYS_SDRAM_SIZE;

	puts("DRAM total: ");
	print_size(gd->ram_size, "\n");
	return 0;
}

void sirfsoc_cmd_err_handler(char *arg)
{
#ifdef CONFIG_KERNEL_BACKUP
	if (
		(strstr(arg, "load") && strstr(arg, "fdtfile")) ||
		(strstr(arg, "fdt") && (strstr(arg, "addr") ||
		strstr(arg, "resize"))) ||
		(strstr(arg, "load") && strstr(arg, "imgfile")) ||
		strstr(arg, "bootz")) {
		sirfsoc_board_reset();
	}
#else
	return;
#endif
}

#ifdef CONFIG_KERNEL_BACKUP
#define BOOT_CONFIG_MAX_SIZE	128
void sirfsoc_clean_kernel_backup_info(void)
{
	sirfsoc_set_watchdog(0, 0);
	sirf_write_boot_status(0, SIRFSOC_BOOT_STATUS);
}

static char* sirfsoc_get_boot_config(char *opt)
{
	char *delim = "=;\n";
	static char boot_cfg_copy[BOOT_CONFIG_MAX_SIZE];
	char *boot_cfg, *boot_cfg_copy_temp;
	char *p;

	boot_cfg = (char *)getenv_hex("bootcfgaddr", 0);
	strncpy(boot_cfg_copy, boot_cfg, BOOT_CONFIG_MAX_SIZE);
	/*
	 * strseq will update its first parameter, keep boot_cfg_copy
	 * unchanged for next copy of boot_cfg.
	 */
	boot_cfg_copy_temp = boot_cfg_copy;
	while ((p = strsep(&boot_cfg_copy_temp, delim)) != NULL) {
		if (!strcmp(p, opt))
			return strsep(&boot_cfg_copy_temp, delim);
	}

	return NULL;
}

static void sirfsoc_apply_boot_config(void)
{
	int idx;
	char tmp[128];
	char *os_type;
	char *str;

	idx = getenv_ulong(CONFIG_KERNEL_INDEX, 10, 0);
	if ((idx != 1) && (idx != 2)) {
		str = sirfsoc_get_boot_config(CONFIG_KERNEL_INDEX);
		if (str)
			idx = simple_strtoul(str, NULL, 10);
		setenv_ulong(CONFIG_KERNEL_INDEX, idx);
	}

	os_type = sirfsoc_get_boot_config("os_type");
	if (!os_type)
		printf("Warning: os_type configuration does not exist!\n");

	if (os_type && !strcmp(os_type, "linux")) {
		/*
		 * Just linux change the root part and resume part num
		 */
		sprintf(tmp, "%d", CONFIG_LINUX_ROOT_PARTITION);
		setenv("rootpart", tmp);
		sprintf(tmp, "%s-v%d", "/zImage", idx);
	} else {
		sprintf(tmp, "%s-v%d", "/uImage", idx);
	}
#ifdef CONFIG_ARCH_ATLAS7
	sprintf(tmp, "%s-v%d", "/zImage", idx);
#endif
	setenv("imgfile", tmp);
	sprintf(tmp, "%s-v%d", "/dtb", idx);
	setenv("fdtfile", tmp);

	idx = getenv_ulong(CONFIG_RECOVERY_INDEX, 10, 0);
	if ((idx != 1) && (idx != 2)) {
		str = sirfsoc_get_boot_config(CONFIG_RECOVERY_INDEX);
		if (str) {
			idx = simple_strtoul(str, NULL, 10);
			setenv_ulong(CONFIG_RECOVERY_INDEX, idx);
		}
	}
	sprintf(tmp, "%s-v%d", "/recovery.img", idx);
	setenv("rcvfile", tmp);
	sprintf(tmp, "%s-v%d", "/rcv_dtb", idx);
	setenv("rcvfdtfile", tmp);

	if (os_type)
		setenv("os_type", os_type);
}

static void sirfsoc_change_image_index(char *idx_name)
{
	int idx = 1;
	char *str;

	str = sirfsoc_get_boot_config(idx_name);
	if (str)
		idx = simple_strtoul(str, NULL, 10);
	else
		printf("Warning: %s configuration does not exist\n", idx_name);

	setenv_ulong(idx_name, (idx == 1) ? 2 : 1);
}

static void sirfsoc_load_boot_cfg(void)
{
	char *boot_cfg;

	run_command(getenv("loadbootcfg"), 0);
	boot_cfg = (char *)getenv_hex("bootcfgaddr", 0);
	boot_cfg[BOOT_CONFIG_MAX_SIZE - 1] = '\0';
}

static void sirfsoc_check_boot_status(void)
{
	u32 boot_status = sirf_read_boot_status(SIRFSOC_BOOT_STATUS);

	if (!(boot_status & RECOVERY_MODE)) {
		if (!(boot_status & KERNEL_BOOTING)) {
			sirf_write_boot_status(
				sirf_read_boot_status(SIRFSOC_BOOT_STATUS) |
				KERNEL_BOOTING,
				SIRFSOC_BOOT_STATUS);
		}
		if (boot_status & KERNEL_BOOTING) {
			if (boot_status & KERNEL_BOOTING_FAIL) {
				if (boot_status & MP_MODE_BOOT) {
					printf("!!kernel image is not available, shutdown!!");
					udelay(2000);
					sirfsoc_board_shutdown();
				} else {
					printf("normal boot failed, try mp boot mode\n");
					sirf_write_boot_status(
						sirf_read_boot_status(
							SIRFSOC_BOOT_STATUS) |
							MP_MODE_BOOT,
							SIRFSOC_BOOT_STATUS);
					/* For MP boot is 1st partition */
					setenv("bootpart", "1");
#ifndef SECURE_BOOT
					setenv("bootcmd2", "run devboot_mp");
#else
					setenv("bootcmd2", "run devseboot_mp");
#endif
				}
			} else {
				printf("last kernel booting failed, change kernel index and try again\n");
				sirf_write_boot_status(
					sirf_read_boot_status(
						SIRFSOC_BOOT_STATUS) |
						KERNEL_BOOTING_FAIL,
						SIRFSOC_BOOT_STATUS);
				sirfsoc_change_image_index(CONFIG_KERNEL_INDEX);
			}
		}
	} else {
		setenv("bootcmd2", "run devbootrcv");
		if (!(boot_status & RECOVERY_BOOTING)) {
			sirf_write_boot_status(
				sirf_read_boot_status(SIRFSOC_BOOT_STATUS) |
				RECOVERY_BOOTING,
				SIRFSOC_BOOT_STATUS);
		}
		if (boot_status & RECOVERY_BOOTING) {
			if (boot_status & RECOVERY_BOOTING_FAIL) {
				printf("!!recovery image is not available, shutdown!!");
				udelay(2000);
				sirfsoc_board_shutdown();
			} else {
				printf("last recovery booting failed, change recovery index and try again\n");
				sirf_write_boot_status(
					sirf_read_boot_status(
						SIRFSOC_BOOT_STATUS) |
						RECOVERY_BOOTING_FAIL,
						SIRFSOC_BOOT_STATUS);
				sirfsoc_change_image_index(CONFIG_RECOVERY_INDEX);
			}
		}
	}
}

#ifdef CONFIG_ARCH_ATLAS7
#define ATLAS7_PWRC_XTAL_REG		0x3064
#define ATLAS7_PWRC_XTALLDO_MUX	0x3068
#define XTAL_DEF_VAL			0x200900

static void sirfsoc_apply_xtal_config(void)
{
	u32 val;
	int need_fix = 0;
	char *str_xtal;
	/* if there is a valid xtal config, do fix */
	str_xtal = sirfsoc_get_boot_config("xtal");
	if (str_xtal &&
	    strict_strtoul(str_xtal, 16, (unsigned long *)&val) == 0) {
		need_fix = 1;
	} else {
		int hwver;

		printf("Warning: valid BT xtal config doesn't exist\n");
		/*
		 * even though there is no valid xtal for B1, do fix bytes
		 * default value
		 */
		hwver = readl(INTC_BASE + INTC_DEVID_OFS) & 0xff;
		if (hwver == INTC_DEV_ID_B1 || hwver == INTC_DEV_ID_B2 ||
		    hwver == INTC_DEV_ID_B3) {
			need_fix = 1;
			val = XTAL_DEF_VAL;
		}
	}
	if (!need_fix)
		return;
	sirfsoc_rtc_iobrg_writel(val, ATLAS7_PWRC_XTAL_REG);
	sirfsoc_rtc_iobrg_writel(0x303f80, ATLAS7_PWRC_XTALLDO_MUX);
}
#endif

#endif

#if defined(CONFIG_SECURE_RECOVERY)
#define RFLAG_ADDR	0x0027f000 /* 2.5MB-4096 */
#define RFLAG_START	0x5a
#define RFLAG_FINISH	0xa5
static int get_recovery_flag(block_dev_desc_t *bdev, int devn)
{
	unsigned long blk;
	unsigned int buf[1024];
	if (bdev == NULL)
		return -1;
	blk = RFLAG_ADDR / bdev->blksz;

	bdev->block_read(devn, blk, 1, buf);
	printf("RCVFLAG:read 1 sector(%lu bytes), buf[0]=0x%08x\n"
			, bdev->blksz, buf[0]);

	return buf[0];
}
#endif

#ifdef CONFIG_CSRVISOR_SUPPORT
static void wakeup_secondary_core(void)
{
	int i;
	debug("Wakeup CPU1 and set to non-secure mode...");
	writel((unsigned long)sirf_smp_pen, SMP_PEN_ADDR);
	writel(SMP_WAKEMAGIC_VALUE, SMP_WAKEMAGIC_ADDR);
	asm volatile("dsb " : : : "memory");
	asm volatile("sev" : : : "memory");

	for (i = 0; i < 50; i++) {
		udelay(1);
		if (readl(SMP_WAKEMAGIC_ADDR) == 0) {
			debug("done.\n");
			return;
		}
	}

	printf("Error: Can't wakeup CPU1 and set to non-secure mode\n");
}
#endif

/*
 * enable the long press of ON_KEY to force shutdown
 * this is pwrc hardware behavior, default key press
 * duration is 5 seconds.
 */
#if defined(CONFIG_ARCH_ATLAS7)
#define SHTDN_5S	0x0 /* 0: Force shutdown in 5 secs */
static void a7_onkey_shutdown_en(void)
{
	sirfsoc_rtc_iobrg_writel(PWRC_PDN_WATCHDOG_STOP |
				 PWRC_PDN_ONKEY_WATCHDOG_EN |
				 PWRC_PDN_EXTON_WATCHDOG_EN |
				 SHTDN_5S << PWRC_PDN_FORCE_SHUTDOWN_TIME,
				 PWRC_REG(PWRC_PDN_CTRL_SET));
	sirfsoc_rtc_iobrg_writel(PWRC_RTC_SHUTDOWN_EN,
				 PWRC_REG(PWRC_RTC_DCOG));
}

#define A7_DMA_OWNER_KAS	0x1b6db6db
#define A7_DMA_OWNER_REG0	0x164
#define A7_DMA_OWNER_REG1	0x168

static void a7_set_dmac_default_owner(void)
{
	int i;
	u32 dmac_regs[] = {
			0x18000000,
			0x133F0000,
			0x10D50000,
			0x10D60000,
			0x11002000
		};

	/* Enable DMACs' clock */
	writel(0x1, CLKC_DMAC0_LEAF_CLK_EN_SET);
	writel(0x40000000, CLKC_ROOT_CLK_EN1_SET);
	writel(0x8, CLKC_DMAC1_LEAF_CLK_EN_SET);
	writel(0x1, CLKC_DMAC2_LEAF_CLK_EN_SET);
	writel(0x1, CLKC_DMAC3_LEAF_CLK_EN_SET);
	writel(0x64, CLKC_BT_LEAF_CLK_EN_SET);

	for (i = 0; i < ARRAY_SIZE(dmac_regs); i++) {
		writel(A7_DMA_OWNER_KAS, dmac_regs[i] + A7_DMA_OWNER_REG0);
		writel(A7_DMA_OWNER_KAS, dmac_regs[i] + A7_DMA_OWNER_REG1);
	}
}
#endif

#define OFFSET_ERR_GAIN0_REG		(0x640 >> 5)
#define OFFSET_ERR_GAIN2_REG		(0x660 >> 5)
#define OFFSET_ERR_BITS_MASK		0x3fff
#define GAIN_ERR_GAIN0_REG			(0x5c0 >> 5)
#define GAIN_ERR_GAIN2_REG			(0x560 >> 5)
#define GAIN_ERR_BITS_MASK			0xfff

#define GAIN_ERR_BITS_LOW_MASK				0xffff
#define GAIN_ERR_BITS_HIGH_MASK				0xffff0000
#define GAIN_ERR_BITS_HIGH_offset			16


#define GAIN_ERR_GAIN23_CH1_2_REG			(0x560 >> 5)
#define GAIN_ERR_GAIN45_CH1_2_REG			(0x580 >> 5)
#define GAIN_ERR_GAIN67_CH1_2_REG			(0x5a0 >> 5)
#define GAIN_ERR_GAIN01_CH3_4_5_6_REG			(0x5c0 >> 5)
#define GAIN_ERR_GAIN23_CH3_4_5_6_REG			(0x5e0 >> 5)
#define GAIN_ERR_GAIN45_CH3_4_5_6_REG			(0x600 >> 5)
#define GAIN_ERR_GAIN67_CH3_4_5_6_REG			(0x620 >> 5)
static void ft_board_fixup_adc(void *blob)
{
#ifdef CONFIG_ARCH_ATLAS7
	int offset;
	int err = 0;
	unsigned long adc_offset0, adc_offset2;
	unsigned long temp;
	unsigned long adc_gain2_ch_1_2, adc_gain3_ch_1_2,
		adc_gain4_ch_1_2, adc_gain5_ch_1_2,
		adc_gain6_ch_1_2, adc_gain7_ch_1_2;
	unsigned long adc_gain0_ch_3_4_5_6, adc_gain1_ch_3_4_5_6,
		adc_gain2_ch_3_4_5_6, adc_gain3_ch_3_4_5_6,
		adc_gain4_ch_3_4_5_6, adc_gain5_ch_3_4_5_6,
		adc_gain6_ch_3_4_5_6, adc_gain7_ch_3_4_5_6;
	char *compatible = "sirf,atlas7-adc";


	/*
	 *Gain error values for channel 1 and 2
	 */
	read_OTP_word(GAIN_ERR_GAIN23_CH1_2_REG, &temp);
	adc_gain2_ch_1_2 = temp & GAIN_ERR_BITS_LOW_MASK;
	adc_gain3_ch_1_2 = (temp & GAIN_ERR_BITS_HIGH_MASK)
		>> GAIN_ERR_BITS_HIGH_offset;


	read_OTP_word(GAIN_ERR_GAIN45_CH1_2_REG, &temp);
	adc_gain4_ch_1_2 = temp & GAIN_ERR_BITS_LOW_MASK;
	adc_gain5_ch_1_2 = (temp & GAIN_ERR_BITS_HIGH_MASK)
		>> GAIN_ERR_BITS_HIGH_offset;


	read_OTP_word(GAIN_ERR_GAIN67_CH1_2_REG, &temp);
	adc_gain6_ch_1_2 = temp & GAIN_ERR_BITS_LOW_MASK;
	adc_gain7_ch_1_2 = (temp & GAIN_ERR_BITS_HIGH_MASK)
		>> GAIN_ERR_BITS_HIGH_offset;

	/*
	 *Gain error values for channel 3, 4, 5 and 6
	 */
	read_OTP_word(GAIN_ERR_GAIN01_CH3_4_5_6_REG, &temp);
	adc_gain0_ch_3_4_5_6 = temp & GAIN_ERR_BITS_LOW_MASK;
	adc_gain1_ch_3_4_5_6 = (temp & GAIN_ERR_BITS_HIGH_MASK)
		>> GAIN_ERR_BITS_HIGH_offset;

	read_OTP_word(GAIN_ERR_GAIN23_CH3_4_5_6_REG, &temp);
	adc_gain2_ch_3_4_5_6 = temp & GAIN_ERR_BITS_LOW_MASK;
	adc_gain3_ch_3_4_5_6 = (temp & GAIN_ERR_BITS_HIGH_MASK)
		>> GAIN_ERR_BITS_HIGH_offset;


	read_OTP_word(GAIN_ERR_GAIN45_CH3_4_5_6_REG, &temp);
	adc_gain4_ch_3_4_5_6 = temp & GAIN_ERR_BITS_LOW_MASK;
	adc_gain5_ch_3_4_5_6 = (temp & GAIN_ERR_BITS_HIGH_MASK)
		>> GAIN_ERR_BITS_HIGH_offset;


	read_OTP_word(GAIN_ERR_GAIN67_CH3_4_5_6_REG, &temp);
	adc_gain6_ch_3_4_5_6 = temp & GAIN_ERR_BITS_LOW_MASK;
	adc_gain7_ch_3_4_5_6 = (temp & GAIN_ERR_BITS_HIGH_MASK)
		>> GAIN_ERR_BITS_HIGH_offset;

	read_OTP_word(OFFSET_ERR_GAIN0_REG, &adc_offset0);
	read_OTP_word(OFFSET_ERR_GAIN2_REG, &adc_offset2);

	adc_offset0 &= OFFSET_ERR_BITS_MASK;
	adc_offset2 &= OFFSET_ERR_BITS_MASK;
	debug("%x-%x\n", (u32)adc_offset0, (u32)adc_offset2);
	offset = fdt_node_offset_by_compatible(blob, -1, compatible);

	if (offset < 0)
		return;

	err |= fdt_setprop_cell(blob, offset, "cali-offset0", adc_offset0);
	err |= fdt_setprop_cell(blob, offset, "cali-offset2", adc_offset2);

	err |= fdt_setprop_cell(blob, offset,
			"cali-gain2-ch1-2", adc_gain2_ch_1_2);
	err |= fdt_setprop_cell(blob, offset,
			"cali-gain3-ch1-2", adc_gain3_ch_1_2);
	err |= fdt_setprop_cell(blob, offset,
			"cali-gain4-ch1-2", adc_gain4_ch_1_2);
	err |= fdt_setprop_cell(blob, offset,
			"cali-gain5-ch1-2", adc_gain5_ch_1_2);
	err |= fdt_setprop_cell(blob, offset,
			"cali-gain6-ch1-2", adc_gain6_ch_1_2);
	err |= fdt_setprop_cell(blob, offset,
			"cali-gain7-ch1-2", adc_gain7_ch_1_2);

	err |= fdt_setprop_cell(blob, offset,
			"cali-gain0-ch3-4-5-6", adc_gain0_ch_3_4_5_6);
	err |= fdt_setprop_cell(blob, offset,
			"cali-gain1-ch3-4-5-6", adc_gain1_ch_3_4_5_6);
	err |= fdt_setprop_cell(blob, offset,
			"cali-gain2-ch3-4-5-6", adc_gain2_ch_3_4_5_6);
	err |= fdt_setprop_cell(blob, offset,
			"cali-gain3-ch3-4-5-6", adc_gain3_ch_3_4_5_6);
	err |= fdt_setprop_cell(blob, offset,
			"cali-gain4-ch3-4-5-6", adc_gain4_ch_3_4_5_6);
	err |= fdt_setprop_cell(blob, offset,
			"cali-gain5-ch3-4-5-6", adc_gain5_ch_3_4_5_6);
	err |= fdt_setprop_cell(blob, offset,
			"cali-gain6-ch3-4-5-6", adc_gain6_ch_3_4_5_6);
	err |= fdt_setprop_cell(blob, offset,
			"cali-gain7-ch3-4-5-6", adc_gain7_ch_3_4_5_6);

	if (err)
		printf("unable to set adc val for %s\n", compatible);
#endif
	return;
}
#define ATLAS7_UPDATE_FLAG	0x424F4F54
#define ATLAS7_UART_BOOT	0x8
static void uart_boot_ack(void)
{
	int i;
	u32 mode;
	u32 flag;
	u8 cmd_buf[] = {0xa1, 0xb2, 0xc3, 0x22,
				 0x0, 0x0, 0x0, 0x0,
				 0x0, 0x0, 0x0, 0x0,
				 0xa1, 0xb2, 0xc3, 0x44,
				 0xa5, 0xa5, 0xa5, 0xa5};

	/* check whether booted from UART,
	 * if true, need to send ACK to uart launcher.
	 * Otherwise, it's normal boot, do nothing.
	 */
	flag = readl(ROM_RETURN_ADRS + 0x0c);
	mode = readl(ROM_RETURN_ADRS + 0x14);
	if (!((flag == ATLAS7_UPDATE_FLAG) && (mode == ATLAS7_UART_BOOT)))
		return;

	for (i = 0; i < sizeof(cmd_buf); i++)
		printch(cmd_buf[i]);
}

#if defined(CONFIG_ARCH_ATLAS7)
static void sirfsoc_ioc_set_nand(void);
static void sirfsoc_ioc_set_sd(int);
#endif
#ifdef CONFIG_ARCH_ATLAS7
/*
 * A7DA's chip FSA3357K8X have a mux to control i2s_dout1/2 or uart4_rts/cts
 * signal to pin 39/41 at Slot3. the mux with two inputs: I2S_SW3_SEL_B and
 * 5V_I2S_MX where 5V_I2S_MX is high always and I2S_SW3_SEL_B is the revert
 * value of I2S_SW3_SEL. when the pair is (0,1) the output is i2s_dout1/2,
 * when the pair is (1,1) the output is uart4_rts/cts.
 * CSR amber bt chip use UART4 with hardware flow control, so enable it here.
 */
static int sirfsoc_set_sw3_sel_low(void)
{
	i2c_dev_reg_bit_clear(0, 0x40, 0, 0x40);
	i2c_dev_reg_bit_clear(0, 0x40, 2, 0x40);
	return 0;
}

static uint32_t core_rwm[] = {0, 0, 0, 0, 0, 0, 0};
static struct cpu_rwm_init_data cpu_rwm_list[] = {
	{0, 0, 0, 0, 0x2},
	{0x02020202, 0x020202, 0x02020202, 0x020202, 0x2},
	{0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0},
};

static void board_set_rwm(void)
{
	uint32_t rwm_config, rwm_index, ret;
	struct cpu_rwm_init_data *cpu_rwm;

	ret = read_OTP_word(63, (ulong *)&rwm_config);
	if (ret != 0) {
		debug("svm:Failed to  read from OTP\n");
		return;
	}
	/*core*/
	if (rwm_config & BIT(0))
		rwm_index = rwm_config>>1 & 0x7;

	else
		rwm_index = rwm_config>>9 & 0x7;

	printf("core svm index:%d\n", rwm_index);
	if (rwm_index)
		writel(core_rwm[--rwm_index], PWRC_RTCM_AMP_ADJUST_F);
	/*cpu*/
	rwm_index = 0;
	if (rwm_config & BIT(16))
		rwm_index = rwm_config>>17 & 0x7;
	else
		rwm_index = rwm_config>>25 & 0x7;

	printf("cpu svm index:%d\n", rwm_index);
	if (rwm_index)	 {
		cpu_rwm = &cpu_rwm_list[--rwm_index];
		writel(cpu_rwm->cpu0_config0, CPU_REG(0x0));
		writel(cpu_rwm->cpu0_config1, CPU_REG(0x4));
		writel(cpu_rwm->cpu1_config0, CPU_REG(0x8));
		writel(cpu_rwm->cpu1_config1, CPU_REG(0xc));
		writel(cpu_rwm->l2_sram_config, CPU_REG(0x10));
	}
}

struct NOCFW_T {
	u32 regoff;
	u32 val;
};

/*
 *Bits		Address range 		Target mapping name
 *1	0x188C0000–0x188CFFFF		hash_reg
 *2	0x188B0000–0x188BFFFF		qspi_reg
 *3	0x20000000–0x27FFFFFF		qspi_xip
 *4	0x188D0000–0x188DFFFF		retain_reg
 *5	0x18830000–0x1883FFFF		rtcm_m3_reg_hub/armm3
 *6	0x18813000–0x18813FFF		rtcm_m3_reg_hub/th
 *7	0x18890000–0x1889FFFF		rtcm_rtc_reg_hub/gpio_rtc
 *8	0x18880000–0x1888FFFF		rtcm_rtc_reg_hub/ioc_rtc
 *9	0x18840000–0x1884FFFF		rtcm_rtc_reg_hub/pwrc_sys_rtc_io_bridge
 *10	0x18810000–0x18810FFF		rtcm_service/non_secure
 *11	0x188A0000–0x188A7FFF		cm3_retain_mem
 *		0x188A8000–0x188AFFFF		cm3_spram
 *12	0x18811000–0x18811FFF		rtcm_service/secure_m3
 *13	0x18880000–0x1880FFFF		canbus0_reg
 *14	0x18812000–0x18812FFF		rtcm_service/secure_reg
 *15	0x18A00000–0x18A1FFFF		rtcm_gpio_fw_reg
 *16	0x18850000–0x18857FFF		rtcm_m3_reg_hub/timer
 */
const  struct NOCFW_T g_nocfw_tbl[] = {
	{0x18812054, 0x00000002},
	{0x18812054, 0x00000008},
	{0x18812054, 0x00000010},
	{0x18812054, 0x00000020},
	{0x18812054, 0x00000040},
	{0x18812054, 0x00000080},
	{0x18812054, 0x00000200},
	{0x18812054, 0x00000800},
	{0x18812054, 0x00001000},
	{0x18812054, 0x00002000},
	{0x18812054, 0x00008000},
	{0x18812054, 0x00010000},
	{0x18812054, 0x00004000},
	{0x18101054, 0x00000002},
	{0x18101054, 0x00010000},
	{0x18101054, 0x00004000},
	{0x18101054, 0x00002000},
	{0x10201054, 0x00000080},
	{0x10201054, 0x00000040},
	{0x10250000, 0x04000000},
	{0x10250004, 0x04020000},
	{0x10250064, 0x000000ff},
	{0x10250060, 0x00000011},
	{0x10250074, 0x00000022},
	{0x1025007c, 0x00000022},
	{0x10250070, 0x00000022},
	{0x10253f04, 0x00000001},
	{0x10250100, 0x0402fc00},
	{0x10250104, 0x04030000},
	{0x10250164, 0x000000ff},
	{0x10250160, 0x00000011},
	{0x10250174, 0x00000022},
	{0x1025017c, 0x00000022},
	{0x10250170, 0x00000022},
	{0x10253f04, 0x00000002},
	{0x10250014, 0xFFFFFFFF},/*rp0:clear non-cpu*/
	{0x1025001C, 0xFFFFFFFF},
	{0x10250024, 0xFFFFFFFF},
	{0x1025002C, 0xFFFFFFFF},
	{0x10250034, 0xFFFFFFFF},
	{0x1025003C, 0xFFFFFFFF},
	{0x10250044, 0xFFFFFFFF},
	{0x1025004c, 0xFFFFFFFF},
	{0x10250010, 0x00000300},/*usb0,usb1*/
	{0x10250018, 0x00000300},
	{0x10250020, 0x04000000},/*rp0:set non-cpu*/
	{0x10250028, 0x04000000},
	{0x10250020, 0x00000080},
	{0x10250028, 0x00000080},
	{0x10250030, 0x00000001},
	{0x10250038, 0x00000001},
	{0x10250030, 0x00010000},
	{0x10250038, 0x00010000},
	{0x10250114, 0xFFFFFFFF},/*rp1:clear non-cpu*/
	{0x1025011C, 0xFFFFFFFF},
	{0x10250124, 0xFFFFFFFF},
	{0x1025012C, 0xFFFFFFFF},
	{0x10250134, 0xFFFFFFFF},
	{0x1025013C, 0xFFFFFFFF},
	{0x10250144, 0xFFFFFFFF},
	{0x1025014c, 0xFFFFFFFF},
	{0x10250120, 0x04000000},/*rp1:set non-cpu*/
	{0x10250128, 0x04000000},
	{0x10250120, 0x00000080},
	{0x10250128, 0x00000080},
	{0x10250130, 0x00000001},
	{0x10250138, 0x00000001},
	{0x10250130, 0x00010000},
	{0x10250138, 0x00010000},
	{0x10821054, 0x00000300},/*usb0,usb1*/
	{0x10821084, 0x00000300},
	{0x10821060, 0x04000000},/*sec*/
	{0x10821090, 0x04000000},
	{0x10821060, 0x00000080},/*nand*/
	{0x10821090, 0x00000080},
	{0x1082106C, 0x00000001},/*sd2*/
	{0x1082109C, 0x00000001},
	{0x1082106C, 0x00010000},/*sd0*/
	{0x1082109C, 0x00010000},
	{0x10822054, 0x00000800},/*dram sec*/
	{0x10822054, 0x00001000},/*dram reg*/
	{0x18812054, 0x00000004}, /*qspi secure*/
	{0x18812060, 0x00000004}, /*disable qspi for A7*/
	{0x18812060, 0x0001b86a},
	{0x18812084, 0x0001fafe}, /*disable for KAS */
	{0x18101060, 0x00000002}, /*disable for A7 */
	{0x18101078, 0x00016000}, /*disable for M3 */
	{0x18101084, 0x00016002}, /*disable for KAS */
	{0x10201078, 0x000000c0}, /*disable for M3 */
	{0x10201084, 0x000000c0}, /*disable for KAS */
};

const  struct NOCFW_T g_nocfw_ext_tbl[] = {
	{0x18812050, 0x00000004},
	{0x1881205C, 0x00000004}, /*enable qspi for A7 */
	{0x18812084, 0x00000100}, /*ioc_rtc disable for KAS */
};


static void board_config_nocfw(void)
{
	const struct NOCFW_T *entry;
	int i;

	for (i = 0; i < ARRAY_SIZE(g_nocfw_tbl); i++) {
		entry = &g_nocfw_tbl[i];
		writel(entry->val, entry->regoff);
	}
}

static void board_config_ext_nocfw(void)
{
	const struct NOCFW_T *entry;
	int i;

	for (i = 0; i < ARRAY_SIZE(g_nocfw_ext_tbl); i++) {
		entry = &g_nocfw_ext_tbl[i];
		writel(entry->val, entry->regoff);
	}
}


#endif

static int nand_is_valid(void)
{
	block_dev_desc_t *nand = get_dev("nand", 0);

	if (!nand)
		return 0;

	if (nanddisk_init_result < 0)
		return 0;

	if (!nand->blksz)
		return 0;

	return 1;
}

#define OTP_FEATURES_SKU	0x3E
int board_init(void)
{
	unsigned int boot_media, nand_boot;
	unsigned long chip_info;

#ifdef CONFIG_CSRVISOR_SUPPORT
	wakeup_secondary_core();
#endif

#ifdef CONFIG_SVM_SUPPORT
	sirfsoc_svm_config();
#endif

#if defined(CONFIG_ARCH_ATLAS7)
	a7_onkey_shutdown_en();
	uart_boot_ack();
	a7_set_dmac_default_owner();
	read_OTP_word(OTP_FEATURES_SKU, &chip_info);
	printf("CHIP SKU Info: 0x%04X\n", (u32)(chip_info >> 18));

	boot_media = readl(ATLAS7_BOOTMODE_REG);
#else
	boot_media = readl(NAND_BASE + NAND_BOOTMODE);
#endif
	printf("boot media is 0x%x!\n", boot_media);

#if defined(CONFIG_ARCH_PRIMAII)
	/*
	 * nand and sd0 share the same slot
	 * 0xd: sd0
	 * 0xe: sd0 boot partition
	 */
	boot_media &= 0xf;
	nand_boot = (boot_media != 0xd && boot_media != 0xe);
#elif defined(CONFIG_ARCH_ATLASVI) || defined(CONFIG_ARCH_ATLAS7)
	/*
	 * nand and sd0 share the same slot
	 * 0x3: sd0
	 * 0x7: sd0 boot partition
	 */
	boot_media &= 0x7;
	nand_boot = (boot_media != 0x3 && boot_media != 0x7);
#endif
	if (nand_boot) {
#ifdef CONFIG_ARCH_ATLAS7
		sirfsoc_ioc_set_nand();
#endif

#ifdef CONFIG_SIRF_NANDDISK
	nanddisk_init_result = nanddisk_init();
#endif
	}
#ifdef CONFIG_SIRF_SPI
	spi_init();
#endif
	sirf_dm9000_hw_init();
	/*
	 * clock for pwm is not enabled when resume from hibernation
	 * so always enable it.
	 */
	writel(readl(CLKC_CLK_EN1) | CLK_PWM_EN, CLKC_CLK_EN1);
	writel(readl(CLKC_CLK_EN0) | CLK_LCD_EN, CLKC_CLK_EN0);

#if defined(CONFIG_ARCH_ATLASVI) && defined(CONFIG_SIRF_LCD)
	video_hw_init();
#endif
#ifdef CONFIG_ARCH_ATLAS7
	sirfsoc_set_sw3_sel_low();
#endif
	return 0;
}

#ifdef CONFIG_BOARD_LATE_INIT
#if defined(CONFIG_ARCH_ATLAS7)
static int check_mmc_exist(int dev)
{
	int ret;
	struct mmc *mmc;
	sirfsoc_ioc_set_sd(1);
	mmc = find_mmc_device(dev);
	if (mmc == NULL)
		return -1;
	ret = mmc_init(mmc);
	if (ret != 0)
		sirfsoc_ioc_set_nand();

	return ret;
}
#elif defined(CONFIG_ARCH_ATLASVI)
static int check_mmc_exist(int dev)
{
	int ret;
	struct mmc *mmc;

	mmc = find_mmc_device(dev);
	if (mmc == NULL)
		return -1;

	ret = mmc_init(mmc);
	if (ret != 0)
		return -1;

	return 0;
}
#endif
#ifdef CONFIG_ARCH_ATLAS7
static bool is_recovery(void)
{
	u32 boot_status = 0;
	boot_status = sirf_read_boot_status(SIRFSOC_BOOT_STATUS);

	if (boot_status & 0x7C)
		return true;
	else
		return false;
}
static bool is_indepdent(void)
{
	if (readl(CPU_BOOT_CFG) & M3_BOOT_TYPE_INDEP)
		return true;
	else
		return false;
}
#endif
int board_late_init(void)
{
	unsigned int boot_media;
	unsigned int boot_mode;
	char mem_info[10];
	unsigned int sect_size;
	char scert_start[10], scert_size[10];
	block_dev_desc_t *bdev = NULL;
	bool has_sd2 = false;
	int n;

	sect_size = 512;
#if defined(CONFIG_ARCH_ATLAS7)
	board_set_rwm();
#ifdef CONFIG_NOCFW_ENABLE
	board_config_nocfw();
#endif
	if (is_resume_required())
		setenv("bootcmd2", "resume");
#endif
#if defined(CONFIG_ARCH_PRIMAII)
	boot_mode = readl(NAND_BASE + NAND_BOOTMODE) & 0x3f;
	boot_media = getmmcbootmode();
	if (boot_media != BM_OTHER) {
		setenv("bootinf", "mmc");
		setenv("bootdev", "0");
		bdev = get_dev("mmc", 0);
	} else {
		setenv("bootinf", "nand");
		setenv("bootdev", "0");
		bdev = get_dev("nand", 0);
	}
#endif
#if defined(CONFIG_ARCH_ATLASVI)
	boot_mode = readl(NAND_BASE + NAND_BOOTMODE)
		& ATLAS6_BOOTMODE_NAND_MASK;
	boot_media = readl(ATLAS6_REALBOOTMODE) & 0xf;
	if (boot_media == ATLAS6_BM_SERIAL)
		/* boot from launcher through uart, check board bootmode */
		boot_media =
		readl(NAND_BASE + NAND_BOOTMODE) & ATLAS6_BOOTMODE_NAND_MASK;
	else
		boot_media = boot_media & ATLAS6_BOOTMODE_NAND_MASK;
#endif
#if defined(CONFIG_ARCH_ATLAS7)
	boot_media = readl(ROM_RETURN_ADRS + 0x14) & 0xf;
	boot_mode = readl(ATLAS7_BOOTMODE_REG) & 0x7;

	if (boot_media == ATLAS7_BM_SERIAL)
		boot_media = boot_mode;

#endif

#if defined(CONFIG_ARCH_ATLASVI) || defined(CONFIG_ARCH_ATLAS7)
	switch (boot_media) {
	case 0x0:
	case 0x6:
		setenv("bootinf", "nand");
		setenv("bootdev", "0");
		bdev = get_dev("nand", 0);
		break;
	case 0x1:
		setenv("bootinf", "usb");
		setenv("bootdev", "1");
		bdev = get_dev("usb", 1);
		break;
	case ATLAS6_BOOTMODE_SD2:
		/*
		 * in case sd2 boot with sd0 has card plugged in,
		 * should pass bootargs to kernle with rootdev as mmcblk1.
		 */
		if (!nand_is_valid() && check_mmc_exist(1) == 0)
			setenv("rootdev", "1");
	case ATLAS6_BOOTMODE_SD0BOOT:
	case ATLAS6_BOOTMODE_SD0:
		setenv("bootinf", "mmc");
		setenv("bootdev", "0");
		bdev = get_dev("mmc", 0);
		break;
	case 0x4:
		setenv("bootinf", "efuse");
		break;
	case 0x5:
		bdev = get_dev("spi", 2);
		if (nanddisk_init_result < 0) {
			setenv("bootinf", "mmc");
			setenv("bootdev", "0");
		} else {
			setenv("bootinf", "nand");
			setenv("bootdev", "0");
		}
		break;
	}
#endif
	if (bdev && bdev->blksz)
		sect_size = bdev->blksz;
	n = snprintf(scert_start, sizeof(scert_start), "0x%x"
			, CONFIG_SCERT_START / sect_size);
	if (n >= sizeof(scert_start))
		panic("!!CONFIG_SCERT_START is too big!!");
	n = snprintf(scert_size, sizeof(scert_size), "0x%x"
			, CONFIG_SCERT_SIZE / sect_size);
	if (n >= sizeof(scert_size))
		panic("!!CONFIG_SCERT_SIZE is too big!!");
	setenv("scert_start", scert_start);
	setenv("scert_size", scert_size);
	n = snprintf(mem_info, sizeof(mem_info), "%dM", SDRAM_SIZE);
	if (n >= sizeof(meminfo))
		panic("!!SDRAM_SIZE is too big!!");
	setenv("meminfo", mem_info);

#if defined(CONFIG_SECURE_RECOVERY)
	printf("SRCV:boot_mode=%x, boot_media=%x\n", boot_mode, boot_media);
	printf("SRCV:SIRFSOC_BOOT_STATUS=%x!\n"
			, sirf_read_boot_status(SIRFSOC_BOOT_STATUS));
	int devn = 0;
	int rcvflag = 0;
	u32 boot_status = 0;
#if defined(CONFIG_ARCH_PRIMAII)
	rcvflag = get_recovery_flag(bdev, devn);
	if ((rcvflag & 0xff) == RFLAG_START) {
		boot_status = sirf_read_boot_status(SIRFSOC_BOOT_STATUS);
		/*
		 * WORST: recovery.img-v1 and recovery.img-v2 both boot
		 * failed. Try recovery from production UDISK if uboot is
		 * OK.
		 */
		if (boot_status == RECOVERY_SECURE_FAIL) {
			printf("\n\nSRCV:recovery from UDISK!\n\n");
			setenv("bootcmd2", "run devboot_usb");
			return 0;
		}

		printf("SRCV:pre-recovery broke, try secure recovery!\n");
		sirf_write_boot_status(
				sirf_read_boot_status(SIRFSOC_BOOT_STATUS) |
				RECOVERY_MODE | RECOVERY_SECURE,
				SIRFSOC_BOOT_STATUS);
		printf("SRCV:SIRFSOC_BOOT_STATUS=%x!\n"
				, sirf_read_boot_status(SIRFSOC_BOOT_STATUS));
	}
#endif
#if defined(CONFIG_ARCH_ATLASVI) || defined(CONFIG_ARCH_ATLAS7)
	/*
	 * Boot from SD2 but the recovery target maybe NAND or SD0.
	 * Get the recovery target media
	 */
	if (boot_media == ATLAS6_BOOTMODE_SD2) {
		printf("SRCV:boot(uboot) from SD2\n");
		if (boot_mode == 0x0 || boot_mode == 0x6) {
			bdev = get_dev("nand", 0);
			devn = 0;
		}
		if (boot_mode == ATLAS6_BOOTMODE_SD0) {
			bdev = get_dev("mmc", 1);
			devn = 1;
		}
	}

	/*
	 * Boot from SPI NOR but recovery target maybe NAND or SD0.
	 * Get the recovery target media
	 */
	if (boot_media == ATLAS7_BM_DRAM) {
		printf("SRCV:boot(uboot) from SPI NOR\n");
		if (boot_mode == 0x0 || boot_mode == 0x6) {
			setenv("bootinf", "nand");
			setenv("bootdev", "0");
			bdev = get_dev("nand", 0);
			devn = 0;
			printf("DRAM_BOOT:recovery nand as devn=%d\n", devn);
		}
		if (boot_mode == ATLAS6_BOOTMODE_SD0) {
			setenv("bootinf", "mmc");
			setenv("bootdev", "0");
			bdev = get_dev("mmc", 0);
			devn = 0;
			printf("DRAM_BOOT:recovery SD0 as devn=%d\n", devn);
			if (get_dev("mmc", 1))
				has_sd2 = true;
		}
	}

	rcvflag = get_recovery_flag(bdev, devn);
	if ((rcvflag & 0xff) == RFLAG_START) {
		boot_status = sirf_read_boot_status(SIRFSOC_BOOT_STATUS);
		/*
		 * WORST: recovery.img-v1 and recovery.img-v2 both boot
		 * failed. Try recovery from production UDISK if uboot is
		 * OK. Otherwise ROMCODE will try boot uboot from SD2 and
		 * we can try recovery from SD2.
		 */
		if (boot_status == RECOVERY_SECURE_FAIL) {
			if (boot_mode == boot_media) {
				printf("\n\nSRCV:recovery from UDISK!\n\n");
				setenv("bootcmd2", "run devboot_usb");
				return 0;
			} else {
				printf("\n\nSRCV:bad recovery SD2 card!\n");
				printf("SRCV:try good recovery SD2 card!\n");
				sirfsoc_board_shutdown();
			}
		}

		printf("SRCV:pre-recovery broke, try secure recovery!\n");
		sirf_write_boot_status(
				sirf_read_boot_status(SIRFSOC_BOOT_STATUS) |
				RECOVERY_MODE | RECOVERY_SECURE,
				SIRFSOC_BOOT_STATUS);
		printf("SRCV:SIRFSOC_BOOT_STATUS=%x!\n"
				, sirf_read_boot_status(SIRFSOC_BOOT_STATUS));
	}
#endif
#endif

#ifdef CONFIG_KERNEL_BACKUP
#ifdef CONFIG_ARCH_ATLAS7
	if (is_resume_required())
		return 0;
#endif
	sirfsoc_load_boot_cfg();

#ifdef CONFIG_ARCH_ATLAS7
	if (boot_media == ATLAS7_BM_DRAM && (rcvflag & 0xff) != RFLAG_FINISH) {
		sirf_write_boot_status(
				sirf_read_boot_status(
				SIRFSOC_BOOT_STATUS) |
				MP_MODE_BOOT,
				SIRFSOC_BOOT_STATUS);
		if (has_sd2) {
			printf("DRAM_BOOT MP from SD2 to SD0\n");
			setenv("bootinf", "mmc");
			setenv("bootdev", "1");
			setenv("bootpart", "1");
			setenv("bootcmd2", "run devboot_mp");
		} else {
			printf("DRAM_BOOT MP from UDISK to SD0\n");
			setenv("bootinf", "usb");
			setenv("bootdev", "0");
			setenv("bootpart", "1");
			setenv("imgfile", "/mpImage");
			setenv("fdtfile", "/dtb");
			setenv("bootcmd", "run loadcsrvisor_usb");
			setenv("bootcmd2", "run devboot_usb");
		}
	} else {
#ifdef CONFIG_NOCFW_ENABLE
		/* enable qspi for A7 at dependment mode */
		if (!is_indepdent())
			board_config_ext_nocfw();
#endif
		sirfsoc_check_boot_status();
	}
	if (is_indepdent() && is_recovery()) {
		/* Hold M3 before recovery */
		sirfsoc_board_ipc2m3(A7DA_M3_HOLD);
		printf("SRCV: Hold M3 before recovery independent mode\n");
#define M3_IN_HOLD 0x11223344
		/* handshake with CM3 to make sure M3 in hold mode */
		while (readl(A7DA_IPC_CONTENT) != M3_IN_HOLD)
			;
#ifdef CONFIG_NOCFW_ENABLE
		/* enable qspi for A7*/
		board_config_ext_nocfw();
#endif
	}
#else
	sirfsoc_check_boot_status();
#endif

	sirfsoc_apply_boot_config();
#ifdef CONFIG_ARCH_ATLAS7
	sirfsoc_apply_xtal_config();
	hw_watchdog_init();
#endif
#endif

	return 0;
}
#endif

#ifndef CONFIG_ARCH_ATLAS7
void reset_cpu(unsigned long ignored)
{
#ifdef CONFIG_ARCH_MARCO
	writel(RESET_SR_SYS_RST, RESET_SR0_SET);
#else
	writel(RESET_SR_SYS_RST, RESET_SR0);
#endif
	while(1);
}
#endif

#ifdef CONFIG_ARCH_ATLAS7
static void sirfsoc_ioc_set_sd(int force_sd0)
{
	u32 val;
	u32 boot_media;

	val = REG_3_SD_CLK_2_MASK | REG_3_SD_CMD_2_MASK;
	val |= REG_3_SD_DAT_2_0_MASK | REG_3_SD_DAT_2_1_MASK;
	val |=  REG_3_SD_DAT_2_2_MASK | REG_3_SD_DAT_2_3_MASK;
	writel(val, SW_TOP_FUNC_SEL_REG_CLR(3));

	val = (1 << REG_3_SD_CLK_2_SHIFT) | (1 << REG_3_SD_CMD_2_SHIFT);
	val |= (1 << REG_3_SD_DAT_2_0_SHIFT) | (1 << REG_3_SD_DAT_2_1_SHIFT);
	val |= (1 << REG_3_SD_DAT_2_2_SHIFT) | (1 << REG_3_SD_DAT_2_3_SHIFT);
	writel(val, SW_TOP_FUNC_SEL_REG_SET(3));

	val = REG_16_SD_CD_B_2_MASK;
	writel(val, SW_TOP_FUNC_SEL_REG_CLR(16));

	val = REG_24_SD_WP_B_2_MASK;
	writel(val, SW_TOP_FUNC_SEL_REG_CLR(24));

	/* make the sd2 clock drive strength lower */
	val = SD_CLK_2_DRIVESTRENGTH_MASK;
	writel(val, SW_TOP_FUNC_SEL_REG_CLR(82));

	val = 0x7;
	writel(val, SW_TOP_FUNC_SEL_REG_SET(82));

	/* disable sd2 internal pull up */
	val = SD_2_PULL_MASK;
	writel(val, SW_TOP_FUNC_SEL_REG_CLR(34));

	val = 0xAAA;
	writel(val, SW_TOP_FUNC_SEL_REG_SET(34));

	val = (2 << REG_16_SD_CD_B_2_SHIFT);
	writel(val, SW_TOP_FUNC_SEL_REG_SET(16));

	val = (7 << REG_24_SD_WP_B_2_SHIFT);
	writel(val, SW_TOP_FUNC_SEL_REG_SET(24));

	val = SW_RTC_IN_DISABLE_1_REG_CLR_SD2__SD_CD_B_2;
	writel(val, SW_RTC_IN_DISABLE_1_REG_CLR);

	boot_media = readl(ATLAS7_BOOTMODE_REG) & 7;

	if (boot_media == 3 || boot_media == 7 || force_sd0) {
		writel(0x77777777, SW_TOP_FUNC_SEL_REG_CLR(4));
		writel(0x22222222, SW_TOP_FUNC_SEL_REG_SET(4));
		writel(0x00000770, SW_TOP_FUNC_SEL_REG_CLR(5));
		writel(0x00000220, SW_TOP_FUNC_SEL_REG_SET(5));
	}
}

static void sirfsoc_ioc_set_nand(void)
{
	writel(0x77777777, SW_TOP_FUNC_SEL_REG_CLR(4));
	writel(0x11111111, SW_TOP_FUNC_SEL_REG_SET(4));
	writel(0x77777777, SW_TOP_FUNC_SEL_REG_CLR(5));
	writel(0x11111111, SW_TOP_FUNC_SEL_REG_SET(5));

	writel(0x7 << 20, SW_TOP_FUNC_SEL_REG_CLR(16));
	setbits_le32(SW_TOP_FUNC_SEL_REG_SET(16), 0x4 << 20);
}

#endif

static void board_sdhci_init(int sdx_boot, int quirks, int index)
{
	int mmc0_quirks = quirks | SDHCI_QUIRK_USE_WIDE8;
	int boot_media;
	if (sdx_boot == BM_SD1) {
		sirf_sdhci_init
		(SD_REG_BASE(index), CONFIG_SIRF_MMC_MAX_CLK, 400000, quirks);
		sirf_sdhci_init
		(SD_REG_BASE(0), CONFIG_SIRF_MMC_MAX_CLK, 400000, mmc0_quirks);
	} else if (sdx_boot == BM_OTHER) {
#if defined(CONFIG_ARCH_ATLAS7)
		boot_media = readl(ATLAS7_BOOTMODE_REG) & 0x7;
		if ((boot_media == 0x3) || (boot_media == 0x7)) {
			sirf_sdhci_init
			(SD_REG_BASE(0), CONFIG_SIRF_MMC_MAX_CLK,
			 400000, mmc0_quirks);
			sirf_sdhci_init
			(SD_REG_BASE(2), CONFIG_SIRF_MMC_MAX_CLK,
			 400000, quirks);
		}
		else
			sirf_sdhci_init
			(SD_REG_BASE(index), CONFIG_SIRF_MMC_MAX_CLK,
			 400000, quirks);
#else
		sirf_sdhci_init
		(SD_REG_BASE(index), CONFIG_SIRF_MMC_MAX_CLK, 400000, quirks);
#endif
	} else {
		sirf_sdhci_init
		(SD_REG_BASE(0), CONFIG_SIRF_MMC_MAX_CLK, 400000, mmc0_quirks);
		sirf_sdhci_init
		(SD_REG_BASE(index), CONFIG_SIRF_MMC_MAX_CLK, 400000, quirks);
	}
}

int board_mmc_init(bd_t *bis)
{
	int quirks, sdx_boot, sd1_index;
	bool nanddev = false;
	bool sd1_init;
	block_dev_desc_t *nand;
#ifdef CONFIG_ARCH_MARCO
	writel(0x00000fff, PADCTRL_PAD_CD_0_SET);
#endif

	sdx_boot = getmmcbootmode();
	/*
	 * nand and sd0 share the same slot, so only enable sd0 pinumx
	 * when no nand is embedded.
	 */
	nand = get_dev("nand", 0);
	if (nand && nand->blksz)
		nanddev = true;

	sd1_init = (sdx_boot == BM_SD1 || sdx_boot == BM_OTHER);
	/*
	 * in spi1 boot mode if can't init nand flash, think sd card
	 * insert in slot 0, otherwise nand is inserted.
	 */
	if (sdx_boot == BM_SPI && nanddisk_init_result < 0)
		nanddev = 0;
	if (!nanddev || (nanddev && sd1_init)) {
		/* take one condition that nand is in dimm
		and boot from sdx(1/2) */
		quirks = SDHCI_QUIRK_REG32_RW |
			SDHCI_QUIRK_BROKEN_VOLTAGE |
			SDHCI_QUIRK_WAIT_SEND_CMD |
			SDHCI_QUIRK_AUTO_STOP_CMD12;

#if defined(CONFIG_ARCH_PRIMAII)
		/* enable sd0 */
		if (!nanddev) {
			clrbits_le32(RSC_PIN_MUX, 1 << 5);
			setbits_le32(CLKC_CLK_EN1, CLK_SDIO01_EN);
		}
		/* enable sd1 */
		clrbits_le32(GPIO_PAD_EN(0), (1 << 27) | (1 << 28) | (1 << 29));
		sd1_index = CONFIG_SIRF_MMC1_IDX;
#elif defined(CONFIG_ARCH_ATLASVI)
		/* enable sd0 */
		if (!nanddev) {
			clrsetbits_le32(RSC_PIN_MUX, 1 << 5, 1 << 19);
			clrbits_le32(GPIO_PAD_EN(3), 1 << 1);
			setbits_le32(CLKC_CLK_EN1, CLK_SDIO01_EN);
		}
		/* enable sd2 */
		clrbits_le32(RSC_PIN_MUX, 1 << 11);
		clrbits_le32(GPIO_PAD_EN(0), (1 << 27) | (1 << 28) | (1 << 29));
		setbits_le32(CLKC_CLK_EN1, CLK_SDIO23_EN);
		sd1_index = CONFIG_SIRF_MMC1_IDX;
#elif defined(CONFIG_ARCH_ATLAS7)
		debug("%s: setting ioc for sd ...", __func__);
		sirfsoc_ioc_set_sd(0);
		debug("done\n");
		debug("%s: add sd card...", __func__);
		sd1_index = CONFIG_SIRF_MMC2_IDX;
#endif
		board_sdhci_init(sdx_boot, quirks, sd1_index);
	}
	return 0;
}

void get_board_serial(struct tag_serialnr *serialnr)
{
	serialnr->low = readl(EFUSE0_OUT0);
	serialnr->high= readl(EFUSE0_OUT1);
}

bool i2c_dev_reg_bit_set(int port_index, u32 dev_addr,
	u32 reg_addr, u8 bit_map);
void sirf_dm9000_hw_init(void)
{
#ifdef CONFIG_SIRFSOC_FPGA
	writel(0x01 << 2, GPIO_PAD_EN_SET(0));
	writel(((~0x7F) & 0xFFFFFFFF) |
		readl(GPIO_BASE + GPIO_PAD_EN_SET(3)),
		GPIO_BASE + GPIO_PAD_EN_SET(3));
#if 0
	/*
	 * Select DM9000 through CPLD
	 */
	if (false == i2c_dev_reg_bit_clear(0, 0x5a, 0x0f, 0x03))
	{
		printf("%s:i2c_dev_reg_bit_clear\n", __func__);
		return;
	}
	if (false == i2c_dev_reg_bit_set(0, 0x5a, 0x0f, 0x02))
	{
		printf("%s:i2c_dev_reg_bit_set\n", __func__);
		return;
	}
	/*
	 * Reset DM9000 through CPLD
	 */
	if (false == i2c_dev_reg_bit_clear(0, 0x5a, 0x08, 1 << 7))
	{
		printf("%s:i2c_dev_reg_bit_clear\n", __func__);
		return;
	}
	udelay(100);
	if (false == i2c_dev_reg_bit_set(0, 0x5a, 0x08, 1 << 7))
	{
		printf("%s:i2c_dev_reg_bit_set\n", __func__);
		return;
	}
	udelay(100);
#endif

#ifdef CONFIG_ARCH_MARCO
	writel(readl(CLKC_CLK_EN1) | CLK_ROM_EN, CLKC_CLK_EN1);
#else
	writel(readl(CLKC_CLK_EN1) | CLK_ROM_EN, CLKC_CLK_EN1);
#endif
	writel(0x01, RSC_PIN_MUX_SET);
	writel(0x80000000, GPIO_PAD_EN_CLR(2));
	writel(0x7ff, GPIO_PAD_EN_CLR(3));
	udelay(100);

	/*
	 * Set ROM config register
	 */
	writel((ROM_SET_WGAP(0x10) | ROM_SET_TACC(0xc) | ROM_SET_TCES(0x1)
		| ROM_SET_TNACC(0xc) | ROM_SET_TDF(0x5) | ROM_DWORD_ACCESS_DIS
		| ROM_BURST_READ_DIS | ROM_BURST_WRITE_DIS | ROM_VARI_ACCESS_DIS
		| ROM_BE_ACTIVE1 | ROM_WRITE_EN | ROM_BUS_8), ROM_CFG_CS1);
	udelay(500000);
#endif
}
/*The HD screen use capacitive screen*/
static bool board_is_cap_touch(void)
{
#ifndef CONFIG_ARCH_ATLAS7
	/*Set the mcu to enable the touch voltage*/
	i2c_dev_reg_bit_set(0, 0x80, 8, 1);
	/*if the i2c dev can set, the touch is capacitive screen*/
	return i2c_dev_reg_bit_set(1, 0xB8, 0x00, 1) ||
			i2c_dev_reg_bit_set(1, 0x70, 0x00, 1);
#else
	return i2c_dev_reg_bit_set(0, 0xB8, 0x00, 1) ||
			i2c_dev_reg_bit_set(0, 0x70, 0x00, 1);
#endif
}

#ifndef CONFIG_ARCH_ATLAS7
static bool ft_board_fixup_lcd(void *blob)
{
	int root_node, node, err;
	int panel_node;
	uint32_t phandle;

	/*if the touch is capacitive, modify the dts to set the lcd type*/
	if (board_is_cap_touch()) {
		if (fdt_check_header(blob))
			return false;

		panel_node = fdt_path_offset(blob, "/display@0/panels/panel@1");
		if (panel_node < 0)
			return false;

		phandle = fdt_get_phandle(blob, panel_node);
		if (phandle <= 0)
			return false;

		node = fdt_path_offset(blob, "/axi/disp-iobg/lcd@90010000");
		if (node < 0)
			return false;

		err = fdt_setprop_cell(blob, node, "default-panel", phandle);
		if (err < 0) {
			printf("setprop error\n");
			return false;
		}
		root_node = fdt_path_offset(blob, "/");
		if (root_node >= 0) {
#if defined(CONFIG_ARCH_PRIMAII)
			err = fdt_appendprop_string(blob, root_node,
					"compatible", "sirf,prima2-wsvga");
			if (err < 0)
				printf("set the wsvga compatible failed\n");
#elif defined(CONFIG_ARCH_ATLASVI)
			err = fdt_appendprop_string(blob, root_node,
					"compatible", "sirf,atlas6-wsvga");
			if (err < 0)
				printf("set the wsvga compatible failed\n");
#endif
		}
	}

	return true;
}
#else
static bool ft_board_fixup_lcd(void *blob)
{
	int err;
	int panel_node;
	int rgb_node, lvds_node;

	/*
	 * if the touch is not capacitive,
	 * modify the dts to set the rgb lcd type
	 */
	if (!board_is_cap_touch()) {
		if (fdt_check_header(blob))
			return false;

		rgb_node = fdt_node_offset_by_compatible(blob, -1, "rgb-panel");
		if (rgb_node < 0)
			return false;

		err = fdt_setprop_string(blob, rgb_node,
				"status", "okay");
		if (err < 0)
			printf("enable rgb panel failed\n");

		lvds_node =
			fdt_node_offset_by_compatible(blob,
							-1, "lvds-panel");
		if (lvds_node < 0)
			return false;

		err = fdt_setprop_string(blob, lvds_node,
				"status", "disabled");
		if (err < 0)
			printf("enable lvds panel failed\n");

		panel_node = fdt_path_offset(blob, "/aliases");
		if (panel_node < 0)
			return false;

		err = fdt_setprop_cell(blob, panel_node, "display0", rgb_node);
		if (err < 0) {
			printf("setprop error\n");
			return false;
		}
	}

	return true;
}
#endif

void ft_board_sd_setup(void *blob)
{
	uint32_t boot_media;
	int err, node;

	if (fdt_check_header(blob)) {
		printf("check the fdt header error\n");
		return;
	}
#if defined(CONFIG_ARCH_ATLAS7)
	boot_media = boot_media; /* FIXME: fix when EVB ready */
#endif

#if defined(CONFIG_ARCH_PRIMAII)
	boot_media = readl(NAND_BASE + NAND_BOOTMODE) &
		PRIMA2_BOOTMODE_NAND_MASK;
	if (PRIMA2_BOOTMODE_SD1 == (boot_media >> PRIMA2_BOOTMODE_SD1_SHIFT)) {
		node = fdt_path_offset(blob,
			"/axi/peri-iobg/pci-iobg/sdhci@56100000");
		if (node < 0)
			return;
	} else
#elif defined(CONFIG_ARCH_ATLASVI)
	boot_media = readl(NAND_BASE + NAND_BOOTMODE) &
		ATLAS6_BOOTMODE_NAND_MASK;
	if (boot_media == ATLAS6_BOOTMODE_SD2) {
		node = fdt_path_offset(blob,
			"/axi/peri-iobg/pci-iobg/sdhci@56200000");
		if (node < 0)
			return;
	} else
#endif
		return;

	err = fdt_setprop_cell(blob, node, "non-removable", 1);
	if (err < 0) {
		printf("sd fix fdt error, err %d\n", err);
		return;
	}
}

void fdt_fixup_nand(void *blob)
{
	int offset, ret;
#ifndef CONFIG_ARCH_ATLAS7
	char *compatible = "sirf,prima2-nand";
#else
	char *compatible = "sirf,atlas7-nand";
#endif
	offset = fdt_node_offset_by_compatible(blob, -1, compatible);
	if (offset < 0)
		return;

	if (!nand_is_valid()) {
		ret = fdt_setprop_string(blob, offset, "status", "disabled");
		if (ret)
			printf("unable to set status for %s\n", compatible);
	}
}

#ifdef CONFIG_ARCH_ATLAS7
static void fdt_fixup_qspi(void *blob)
{
	int offset, ret;
	char *compatible = "sirf,atlas7-qspi-nor";

	/*disable qspi*/
	if (!(!is_recovery() && is_indepdent()))
		return;

	offset = fdt_node_offset_by_compatible(blob, -1, compatible);
	if (offset < 0)
		return;

	ret = fdt_setprop_string(blob, offset, "status", "disabled");
	if (ret)
		printf("unable to set status for %s\n", compatible);
}

static void fdt_fixup_3d(void *blob)
{
	int offset, ret;
	char *compatible = "powervr,sgx531";

	offset = fdt_node_offset_by_compatible(blob, -1, compatible);
	if (offset < 0)
		return;

	ret = fdt_setprop_string(blob, offset, "status", "disabled");
	if (ret)
		printf("unable to set status for %s\n", compatible);

	offset = fdt_path_offset(blob, "/noc/gpum/gpum/qos/sgx");
	if (offset > 0)
		fdt_del_node(blob, offset);

	offset = fdt_path_offset(blob, "/noc/gpum/gpum/bw_probe/sgx");
	if (offset > 0)
		fdt_del_node(blob, offset);

	/*block access by noc register firewall*/
	writel(0x00000020, 0x13001060);
}

static void fdt_fixup_gnss(void *blob)
{
	int offset, ret;
	char *compatible = "sirf,atlas7-gps";

	offset = fdt_node_offset_by_compatible(blob, -1, compatible);
	if (offset < 0)
		return;

	ret = fdt_setprop_string(blob, offset, "status", "disabled");
	if (ret)
		printf("unable to set status for %s\n", compatible);
}

static void fdt_fixup_bt(void *blob)
{
	int offset, ret;
	char *compatible = "sirf,a7ca_bt";

	offset = fdt_node_offset_by_compatible(blob, -1, compatible);
	if (offset < 0)
		return;

	ret = fdt_setprop_string(blob, offset, "status", "disabled");
	if (ret)
		printf("unable to set status for %s\n", compatible);

	/*block access by noc register firewall*/
	writel(0x00000001, 0x10E6001C);
}

static void fdt_fixup_eth(void *blob)
{
	int offset, ret;
	char *compatible = "snps, dwc-eth-qos";

	offset = fdt_node_offset_by_compatible(blob, -1, compatible);
	if (offset < 0)
		return;

	ret = fdt_setprop_string(blob, offset, "status", "disabled");
	if (ret)
		printf("unable to set status for %s\n", compatible);

	offset = fdt_path_offset(blob, "/noc/gnssm/gnssm/qos/eth_avb");
	if (offset > 0)
		fdt_del_node(blob, offset);

	offset = fdt_path_offset(blob, "/noc/gnssm/gnssm/bw_probe/eth0");
	if (offset > 0)
		fdt_del_node(blob, offset);

	/*block access by noc register firewall*/
	writel(0x00000004, 0x18101060);
}

static void fdt_fixup_jpeg(void *blob)
{
	int offset, ret;
	char *compatible = "sirf,atlas7-jpeg";

	offset = fdt_node_offset_by_compatible(blob, -1, compatible);
	if (offset < 0)
		return;

	ret = fdt_setprop_string(blob, offset, "status", "disabled");
	if (ret)
		printf("unable to set status for %s\n", compatible);
	offset = fdt_path_offset(blob, "/reserved-memory/jpeg");
	if (offset > 0)
		fdt_del_node(blob, offset);
	offset = fdt_path_offset(blob, "/noc/mediam/mediam/qos/jpeg");
	if (offset > 0)
		fdt_del_node(blob, offset);

	/*block access by noc register firewall*/
	writel(0x00000004, 0x170a1060);
}

static void fdt_fixup_multimedia(void *blob)
{
	int offset, ret;
	char *compatible = "sirf,atlas7-video-codec";

	offset = fdt_node_offset_by_compatible(blob, -1, compatible);
	if (offset < 0)
		return;

	ret = fdt_setprop_string(blob, offset, "status", "disabled");
	if (ret)
		printf("unable to set status for %s\n", compatible);
	offset = fdt_path_offset(blob, "/reserved-memory/multimedia");
	if (offset > 0)
		fdt_del_node(blob, offset);
	offset = fdt_path_offset(blob, "/noc/mediam/mediam/qos/vxd");
	if (offset > 0)
		fdt_del_node(blob, offset);
	offset = fdt_path_offset(blob, "/noc/mediam/mediam/bw_probe/vxd");
	if (offset > 0)
		fdt_del_node(blob, offset);

	/*block access by noc register firewall*/
	writel(0x00008000, 0x170a1060);

}
#define OTP_FEATURES_OFS	0x21
struct sku_config {
	unsigned long mask;
	void (*fdt_fixup)(void *blob);
};
#define SKU_3D		0x1
#define SKU_GNSS	(0x1 << 3)
#define SKU_BT		(0x1 << 5)
#define SKU_ETH		(0x1 << 9)
#define SKU_JPEG	((0x1 << 10) | (0x1 << 11))
#define SKU_MULTIMEDIA	(0x3FF << 16)

#define SKU_VIP		(0x1 << 1)
#define SKU_CPU1G	(0x1 << 2)
#define SKU_LVDS	(0x1 << 3)

struct sku_config otpc[] = {
	{ SKU_3D, fdt_fixup_3d },
	{ SKU_GNSS, fdt_fixup_gnss },
	{ SKU_BT, fdt_fixup_bt },
	{ SKU_ETH, fdt_fixup_eth },
	{ SKU_JPEG, fdt_fixup_jpeg },
	{ SKU_MULTIMEDIA, fdt_fixup_multimedia },
};

void fdt_fixup_cpu_freq(void *blob)
{
	u32 *prop;
	int offset;
	fdt32_t tmp;
	int len, err;

	offset = fdt_path_offset(blob, "/cpus/cpu");
	if (offset < 0) {
		printf("find /cpu error\n");
		return;
	}
	prop = (u32 *)fdt_getprop(blob, offset, "operating-points", &len);
	if (!prop) {
		printf("getprop operating-points fail\n");
		return;
	}
	tmp = cpu_to_fdt32(1014000);
	memcpy(prop, &tmp, sizeof(u32));
	err = fdt_setprop_inplace(blob, offset, "operating-points", prop, len);
	if (err < 0)
		printf("set operating-points err %d\n", err);
}

void fdt_fixup_vip(void *blob)
{
	int offset = -1, ret = 0;
	int i;
	char *compatible[] = { "sirf,atlas7-cvd-vip", "sirf,atlas7-vip-ts"};

	for (i = 0; i < 2; i++) {
		do {
			offset = fdt_node_offset_by_compatible(blob,
				offset, compatible[i]);
			if (offset < 0)
				break;

			ret = fdt_setprop_string(blob, offset,
						"status", "disabled");
			if (ret)
				printf("unable to set status for %s\n",
				       compatible[i]);
		} while (offset != -1);
	}
}

void fdt_fixup_lvds(void *blob)
{
	int offset, ret;
	int i;
	char *compatible[] = {"sirf,atlas7-lvdsc", "lvds-panel"};

	for (i = 0; i < 2; i++) {
		offset = fdt_node_offset_by_compatible(blob, -1, compatible[i]);
		if (offset < 0)
			continue;

		ret = fdt_setprop_string(blob, offset, "status", "disabled");
		if (ret)
			printf("unable to set status for %s\n", compatible[i]);
	}
}

struct sku_config skus[] = {
	{ SKU_VIP, fdt_fixup_vip },
	{ SKU_LVDS, fdt_fixup_lvds },
	{ SKU_CPU1G, fdt_fixup_cpu_freq },
};

#ifndef CONFIG_SVM_SUPPORT
static void fdt_fixup_svm(void *blob)
{
	int offset;
	int err = 0;
	char *compatible = "sirf,sirf-sysctl";
	u32 svm;

	offset = fdt_node_offset_by_compatible(blob, -1, compatible);
	if (offset < 0)
		return;

	atlas7_otp_get_svm(&svm);
	err =  fdt_appendprop_cell(blob, offset, "svm", svm);
	if (err < 0) {
		printf("svm setprop error\n");
		return;
	}
}
#endif
#endif
void ft_board_setup(void *blob, bd_t *bd)
{
	int root_node;
	int hwver;
	int err = 0;
	int i;
	unsigned long config;
	unsigned long chip_info;

	root_node = 0;
	ft_board_sd_setup(blob);
	fdt_fixup_nand(blob);
#ifdef CONFIG_ARCH_ATLAS7
#ifdef CONFIG_NOCFW_ENABLE
	fdt_fixup_qspi(blob);
#endif
#ifndef CONFIG_SVM_SUPPORT
	fdt_fixup_svm(blob);
#endif
#endif
	ft_board_fixup_adc(blob);
	if (!ft_board_fixup_lcd(blob))
		printf("To fix the lcd fdt wrong\n");
#ifdef CONFIG_ARCH_ATLAS7

	hwver = readl(INTC_BASE + INTC_DEVID_OFS) & 0xff;
	if (hwver == INTC_DEV_ID_A0)
		err = fdt_appendprop_string(blob, root_node,
				"compatible", "sirf,atlas7-a0");
	else if (hwver == INTC_DEV_ID_A1)
		err = fdt_appendprop_string(blob, root_node,
						"compatible", "sirf,atlas7-a1");
	else if (hwver == INTC_DEV_ID_B0)
		err = fdt_appendprop_string(blob, root_node,
						"compatible", "sirf,atlas7-b0");
	else if (hwver == INTC_DEV_ID_B1)
		err = fdt_appendprop_string(blob, root_node,
						"compatible", "sirf,atlas7-b1");
	else if (hwver == INTC_DEV_ID_B2)
		err = fdt_appendprop_string(blob, root_node,
						"compatible", "sirf,atlas7-b2");
	else if (hwver == INTC_DEV_ID_B3)
		err = fdt_appendprop_string(blob, root_node,
						"compatible", "sirf,atlas7-b3");
	if (err < 0)
		printf("set hw ver compatible failed\n");

	err = read_OTP_word(OTP_FEATURES_SKU, &chip_info);
	if (err)
		return;
	printf("chip id: %x\n", (u32)(chip_info >> 24));

	for (i = 0; i < sizeof(skus) / sizeof(skus[0]); i++) {
		if ((skus[i].mask & chip_info) == skus[i].mask)
			skus[i].fdt_fixup(blob);
	}

	err = read_OTP_word(OTP_FEATURES_OFS, &config);
	if (err < 0) {
		printf("read OTP features failed\n");
		return;
	}
	for (i = 0; i < sizeof(otpc) / sizeof(otpc[0]); i++) {
		if ((otpc[i].mask & config) == otpc[i].mask)
			otpc[i].fdt_fixup(blob);
	}

#endif
}

#if defined (CONFIG_HAVE_OWN_RESET)
int do_reset(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	sirfsoc_board_reset();

	/*NOTREACHED*/
	return 0;
}
#endif

#if defined(CONFIG_USB_ETHER)
int board_eth_init(bd_t *bis)
{
	return usb_eth_initialize(bis);
}
#endif
