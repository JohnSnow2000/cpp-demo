/*
 * Copyright (c) 2015, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <command.h>

#include "dxotprw.h"

static int do_dxotp_dump(cmd_tbl_t *cmdtp, int flag,
			 int argc, char * const argv[])
{
	unsigned long offset, i, value;
	unsigned long count = 1;
	int ret;

	if (argc < 2)
		return CMD_RET_USAGE;

	offset = simple_strtoul(argv[1], NULL, 16);
	if (argc > 2)
		count = simple_strtoul(argv[2], NULL, 16);

	for (i = 0; i < count; i++) {
		ret = read_OTP_word(offset, &value);
		if (ret != 0) {
			printf("Error:%d occurred while reading offset: %x\n",
			       ret, (unsigned int)offset);
			return CMD_RET_FAILURE;
		}

		if (!(i % 4))
			printf("\noffset 0x%08x: ", (unsigned int)offset);

		printf("0x%08x ", (unsigned int)value);
		offset++;
	}

	printf("\n");

	return CMD_RET_SUCCESS;
}

static int do_dxotp_program(cmd_tbl_t *cmdtp, int flag,
			    int argc, char * const argv[])
{
	unsigned long offset, value;
	int ret;

	if (argc < 3)
		return CMD_RET_USAGE;

	offset = simple_strtoul(argv[1], NULL, 16);
	value = simple_strtoul(argv[2], NULL, 16);

	/* ask 'Y' for sure in case user mis-operation */
	printf("writing val %x into :%lu, confirm? 'Y':\n",
	       (unsigned int)value, offset);
	if (getc() != (int)'Y') {
		printf("aborted.\n");
		return CMD_RET_FAILURE;
	}

	ret = write_OTP_word(offset, value);
	if (ret != 0) {
		printf("Error:%d occurred while writing val:%x @offset:%x\n",
		       ret, (unsigned int)value, (unsigned int)offset);
		return CMD_RET_FAILURE;
	}

	printf("done.\n");

	return CMD_RET_SUCCESS;
}

U_BOOT_CMD(
	otpdump, 3, 0, do_dxotp_dump,
	"dump dx otp content",
	"offset(in word) [count](default 1)\n"
);

U_BOOT_CMD(
	otpprog, 3, 0, do_dxotp_program,
	"program dx otp content",
	"offset(in word) value(in word)\n"
);
