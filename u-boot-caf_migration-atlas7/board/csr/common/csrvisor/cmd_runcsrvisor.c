/*
 * Run csrvisor if digest valdiation passed
 *
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <asm/io.h>
#include <sha256.h>
#include <hash.h>
#include "cmd_runcsrvisor.h"

struct NOCFW_T {
	u32 regoff;
	u32 val;
};

struct NOCFW_T g_nocfw_ns_tbl[] = {
	{0x1082105c, 0x00000080},/*nand*/
	{0x1082108c, 0x00000080},
	{0x10821068, 0x00000001},/*sd2*/
	{0x10821098, 0x00000001},
	{0x10821068, 0x00010000},/*sd0*/
	{0x10821098, 0x00010000},
	{0x10821050, 0x03000000},/*usb0,usb1*/
	{0x10821080, 0x03000000},
};

static void config_nocfw_ns(void)
{
	struct NOCFW_T *entry;
	int i;

	for (i = 0; i < ARRAY_SIZE(g_nocfw_ns_tbl); i++) {
		entry = &g_nocfw_ns_tbl[i];
		writel(entry->val, entry->regoff);
	}
}

static int do_runcsrvisor(cmd_tbl_t *cmdtp, int flag, int argc,
		char * const argv[])
{
	unsigned long file_size;
	unsigned char calc_digest[SHA256_SUM_LEN];
	char command[32];
	int calc_digest_len;

	if ((unsigned)&csrvisor_digest_end -
	    (unsigned)&csrvisor_digest_start != SHA256_SUM_LEN) {
		printf("no valid saved digest found.\n");
		goto fail_exit;
	}

	if (argc < 2)
		goto fail_exit;

	file_size = simple_strtoul(argv[1], NULL, 16);
	if (file_size > CONFIG_CSRVISOR_MEM_SIZE) {
		printf("csrvisor file size exceed limit.\n");
		goto fail_exit;
	}

	/*configure bootmedia as non-secure master as kernel need*/
	config_nocfw_ns();

	calc_digest_len = sizeof(calc_digest);
	if (hash_block("sha256",
		       (void *)CONFIG_CSRVISOR_VADDR,
		       (unsigned int)file_size,
		       calc_digest,
		       &calc_digest_len)) {
		printf("error in calculate digest.\n");
		goto fail_exit;
	}

	if (memcmp(calc_digest, (void *)&csrvisor_digest_start,
		   SHA256_SUM_LEN)) {
		printf("digest not correct.\n");
		goto fail_exit;
	}

	/* ready to launch */
	if (snprintf(command, sizeof(command),
		     "go 0x%x", CONFIG_CSRVISOR_VADDR) < 0) {
		printf("error in formating command.\n");
		goto fail_exit;
	}

	run_command(command, 0);

	return CMD_RET_SUCCESS;

fail_exit:
	/* destroy loaded csrvisor if verify failed */
	memset((void *)CONFIG_CSRVISOR_VADDR, 0x0, CONFIG_CSRVISOR_MEM_SIZE);

	return CMD_RET_FAILURE;
}

U_BOOT_CMD(
	runcv, 2,	0,	do_runcsrvisor,
	"run csrvisor if digest validation passed",
	"file-size - csrvisor file size in partition.\n"
);
