/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <command.h>
#include <asm/arch/cpu.h>
#include <asm/processor.h>
#include <asm/io.h>
#ifdef CONFIG_ARCH_ATLAS7
int do_a7da_resume(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	u32 addr;

	cleanup_before_linux();
	addr = readl(PWRC_SCRATCH_PAD7_F);
	((void (*)(void))addr)();
	return 0;
}

bool is_resume_required(void)
{
	return  !!readl(PWRC_SCRATCH_PAD10_F);
}

U_BOOT_CMD(
	resume,	CONFIG_SYS_MAXARGS,	1,	do_a7da_resume,
	"resume",
	""
);
#endif
