/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <asm/io.h>
#include <asm/arch/board.h>
#include <asm/arch/regulator.h>
#include <asm/arch/regulator_pwm.h>
#include <asm/arch/pwm.h>
#include <stdarg.h>
#include <exports.h>
#include "dxotprw.h"

#define BIT(x)		(1 << (x))

#define VOL_LEVEL 16
#define OTP_SVM_ADD 0x4E

#define CPU_REG_PWM_ID 2
#define CORE_REG_PWM_ID 4

enum {
	SVM_CPU = 1,
	SVM_CORE,
};

struct svm_info {
	u32 svm;
	struct regulator *cpu_reg;
	struct regulator *core_reg;
};

static struct regulator cpur;
static struct regulator corer;
static struct svm_info infos;

unsigned long voltage[VOL_LEVEL] = {
	1265000, /* ignore index 0 */
	1265000,
	1237542,
	1210038,
	1182534,
	1155030,
	1127526,
	1100022,
	1072518,
	1045014,
	1017510,
	990006,
	962502,
	934998,
	907494,
	879990
};

static int atlas7_svm_core_idx(u32 svm_config)
{
	u32 svm_index;

#define	CORE_REDUNDANCY 0
#define	CORE_REDUNDANCY_1 8
#define	VCORE_LEVEL 4
#define	VCORE_LEVEL_1 12

	/*
	* 0: Use primary core SVM fields,
	* 1: Use secondary core SVM fields
	*/
	if (!(svm_config & BIT(CORE_REDUNDANCY)))
		svm_index = svm_config>>VCORE_LEVEL & 0xf;
	else if (svm_config & BIT(CORE_REDUNDANCY_1))
		svm_index = 0;
	else
		/*
		* Only valid if Core_redundancy = 1
		* 0: Use secondary core SVM fields
		* 1: Do not use secondary core SVM fields (disable
		* core SVM)
		*/
		svm_index = svm_config>>VCORE_LEVEL_1 & 0xf;

	return svm_index;
}


static int atlas7_svm_cpu_idx(u32 svm_config)
{
	u32 svm_index;

#define	CPU_REDUNDANCY 16
#define	CPU_REDUNDANCY_1 24
#define	VCPU_LEVEL 20
#define	VCPU_LEVEL_1 28

	/*
	*0: Use primary CPU SVM fields
	*1: Use secondary CPU SVM fields
	*/
	if (!(svm_config & BIT(CPU_REDUNDANCY)))
		svm_index = svm_config>>VCPU_LEVEL & 0xf;
	else if (svm_config & BIT(CPU_REDUNDANCY_1))
		svm_index = 0;
	else
		/*
		*Only valid if CPU_redundancy = 1
		*0: Use secondary CPU SVM fields
		*1: Do not use secondary CPU SVM fields (disable
		*CPU SVM)
		*/
		svm_index = svm_config>>VCPU_LEVEL_1 & 0xf;

	return svm_index;
}

static void atlas7_pm_svm(struct svm_info *info, int dev_type)
{
	struct regulator *reg;
	unsigned long volt = 0;
	int ret, index;

	if (dev_type == SVM_CORE) {
		reg = info->core_reg;
		index = atlas7_svm_core_idx(info->svm);
	} else if (dev_type == SVM_CPU) {
		reg = info->cpu_reg;
		index = atlas7_svm_cpu_idx(info->svm);
	} else {
		goto out;
	}

	/* vdd_core share same voltage table as vdd_cpu*/
	volt = voltage[index];
	/* if index is 0, No change, do nothing */
	if (!index)
		goto out;

	reg->volt = volt;
	ret = regulator_set_voltage(reg, volt);
	if (ret) {
		printf("regulator %s set voltage failed!\n", reg->name);
		goto out;
	}
	ret = regulator_enable(reg);
	if (ret) {
		printf("regulator %s enable failed!\n", reg->name);
		goto out;
	}

out:
	return;
}

void atlas7_otp_get_svm(u32 *svm)
{
	read_OTP_word(OTP_SVM_ADD, (ulong *)svm);
}


static struct pwm_device cpu_reg_pwm;
static struct pwm_regulator_data cpu_reg_pwm_data;
static struct pwm_device core_reg_pwm;
static struct pwm_regulator_data core_reg_pwm_data;

static int svm_reg_pwm_init(struct regulator *regulator)
{
	struct pwm_device *pwm;
	struct pwm_regulator_data *pwm_reg_data;

	if (regulator->id == SVM_CPU) {
		pwm = &cpu_reg_pwm;
		pwm_reg_data = &cpu_reg_pwm_data;
		pwm->pwm = CPU_REG_PWM_ID;
	} else if (regulator->id == SVM_CORE) {
		pwm = &core_reg_pwm;
		pwm_reg_data = &core_reg_pwm_data;
		pwm->pwm = CORE_REG_PWM_ID;
	} else
		return -1;

	pwm_reg_data->pwm = pwm;
	regulator->reg_data = pwm_reg_data;
	regulator->volt = 0;

	return 0;
}

static int atlas7_pm_svm_init(struct svm_info *info)
{
	struct regulator *cpu_reg = &cpur;
	struct regulator *core_reg = &corer;
	int ret = 0;

	cpu_reg->name = "CPU_REG";
	core_reg->name = "CORE_REG";
	cpu_reg->id = SVM_CPU;
	core_reg->id = SVM_CORE;

	ret = svm_reg_pwm_init(cpu_reg);
	if (ret)
		goto exit;
	ret = svm_reg_pwm_init(core_reg);
	if (ret)
		goto exit;

	reg_pwm_probe(cpu_reg);
	reg_pwm_probe(core_reg);
	/* enable PWM IO clock */
	sirf_pwm_io_clock_enable();
	/* config gpio2/4 to pwm function */
	sirf_gpio_mux_to_pwm();

	info->core_reg = core_reg;
	info->cpu_reg = cpu_reg;

	atlas7_otp_get_svm(&info->svm);

exit:
	return ret;
}

int sirfsoc_svm_config(void)
{
	int ret;
	struct svm_info *info = &infos;

	ret = atlas7_pm_svm_init(info);
	if (ret != 0)
		goto out;

	atlas7_pm_svm(info, SVM_CPU);
	atlas7_pm_svm(info, SVM_CORE);

	return 0;
out:
	return ret;
}
