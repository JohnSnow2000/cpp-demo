/*
 * OTP read/write from discretix interface
 *
 * Copyright (c) 2015, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <errno.h>
#include <asm/io.h>
#include "dxotprw.h"

/* macros */
#define DX_HOST_AIB_ADDR_REG_REG_OFFSET		0xAA4UL
#define DX_HOST_AIB_WDATA_REG_REG_OFFSET	0xAA8UL
#define DX_HOST_AIB_RDATA_REG_REG_OFFSET	0xAACUL
#define DX_AIB_FUSE_PROG_COMPLETED_REG_OFFSET	0xAB0UL
#define DX_AIB_FUSE_ACK_REG_OFFSET		0xAB4UL

#define OTP_MAX_OFFSET				0xff
#define OTP_MODULE_BASE_ADDR			0x18240000

#define SYS_WriteRegister(addr, val) writel(val, addr)
#define SYS_ReadRegister(addr, val) val = readl(addr)

#define DX_MNG_READ_WORD_VIA_AIB(nvm_addr, nvm_data)	\
	do { \
		unsigned int tval; \
		SYS_WriteRegister( \
			OTP_MODULE_BASE_ADDR + \
			DX_HOST_AIB_ADDR_REG_REG_OFFSET, nvm_addr); \
		do { \
			SYS_ReadRegister( \
				OTP_MODULE_BASE_ADDR + \
				DX_AIB_FUSE_ACK_REG_OFFSET, tval); \
		} while (!(tval & 0x1)); \
		SYS_ReadRegister( \
			OTP_MODULE_BASE_ADDR + \
			DX_HOST_AIB_RDATA_REG_REG_OFFSET, nvm_data);\
	} while (0)

/* Write a word via NVM */
#define DX_MNG_WRITE_WORD_VIA_AIB(nvm_addr, nvm_data) \
	do { \
		unsigned int tval; \
		SYS_WriteRegister( \
			OTP_MODULE_BASE_ADDR + \
			DX_HOST_AIB_WDATA_REG_REG_OFFSET, nvm_data); \
		SYS_WriteRegister( \
			OTP_MODULE_BASE_ADDR +  \
			DX_HOST_AIB_ADDR_REG_REG_OFFSET, nvm_addr); \
		do { \
			SYS_ReadRegister( \
				OTP_MODULE_BASE_ADDR + \
				DX_AIB_FUSE_ACK_REG_OFFSET, tval); \
		} while (!(tval & 0x1)); \
		do { \
			SYS_ReadRegister( \
				OTP_MODULE_BASE_ADDR + \
				DX_AIB_FUSE_PROG_COMPLETED_REG_OFFSET, \
				tval); \
		} while (!(tval & 0x1)); \
	} while (0)

int read_OTP_word(unsigned long offset, unsigned long *val)
{
	unsigned int old_val;

	if (offset > OTP_MAX_OFFSET)
		return -EINVAL;
	/* OTP read in specification describes in WORD values at WORD offset;
	* while the ADDR register needs to be set in 4 bytes aligned fomrat.
	* Expand the WORD offset into byte offset here before reading */
	offset *= sizeof(unsigned long);
	DX_MNG_READ_WORD_VIA_AIB((0x1 << 0x10) | offset, old_val);

	*val = old_val;

	return 0;
}

int write_OTP_word(unsigned long offset, unsigned long val)
{
	unsigned int old_val;

	if (offset > OTP_MAX_OFFSET)
		return -EINVAL;

	/* OTP write in specification describes in WORD values at WORD offset;
	* while the ADDR register needs to be set in 4 bytes aligned fomrat.
	* Expand the WORD offset into byte offset here before writing */
	offset *= sizeof(unsigned long);

	DX_MNG_READ_WORD_VIA_AIB((0x1 << 0x10) | offset, old_val);
	val |= old_val;
	DX_MNG_WRITE_WORD_VIA_AIB((0x1 << 0x11) | offset, val);
	DX_MNG_READ_WORD_VIA_AIB((0x1 << 0x10) | offset, old_val);

	if (old_val != val)
		return -EIO;

	return 0;
}
