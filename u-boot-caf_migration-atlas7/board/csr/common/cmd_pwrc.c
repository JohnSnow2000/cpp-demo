/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <command.h>
#include <asm/arch/cpu.h>
#include <asm/processor.h>
#include <asm/io.h>
#include <asm/arch/board.h>


#if defined(CONFIG_CMD_IOBG)
int do_pwrc_indirect(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	u32 addr;
	u32 val;

	switch (argc) {
	case 2:
		addr = simple_strtoul(argv[1], NULL, 16);
		printf("0x%x\n", sirfsoc_rtc_iobrg_readl(addr));
		break;
	case 3:
		addr = simple_strtoul(argv[1], NULL, 16);
		val = simple_strtoul(argv[2], NULL, 16);
		sirfsoc_rtc_iobrg_writel(val, addr);
		break;
	default:
		return cmd_usage(cmdtp);
	}
	return 0;
}

U_BOOT_CMD(
	pwrc,	3,	1,	do_pwrc_indirect,
	"offset [val]",
	""
);

#endif
