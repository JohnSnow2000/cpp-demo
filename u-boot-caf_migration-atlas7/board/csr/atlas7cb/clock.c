#include <common.h>
#include <asm/utils.h>
#include <asm/io.h>
#include <asm/arch/clkc.h>
#include <asm/arch/ddrphy.h>
#include <asm/arch/upctl.h>
#include <asm/arch/cpurtc_io.h>
#include <asm/arch/cpu.h>
#include <asm/arch/board.h>
#include <asm/arch/pwrc.h>

#define modify_reg(a, and_value, or_value) { \
	writel((readl(a) & and_value) | or_value, a); \
}

#define poll_reg(a, m, v, l, e) { \
	unsigned int wd = l; \
	while ((readl(a)&m) != (v&m)) { \
		if (--wd == 0) { \
			return ; \
		} \
	} \
}

struct PLLREG {
	u32 r_freq;
	u32 r_ssc;
	u32 r_ctrl0;
	u32 r_ctrl1;
	u32 r_st;

};

struct PLLVAL {
	u32 reg_base;
	u32 divf;
	u32 divr;
	u32 sse;
	u32 ssdiv;
	u32 ssmod;
	u32 ssdepth;
	u32 divq1;
	u32 divq2;
	u32 divq3;
	u32 en;
};

#define PLL_AB_CTRL0_SSE			(1 << 12)
#define PLL_AB_CTRL1_DIVQ1_MASK		0x7
#define PLL_AB_CTRL0_RSB			(1 << 0)
#define PLL_AB_CTRL0_BYPASS			(1 << 4)

#define SYS0_SFT 0
#define SYS1_SFT 8
#define SYS2_SFT 16
#define SYS3_SFT 24

#define S2_20_SFT 0
#define S1_20_SFT 8
#define S1_19_SFT 16
#define S1_18_SFT 24
#define S0_20_SFT 0
#define S1_17_SFT 8

#define SYS0_EN BIT(0)
#define SYS1_EN BIT(4)
#define SYS2_EN BIT(8)
#define SYS3_EN BIT(12)

#define SEL_XIN     0
#define SEL_XINiW   1
#define SEL_SYS0    2
#define SEL_SYS1    3
#define SEL_SYS2    4
#define SEL_SYS3    5

#define SEL_SYS2_D20    2   /* PRE-SEL 2 */
#define SEL_SYS1_D20    3   /* PRE-SEL 3 */
#define SEL_SYS1_D19    4   /* PRE-SEL 4 */
#define SEL_SYS1_D18    5   /* PRE-SEL 5 */
#define SEL_SYS0_D20    6   /* PRE-SEL 6 */
#define SEL_SYS1_D17    7   /* PRE-SEL 7 */

const  struct PLLVAL g_pll_tbl[] = {
	/* base,divf,divr,sse,ssdiv,ssmod,ssdepth,divq1,divq2,divq3,en */
	{CLKC_CPUPLL_AB_FREQ, 60, 0, 0, 0, 0, 0, 2, 0, 0, 0x7},
	{CLKC_SYS0_BTPLL_AB_FREQ, 39, 0, 0, 0, 0, 0, 0, 0, 0, 0x3},
	{CLKC_SYS1_USBPLL_AB_FREQ, 45, 0, 1, 709, 249, 0, 0, 0, 0, 0x3},
	{CLKC_SYS2_ETHPLL_AB_FREQ, 124, 1, 0, 0, 0, 0, 0, 0, 0, 0x0},
	{CLKC_SYS3_SSCPLL_AB_FREQ, 116, 3, 1, 176, 105, 3, 0, 0, 0, 0x3},
};

void pll_init(void)
{
	int offset;
	int tmp;
	int i;
	int pll_num;
	struct PLLREG *reg;
	struct PLLVAL *p_plltbl;

	__asm__ __volatile__("@ get_off\n"
			     "adr %0, pll_init\n"
			     "ldr %1, =pll_init\n"
			     "sub %0, %0, %1\n" : "=&r"(offset), "=&r"(tmp)
			     : : );

	p_plltbl = (struct PLLVAL *)((int)g_pll_tbl + offset);

	if (PWRC_IS_CLOCK_INITIALIZED())
		pll_num = 1; /* the other 4 has been initialized by M3 */
	else
		pll_num =  ARRAY_SIZE(g_pll_tbl);

	for (i = 0; i < pll_num; i++) {
		if (p_plltbl[i].reg_base == 0)
			break;
		reg = (struct PLLREG *)p_plltbl[i].reg_base;
		/* set freq reg */
		writel((p_plltbl[i].divf & 0x1ff) |
		       ((p_plltbl[i].divr & 0x7) << 16), &reg->r_freq);
		/* set SS mode */
		if (p_plltbl[i].sse != 0)
			writel(p_plltbl[i].ssdepth << 20 |
		  p_plltbl[i].ssdiv << 8 | p_plltbl[i].ssmod, &reg->r_ssc);
		modify_reg(&reg->r_ctrl0,
			   ~(PLL_AB_CTRL0_SSE), p_plltbl[i].sse << 12);
		/* set DIVQ for pll out 1/2/3 */
		if (p_plltbl[i].divq1 != 0)
			modify_reg(&reg->r_ctrl1, ~(0x7 << 0),
				   p_plltbl[i].divq1 << 0);
		if (p_plltbl[i].divq2 != 0)
			modify_reg(&reg->r_ctrl1, ~(0x7 << 4),
				   p_plltbl[i].divq2 << 4);
		if (p_plltbl[i].divq3 != 0)
			modify_reg(&reg->r_ctrl1, ~(0x7 << 8),
				   p_plltbl[i].divq3 << 8);
		/* set pll outx en */
		modify_reg(&reg->r_ctrl1, ~(0x7 << 12), p_plltbl[i].en << 12);

		/* clear by-pass bit */
		modify_reg(&reg->r_ctrl0, ~(PLL_AB_CTRL0_BYPASS), 0);

		/* release reset */
		modify_reg(&reg->r_ctrl0, ~(0), PLL_AB_CTRL0_RSB);
	}

	for (i = 0; i < pll_num; i++) {
		/* wait all plls to get locked */
		poll_reg(p_plltbl[i].reg_base + 0x10, 0x1, 0x1, 100000, -1);
	}
}


static void clk_src_init(void)
{
	/* cpu clock sel */
	writel(4, CLKC_CPU_CLK_SEL);

	/* already initialized by M3, so return without futher action*/
	if (PWRC_IS_CLOCK_INITIALIZED()) {
		/* sdphy01 clock sel */
		writel(19 << SYS0_SFT, CLKC_SDPHY01_CLK_CFG);
		writel(SYS0_EN, CLKC_SDPHY01_CLK_DIVENA);
		writel(SEL_SYS0, CLKC_SDPHY01_CLK_SEL);
		/* sdphy23 clock sel */
		writel(23 << SYS1_SFT, CLKC_SDPHY23_CLK_CFG);
		writel(SYS1_EN, CLKC_SDPHY23_CLK_DIVENA);
		writel(SEL_SYS1, CLKC_SDPHY23_CLK_SEL);
		return;
	}

	/* usb phy clock sel */
	writel(49 << SYS1_SFT, CLKC_USBPHY_CLK_CFG);
	writel(SYS1_EN, CLKC_USBPHY_CLK_DIVENA);
	writel(SEL_SYS1, CLKC_USBPHY_CLK_SEL);
	writel((1<<1) | (1<<28), CLKC_ROOT_CLK_EN1_SET);
	writel((0xf << 9), CLKC_MISC1_LEAF_CLK_EN_SET);

	/* bt clock sel */
	writel(12 << SYS0_SFT, CLKC_BTSS_CLK_CFG);
	writel(SYS0_EN, CLKC_BTSS_CLK_DIVENA);
	writel(SEL_SYS0, CLKC_BTSS_CLK_SEL);

	/* rgmii clock sel */
	writel(12 << SYS2_SFT, CLKC_RGMII_CLK_CFG);
	writel(SYS2_EN, CLKC_RGMII_CLK_DIVENA);
	writel(SEL_SYS2, CLKC_RGMII_CLK_SEL);

	/* rgmii clock sel */
	writel(12 << SYS2_SFT, CLKC_RGMII_CLK_CFG);
	writel(SYS2_EN, CLKC_RGMII_CLK_DIVENA);
	writel(SEL_SYS2, CLKC_RGMII_CLK_SEL);

	/* CAN clock sel */
	writel(24 << SYS1_SFT, CLKC_CAN_CLK_CFG);
	writel(SYS1_EN, CLKC_CAN_CLK_DIVENA);
	writel(SEL_SYS1, CLKC_CAN_CLK_SEL);

	/* deint clock sel */
	writel(8 << SYS2_SFT, CLKC_DEINT_CLK_CFG);
	writel(SYS2_EN, CLKC_DEINT_CLK_DIVENA);
	writel(SEL_SYS2, CLKC_DEINT_CLK_SEL);

	/* NAND clock sel */
	writel(1 << SYS1_SFT, CLKC_NAND_CLK_CFG);
	writel(SYS1_EN, CLKC_NAND_CLK_DIVENA);
	writel(SEL_SYS1, CLKC_NAND_CLK_SEL);

	/* DISP0 clock sel */
	writel(3 << SYS1_SFT, CLKC_DISP0_CLK_CFG);
	writel(SYS1_EN, CLKC_DISP0_CLK_DIVENA);
	writel(SEL_SYS1, CLKC_DISP0_CLK_SEL);

	/* DISP1 clock sel */
	writel(3 << SYS1_SFT, CLKC_DISP1_CLK_CFG);
	writel(SYS1_EN, CLKC_DISP1_CLK_DIVENA);
	writel(SEL_SYS1, CLKC_DISP1_CLK_SEL);

	/* GPU clock sel */
	writel(3 << SYS1_SFT, CLKC_GPU_CLK_CFG);
	writel(SYS1_EN, CLKC_GPU_CLK_DIVENA);
	writel(SEL_SYS1, CLKC_GPU_CLK_SEL);

	/* GNSS clock sel: sys0 qa15 */
	writel(8 << SYS1_SFT, CLKC_GNSS_CLK_CFG);
	writel(SYS1_EN, CLKC_GNSS_CLK_DIVENA);
	poll_reg(CLKC_GNSS_CLK_SEL_STATUS, 1, 1, 10000, -1);
	writel(SEL_SYS1, CLKC_GNSS_CLK_SEL);
	poll_reg(CLKC_GNSS_CLK_SEL_STATUS, 1, 1, 10000, -1);
	/* Pre-sel clock source */
	writel((15 << S2_20_SFT) | (7 << S1_20_SFT) | (5 << S1_19_SFT) |
		(4 << S1_18_SFT), CLKC_SHARED_DIVIDERS_CFG0);
	writel((3 << S0_20_SFT) | (3 << S1_17_SFT), CLKC_SHARED_DIVIDERS_CFG1);
	writel((1 << 0) | (1 << 4) | (1 << 8) | (1 << 12) |
		(1 << 16) | (1 << 20), CLKC_SHARED_DIVIDERS_ENA);

	/* sdphy01 clock sel */
	writel(19 << SYS0_SFT, CLKC_SDPHY01_CLK_CFG);
	writel(SYS0_EN, CLKC_SDPHY01_CLK_DIVENA);
	writel(SEL_SYS0, CLKC_SDPHY01_CLK_SEL);

	/* sdphy23 clock sel */
	writel(23 << SYS1_SFT, CLKC_SDPHY23_CLK_CFG);
	writel(SYS1_EN, CLKC_SDPHY23_CLK_DIVENA);
	writel(SEL_SYS1, CLKC_SDPHY23_CLK_SEL);

	/* below modules use pre-sel clock */
	writel(4, CLKC_VDEC_CLK_SEL);
	writel(4, CLKC_KAS_CLK_SEL);
	writel(4, CLKC_SDR_CLK_SEL);
	writel(4, CLKC_NOCR_CLK_SEL);
	writel(4, CLKC_NOCD_CLK_SEL);
	writel(5, CLKC_TPIU_CLK_SEL);
	writel(6, CLKC_SEC_CLK_SEL);
	writel(6, CLKC_JPENC_CLK_SEL);
	writel(7, CLKC_SYS_CLK_SEL);
	writel(7, CLKC_G2D_CLK_SEL);
	writel(6, CLKC_GMAC_CLK_SEL);
	writel(7, CLKC_USB_CLK_SEL);
	writel(7, CLKC_VIP_CLK_SEL);
	writel(7, CLKC_HSI2S_CLK_SEL);

	/* patch for QSPI Rd failure in XIP mode @ -40C	*/
	#define A7DA_PWRC_XINW_FMODE_CTRL	0x302C
	sirfsoc_rtc_iobrg_unsafe_writel(1, A7DA_PWRC_XINW_FMODE_CTRL);

}

/* clk_src_late_init() will be called in 2nd stage */
void clk_src_late_init(void)
{
	request_hwlock();

	if (!PWRC_IS_CLOCK_LATE_INITIALIZED()) {
		/* ioclk */
		writel(3, CLKC_IO_CLK_SEL);

		/* sdphy01 clock sel */
		writel(9 << SYS0_SFT, CLKC_SDPHY01_CLK_CFG);
		writel(SYS0_EN, CLKC_SDPHY01_CLK_DIVENA);
		writel(SEL_SYS0, CLKC_SDPHY01_CLK_SEL);

		/* sdphy23 clock sel */
		writel(11 << SYS1_SFT, CLKC_SDPHY23_CLK_CFG);
		writel(SYS1_EN, CLKC_SDPHY23_CLK_DIVENA);
		writel(SEL_SYS1, CLKC_SDPHY23_CLK_SEL);

		/* sdphy45 clock sel */
		writel(15 << SYS1_SFT, CLKC_SDPHY45_CLK_CFG);
		writel(SYS1_EN, CLKC_SDPHY45_CLK_DIVENA);
		writel(SEL_SYS1, CLKC_SDPHY45_CLK_SEL);

		/* sdphy67 clock sel */
		writel(9 << SYS0_SFT, CLKC_SDPHY67_CLK_CFG);
		writel(SYS0_EN, CLKC_SDPHY67_CLK_DIVENA);
		writel(SEL_SYS0, CLKC_SDPHY67_CLK_SEL);

		PWRC_SET_CLOCK_LATE_INITIALIZED();
	} else {
		/* sdphy01 clock sel */
		writel(9 << SYS0_SFT, CLKC_SDPHY01_CLK_CFG);
		writel(SYS0_EN, CLKC_SDPHY01_CLK_DIVENA);
		writel(SEL_SYS0, CLKC_SDPHY01_CLK_SEL);

		/* sdphy23 clock sel */
		writel(11 << SYS1_SFT, CLKC_SDPHY23_CLK_CFG);
		writel(SYS1_EN, CLKC_SDPHY23_CLK_DIVENA);
		writel(SEL_SYS1, CLKC_SDPHY23_CLK_SEL);
	}

	release_hwlock();

}

void clock_init(void)
{
	request_hwlock();
	pll_init();
	clk_src_init();

	if (PWRC_IS_CLOCK_INITIALIZED()) {
		release_hwlock();
		return;
	}

	/* init the necesary clocks used in uboot stg1 */
	writel(CLKC_ROOT_CLK_EN0_SET_SDPHY01 |
			CLKC_ROOT_CLK_EN0_SET_MEDIAM_IO |
			CLKC_ROOT_CLK_EN0_SET_VDIFM_IO,
			CLKC_ROOT_CLK_EN0_SET);
	writel(CLKC_MISC1_LEAF_SYS2PCI_IO_CLKEN |
			CLKC_MISC1_LEAF_PCIARB_IO_CLKEN |
			CLKC_MISC1_LEAF_SYS2PCI2_IO_CLKEN,
			CLKC_MISC1_LEAF_CLK_EN_SET);
	writel(CLKC_SDIO_LEAF_IO_CLKEN |
			CLKC_SDIO_LEAF_SDPHY_CLKEN,
			CLKC_SDIO01_LEAF_CLK_EN_SET);
	writel(CLKC_SDIO_LEAF_IO_CLKEN |
			CLKC_SDIO_LEAF_SDPHY_CLKEN,
			CLKC_SDIO23_LEAF_CLK_EN_SET);

	PWRC_SET_CLOCK_INITIALIZED();

	release_hwlock();
}
