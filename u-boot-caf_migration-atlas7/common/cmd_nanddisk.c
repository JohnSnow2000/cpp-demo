/*
 * (C) Copyright 2000-2003
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <command.h>
#include <net.h>
#include <part.h>
#include <malloc.h>
#include <asm/arch/nand.h>

int do_nanddisk(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	char *cmd;
	enum nanddisk_map_state state;

	block_dev_desc_t *nanddisk_dev = NULL;

	if (argc < 2)
		return CMD_RET_USAGE;

	cmd = argv[1];

	if (strncmp(cmd, "init", 4) == 0) {
		printf("init NANDDISK...\n");
		return !!nanddisk_init();
	}

	nanddisk_dev = get_dev("nand", 0);
	if (!nanddisk_dev || !nanddisk_dev->blksz) {
		printf("no valid nanddisk, try 'ndisk init' first.\n");
		return 1;
	}

	if (strncmp(cmd, "erase", 5) == 0) {
		if (cmd[5] != 0) {
			if (!strcmp(&cmd[5], ".part")) {
				printf("erase part chip...\n");
				return !!nanddisk_reset();
			} else if (!strcmp(&cmd[5], ".chip")) {
				printf("erase whole chip...\n");
				return !!nanddisk_erase();
			} else {
				goto usage;
			}
		}
	}

	if (strncmp(cmd, "info", 4) == 0) {
		nanddisk_print_info();
		return 0;
	}

	if (strncmp(cmd, "map", 3) == 0) {
		if (cmd[3] != 0) {
			if (!strcmp(&cmd[3], ".nml")) {
				printf("change to normal map.\n");
				state = NANDISK_MAP_NORMAL;
			} else if (!strcmp(&cmd[3], ".bpa")) {
				state = NANDISK_MAP_BOOTPARTITION;
				printf("change to new map for boot partition.\n");
			} else {
				goto usage;
			}
			return !!nanddisk_map_change(state);
		}

		return 0;
	}

	if (strcmp(cmd, "read") == 0) {
		if (argc == 5) {
			unsigned long addr = simple_strtoul(argv[2], NULL, 16);
			unsigned long blk  = simple_strtoul(argv[3], NULL, 16);
			unsigned long cnt  = simple_strtoul(argv[4], NULL, 16);
			unsigned long n;
			unsigned long time_start, time_cost, len;

			printf("\nNANDDISK read: block # %ld, count %ld"
					"...\n", blk, cnt);

			time_start = get_timer(0);
			n = nanddisk_dev->block_read(0, blk, cnt,
					(ulong *)addr);
			time_cost = get_timer(time_start);

			printf("%ld blocks read: %s.\n", n,
			       (n == cnt) ? "OK" : "ERROR");

			len = n * nanddisk_dev->blksz;
			printf("%lu bytes read in %lu ms", len, time_cost);
			if (time_cost > 0) {
				puts("(");
				print_size(len / time_cost * 1000, "/s");
				puts(")");
			}
			puts("\n");

			if (n == cnt)
				return 0;
			return 1;
		}
	}

	if (strcmp(cmd, "write") == 0) {
		if (argc == 5) {
			unsigned long addr = simple_strtoul(argv[2], NULL, 16);
			unsigned long blk  = simple_strtoul(argv[3], NULL, 16);
			unsigned long cnt  = simple_strtoul(argv[4], NULL, 16);
			unsigned long n;
			unsigned long time_start, time_cost, len;

			printf("\nNANDDISK write: block # %ld, count %ld"
					"...\n",  blk, cnt);

			time_start = get_timer(0);
			n = nanddisk_dev->block_write(0, blk, cnt,
					(ulong *)addr);
			time_cost = get_timer(time_start);

			printf("%ld blocks write: %s\n", n,
			       (n == cnt) ? "OK" : "ERROR");

			len = n * nanddisk_dev->blksz;
			printf("%lu bytes write in %lu ms", len, time_cost);
			if (time_cost > 0) {
				puts("(");
				print_size(len / time_cost * 1000, "/s");
				puts(")");
			}
			puts("\n");

			if (n == cnt)
				return 0;
			return 1;
		}
	}

usage:
	return CMD_RET_USAGE;
}

U_BOOT_CMD(
	ndisk,	5,	1,	do_nanddisk,
	"NANDDISK firmware",
	"init - init nanddisk firmware.\n"
	"ndisk info - nanddisk information.\n"
	"ndisk map.nml - nanddisk normal map.\n"
	"ndisk map.bpa - nanddisk new map for boot partition.\n"
	"ndisk erase.part - erase the data partition.\n"
	"ndisk erase.chip - erase the whole nand.\n"
	"ndisk read addr blk# cnt - read `cnt' blocks starting at\n"
	"	block `blk#' to memory address `addr'\n"
	"ndisk write addr blk# cnt - write `cnt' blocks starting at\n"
	"	block `blk#' from memory address `addr'"
);
